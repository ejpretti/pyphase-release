# pyphase_test.test_lammps: Evan Pretti

import numpy
import os
import unittest
import unittest.mock

from pyphase import lammps
from pyphase_tools import _test_utilities

class FrameworkTests(unittest.TestCase):
    def test_lammps_override(self):
        # Tests to see that a global override can be set and cleared.
        test_path = "test_override_path"

        lammps.set_lammps_global()
        self.assertIs(lammps._LAMMPS_OVERRIDE, None)

        lammps.set_lammps_global(test_path)
        self.assertEqual(lammps._LAMMPS_OVERRIDE, test_path)

        lammps.set_lammps_global()
        self.assertIs(lammps._LAMMPS_OVERRIDE, None)

    def test_random_seed(self):
        # Ensures that a random seed can be generated.  This simply tests that the routine does not crash.
        self.assertTrue(isinstance(lammps.random_seed(), int))

    def test_find(self):
        # Default LAMMPS search behavior
        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertTrue(_test_utilities.is_lammps(path))

    def test_find_override(self):
        # Ensures that an unconditional override works
        # The path should not be resolved to an absolute path
        test_path = "test_override_path"

        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertNotEqual(path, test_path)

            lammps.set_lammps_global(test_path)
            self.assertEqual(lammps.find_lammps(), test_path)

    def test_find_path_override(self):
        # Ensure that override via PYPHASELMPPATH works
        # The path should not be resolved to an absolute path
        test_path = "test_override_path"

        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertNotEqual(path, test_path)

            os.environ[lammps._PYPHASELMPPATH] = test_path
            self.assertEqual(lammps.find_lammps(), test_path)

    def test_find_override_path(self):
        # Ensures that an unconditional override works
        # even if PYPHASELMPPATH is set differently
        # The path should not be resolved to an absolute path
        test_path = "test_override_path"
        alt_path = "alt_override_path"

        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertNotEqual(path, test_path)

            lammps.set_lammps_global(test_path)
            os.environ[lammps._PYPHASELMPPATH] = alt_path
            self.assertEqual(lammps.find_lammps(), test_path)

    def test_find_name(self):
        # Ensures that PYPHASELMPNAME works
        lmp_name = "test_specify_lammps"

        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertNotEqual(os.path.basename(path), lmp_name)
            
            with _test_utilities.mock_lammps(lmp_name) as mock_path:
                path_patch = {"PATH": "{}{}{}".format(os.path.dirname(mock_path), os.pathsep, os.environ["PATH"])}
                with unittest.mock.patch.dict(os.environ, path_patch):
                    os.environ[lammps._PYPHASELMPNAME] = lmp_name
                    self.assertEqual(lammps.find_lammps(), mock_path)

    def test_find_explicit(self):
        # Ensures that LAMMPS can be found by specifying a name explicitly
        lmp_name = "test_specify_lammps"

        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertNotEqual(os.path.basename(path), lmp_name)
            
            with _test_utilities.mock_lammps(lmp_name) as mock_path:
                path_patch = {"PATH": "{}{}{}".format(os.path.dirname(mock_path), os.pathsep, os.environ["PATH"])}
                with unittest.mock.patch.dict(os.environ, path_patch):
                    self.assertEqual(lammps.find_lammps([lmp_name]), mock_path)

    def test_find_name_explicit(self):
        # Ensures that PYPHASELMPNAME inserts in front of an explicit name
        lmp_name = "test_specify_lammps"
        alt_name = "alt_specify_lammps"

        with _test_utilities.reset_lookup():
            path = lammps.find_lammps()
            self.assertNotEqual(os.path.basename(path), lmp_name)
            
            with _test_utilities.mock_lammps(lmp_name) as mock_path:
                with _test_utilities.mock_lammps(alt_name) as alt_path:
                    path_patch = {"PATH": "{}{}{}{}{}".format(
                        os.path.dirname(alt_path), os.pathsep,
                        os.path.dirname(mock_path), os.pathsep, os.environ["PATH"])}
                    with unittest.mock.patch.dict(os.environ, path_patch):
                        os.environ[lammps._PYPHASELMPNAME] = lmp_name
                        self.assertEqual(lammps.find_lammps([alt_name]), mock_path)
                        self.assertNotEqual(lammps.find_lammps([alt_name]), alt_path)

    def test_persistent_dir(self):
        # PersistentStorage directory testing
        with lammps.PersistentStorage() as persistent_storage:
            # Directory should exist
            self.assertTrue(os.path.isdir(persistent_storage()))

    def test_persistent_dir(self):
        # PersistentStorage file testing
        test_name = "test_name"
        test_data = b"test_data"

        with lammps.PersistentStorage() as persistent_storage:
            file_name = persistent_storage(test_name)
            self.assertEqual(os.path.basename(file_name), test_name)
            # File should not exist but should be writable
            self.assertFalse(os.path.exists(file_name))
            with open(file_name, "wb") as test_file:
                test_file.write(test_data)
            with open(file_name, "rb") as test_file:
                self.assertEqual(test_data, test_file.read())

    def test_quote_single(self):
        # No quotation necessary
        test = "test"
        self.assertEqual(test, lammps._quote(test))

        # Default should be double quotes
        test = "test "
        self.assertEqual("\"{}\"".format(test), lammps._quote(test))

        # Single quotes should also be surrounded in double quotes
        test = "test'"
        self.assertEqual("\"{}\"".format(test), lammps._quote(test))

        # Single quotes should be used when necessary
        test = "test\""
        self.assertEqual("'{}'".format(test), lammps._quote(test))

    def test_quote_triple(self):
        # Newlines should be surrounded in triple quotes
        test = "test'''\n"
        self.assertEqual("\"\"\"{}\"\"\"".format(test), lammps._quote(test))

        # If triple quotes are already present, the quoting should fail
        test = "test\"\"\"\n"
        with self.assertRaises(ValueError):
            lammps._quote(test)

        # Triple quotes can be used if single and double quotes are present
        test = "test\"'"
        self.assertEqual("\"\"\"{}\"\"\"".format(test), lammps._quote(test))

        # But not if the string ends in a double quote
        test = "test'\""
        with self.assertRaises(ValueError):
            lammps._quote(test)

        # With a newline and sequence of three triple quotes, quoting should fail
        test = "test'''\"\"\"\n"
        with self.assertRaises(ValueError):
            lammps._quote(test)

        # Likewise if the string ends in double quotes
        test = "test'''\n\"\""
        with self.assertRaises(ValueError):
            lammps._quote(test)

class RunTests(unittest.TestCase):
    def test_standard(self):
        # Standard LAMMPS execution
        run = lammps.LAMMPS()
        run.quit()
        lammps.run_lammps(run, [])
    
    def test_fail_command(self):
        # Ensure a LAMMPSError is raised if something goes wrong
        run = lammps.LAMMPS()
        run.nonexistent_command()
        with self.assertRaises(lammps.LAMMPSError):
            lammps.run_lammps(run, [])

    def test_fail_exit_code(self):
        # Ensure a LAMMPSError is raised if something goes wrong
        run = lammps.LAMMPS()
        run.quit(1)
        with self.assertRaises(lammps.LAMMPSError):
            lammps.run_lammps(run, [])

    def test_log(self):
        # Send a string to the log file and make sure it is retrievable
        test_message = "test message"

        run = lammps.LAMMPS()
        run.print(test_message)
        log = lammps.run_lammps(run, [])[0].decode()
        self.assertIn("\n{0}\n".format(test_message), log)

    def test_output_names(self):
        # Have LAMMPS generate a file and make sure it is retrievable
        test_file = "test_file"
        test_message = "test message"

        run = lammps.LAMMPS()
        run.print(test_message, "file", test_file)
        data = lammps.run_lammps(run, [test_file])[1][test_file].decode()
        self.assertEqual(data.strip(), test_message)

    def test_auxiliary_data(self):
        # Make sure that LAMMPS can read a file in and out
        test_script = "test_script"
        test_file = "test_file"
        test_message = "test message"

        run = lammps.LAMMPS()
        run.include(test_script)
        data = lammps.run_lammps(run, [test_file],
            auxiliary_data={test_script: "print {} file {}".format(lammps._quote(test_message),
            lammps._quote(test_file)).encode()})[1][test_file].decode()
        self.assertEqual(data.strip(), test_message)

    def test_move_in(self):
        # Make sure that files moved in are gone from the source and
        # present in the destination with their contents intact
        test_source = "test_source"
        test_script = "test_script"
        test_file = "test_file"
        test_message = "test message"

        with lammps.PersistentStorage() as persistent_storage:
            in_path = persistent_storage(test_source)
            with open(in_path, "w") as in_file:
                in_file.write("print {} file {}".format(lammps._quote(test_message), lammps._quote(test_file)))

            run = lammps.LAMMPS()
            run.include(test_script)
            data = lammps.run_lammps(run, [test_file], move_in=[(in_path, test_script)])[1][test_file].decode()

            self.assertFalse(os.path.exists(in_path))
            self.assertEqual(data.strip(), test_message)

    def test_move_out(self):
        # Make sure that files moved out are moved unconditionally
        # and that the data within them is recoverable
        over_file = "over_file"
        over_message = "over_message"
        test_file = "test_file"
        test_message = "test message"
        
        with lammps.PersistentStorage() as persistent_storage:
            over_path = persistent_storage(over_file)
            with open(over_path, "w") as over_data:
                over_data.write(over_message)

            run = lammps.LAMMPS()
            run.print(test_message, "file", test_file)
            lammps.run_lammps(run, [], move_out=[(test_file, over_path)])

            with open(over_path, "r") as over_data:
                self.assertEqual(over_data.read().strip(), test_message)

# Reference implementation of Lennard-Jones potential
def _lj(r, epsilon, sigma, r_cut):
    inv_r = sigma / r
    inv_r_2 = inv_r * inv_r
    inv_r_6 = inv_r_2 * inv_r_2 * inv_r_2

    u = 4 * epsilon * (inv_r_6 * (inv_r_6 - 1))
    f = 24 * epsilon * inv_r * (inv_r_6 * (2 * inv_r_6 - 1)) / sigma

    u[r >= r_cut] = 0
    f[r >= r_cut] = 0

    return u, f

class PairTests(unittest.TestCase):
    def test_pair_abstract(self):
        # It should not be possible to use a lammps.Pair directly
        with self.assertRaises(NotImplementedError):
            lammps.Pair(1)(lammps.LAMMPS())

    def test_lammps_pair_unary(self):
        # Standard test with Lennard-Jones
        epsilon = 1.01
        sigma = 0.99
        r_in = 0.5
        r_cut = 3
        r_out = 4
        n = 100

        pair = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (epsilon, sigma)}, style_args=(r_cut,))
        r = numpy.linspace(r_in, r_out, n)
        reference = _lj(r, epsilon, sigma, r_cut)
        evaluated = pair.evaluate(1, 1, r_in, r_out, n)

        self.assertTrue(numpy.all(numpy.isclose(r, evaluated[0])))
        self.assertTrue(numpy.all(numpy.isclose(reference[0], evaluated[1])))
        self.assertTrue(numpy.all(numpy.isclose(reference[1], evaluated[2])))

    def test_lammps_pair_shift(self):
        # Ensure that modification arguments can be passed
        epsilon = 1.01
        sigma = 0.99
        r_in = 0.5
        r_cut = 3
        r_out = 4
        n = 100

        pair = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (epsilon, sigma)}, style_args=(r_cut,), modifications=("shift", "yes"))
        r = numpy.linspace(r_in, r_out, n)
        reference = _lj(r, epsilon, sigma, r_cut)
        evaluated = pair.evaluate(1, 1, r_in, r_out, n)

        reference[0][r < r_cut] -= _lj(numpy.array([r_cut]), epsilon, sigma, r_out)[0]

        self.assertTrue(numpy.all(numpy.isclose(r, evaluated[0])))
        self.assertTrue(numpy.all(numpy.isclose(reference[0], evaluated[1])))
        self.assertTrue(numpy.all(numpy.isclose(reference[1], evaluated[2])))

    def test_lammps_pair_binary(self):
        # Test for multi-component systems
        epsilon = 1.01, 2.1, 1.4
        sigma = 0.99, 0.95, 0.97
        r_in = 0.5
        r_cut = 3
        r_out = 4
        n = 100
        ij = [(1, 1), (2, 2), (1, 2)]

        pair = lammps.LAMMPSPair("lj/cut", 2,
            {(i, j): (epsilon_i, sigma_i) for (i, j), epsilon_i, sigma_i in zip(ij, epsilon, sigma)},
            style_args=(r_cut,))
        r = numpy.linspace(r_in, r_out, n)
        reference = tuple(_lj(r, epsilon_i, sigma_i, r_cut) for epsilon_i, sigma_i in zip(epsilon, sigma))
        evaluated = tuple(pair.evaluate(i, j, r_in, r_out, n) for i, j in [(1, 1), (2, 2), (1, 2)])
        
        for reference_i, evaluated_i in zip(reference, evaluated):
            self.assertTrue(numpy.all(numpy.isclose(r, evaluated_i[0])))
            self.assertTrue(numpy.all(numpy.isclose(reference_i[0], evaluated_i[1])))
            self.assertTrue(numpy.all(numpy.isclose(reference_i[1], evaluated_i[2])))

    def test_lammps_pair_cut(self):
        # Make sure that behavior near the cutoff is correct
        epsilon = 1.01
        sigma = 0.09
        r_in = 2.99
        r_cut = 3
        r_out = 3.01
        n = 201

        pair = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (epsilon, sigma)}, style_args=(r_cut,))
        r = numpy.linspace(r_in, r_out, n)
        evaluated = pair.evaluate(1, 1, r_in, r_out, n)

        self.assertTrue(numpy.all(numpy.isclose(r, evaluated[0])))
        for evaluated_i in evaluated[1:]:
            self.assertTrue(numpy.all(evaluated_i[r < r_cut] != 0))
            self.assertTrue(numpy.all(evaluated_i[r >= r_cut] == 0))

    def test_python_pair_unary(self):
        # Standard test with Lennard-Jones
        epsilon = 1.01
        sigma = 0.99
        r_in = 0.5
        r_cut = 3
        r_out = 4
        n_tab = 4096
        n = 100
        tolerance = 1e-3

        pair = lammps.PythonPair(1,
            {(1, 1): lambda r: tuple(array[0] for array in _lj(numpy.array([r]), epsilon, sigma, r_out))},
            r_in, r_cut, n_tab)
        r = numpy.linspace(r_in, r_out, n)
        reference = _lj(r, epsilon, sigma, r_cut)
        evaluated = pair.evaluate(1, 1, r_in, r_out, n)

        potential_error = evaluated[1] - reference[0]
        potential_error = numpy.minimum(numpy.abs(potential_error), numpy.abs(potential_error / reference[0]))
        force_error = evaluated[2] - reference[1]
        force_error = numpy.minimum(numpy.abs(force_error), numpy.abs(force_error / reference[1]))
    
        # The relative error should be low but will not be zero due to tabulation
        # Check whether or not we are past the cutoff since the relative error will be undefined
        self.assertTrue(numpy.all(numpy.logical_or(potential_error < tolerance, r >= r_cut)))
        self.assertTrue(numpy.all(numpy.logical_or(force_error < tolerance, r >= r_cut)))

    def test_python_pair_binary(self):
        # Test for multi-component systems
        epsilon = 1.01, 2.1, 1.4
        sigma = 0.99, 0.95, 0.97
        r_in = 0.5
        r_cut = 3
        r_out = 4
        n_tab = 4096
        n = 100
        tolerance = 1e-3
        ij = [(1, 1), (2, 2), (1, 2)]

        pair = lammps.PythonPair(2,
            {(i, j): lambda r, epsilon_i=epsilon_i, sigma_i=sigma_i: tuple(array[0] for array in _lj(numpy.array([r]), epsilon_i, sigma_i, r_out))
            for (i, j), epsilon_i, sigma_i in zip(ij, epsilon, sigma)}, r_in, r_cut, n_tab)
        r = numpy.linspace(r_in, r_out, n)
        for (i, j), epsilon_i, sigma_i in zip(ij, epsilon, sigma):
            reference = _lj(r, epsilon_i, sigma_i, r_cut)
            evaluated = pair.evaluate(i, j, r_in, r_out, n)

            potential_error = evaluated[1] - reference[0]
            potential_error = numpy.minimum(numpy.abs(potential_error), numpy.abs(potential_error / reference[0]))
            force_error = evaluated[2] - reference[1]
            force_error = numpy.minimum(numpy.abs(force_error), numpy.abs(force_error / reference[1]))

            # The relative error should be low but will not be zero due to tabulation
            # Check whether or not we are past the cutoff since the relative error will be undefined
            self.assertTrue(numpy.all(numpy.logical_or(potential_error < tolerance, r >= r_cut)))
            self.assertTrue(numpy.all(numpy.logical_or(force_error < tolerance, r >= r_cut)))

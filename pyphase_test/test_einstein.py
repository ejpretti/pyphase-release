# pyphase_test.test_einstein: Evan Pretti

import io
import numpy
import os
import pickle
import sys
import time
import traceback
import unittest

from pyphase import crystal
from pyphase import structure
from pyphase import lammps
from pyphase import relax
from pyphase import einstein

from pyphase_tools import _test_utilities

#: Timestep to match VNLJSTS system
_VNLJSTS_DT = 0.0005
#: Options N_equil, N_prod, and N_block for VNLJSTS system
_VNLJSTS_STEPS = (10000, 100000, 1000)
#: Option N_dump for VNLJSTS system
_VNLJSTS_N_DUMP = 10
#: Temperature to match VNLJSTS system
_VNLJSTS_T = 2
#: Thermostat time to match VNLJSTS system
_VNLJSTS_TAU_DAMP = 0.1
#: Maximum spring constant to match VNLJSTS system
_VNLJSTS_K_E_MAX = 115940.25
#: Gauss-Legendre quadrature points to match VNLJSTS system
_VNLJSTS_N_GL = 15
#: Integration shift constant to match VNLJSTS system
_VNLJSTS_CD1 = numpy.exp(3.5) / 2500

#: Extra parameters for testing (use debug_repl=True for debugging)
_LAMMPS_KWARGS = {"debug_repl": False}

def _make_vnljsts():
    """
    Specifies an initial configuration for the VNLJSTS test case.
    """

    return crystal.CellTools.tile(crystal.CellTools.scale_to(structure.Unit.fcc(), 1.28), (4, 4, 4)), \
        lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(2.7,), modifications=("shift", "yes"))

def _run_test(name, method, expect):
    """
    Runs and times a test for the :py:mod:`pyphase.einstein` routines.
    """

    try:
        print(file=sys.stderr)

        # Run the test itself
        wall_start, proc_start = time.perf_counter(), time.process_time()
        result, cache = method()
        wall_elapsed, proc_elapsed = time.perf_counter() - wall_start, time.process_time() - proc_start
        
        # Write to cache file for later inspection
        with open(os.path.join(_test_utilities.ensure_cache_path(), "{}.cache".format(name)), "wb") as cache_file:
            pickle.dump(cache, cache_file)

        # Display output
        print("{} = {}".format(name, "{} +/- {}".format(*result) if type(result) is tuple else result), file=sys.stderr)
        if expect(result):
            print("    Test \x1b[1;32mreturned expected result\x1b[0m", file=sys.stderr, end="")
            success = True
        else:
            print("    Test \x1b[1;33mreturned unexpected result\x1b[0m", file=sys.stderr, end="")
            success = False
        print(" (wall time {:.6f}s, process time {:.6f}s)".format(wall_elapsed, proc_elapsed), file=sys.stderr)

    except Exception as exception:
        traceback.print_exc()
        message = str(exception)
        print("\n    Test \x1b[1;31mfailed ({}{}{})\x1b[0m".format(
            type(exception).__name__, ": " if message else "", message), file=sys.stderr)
        success = False

    return success

class VNLJSTSTestCases:
    @staticmethod
    def test_reference_states():
        cell, pair = _make_vnljsts()

        cache = []

        ig_abs = einstein.calc_ig_a0(cell, _VNLJSTS_T, False, cache, **_LAMMPS_KWARGS)
        em_abs = einstein.calc_em_a0(cell, _VNLJSTS_T, _VNLJSTS_K_E_MAX, cell.atom_count(), 1, False, (0,), cache, **_LAMMPS_KWARGS)
        ex_abs = em_abs - ig_abs

        ig_off = einstein.calc_ig_a0(cell, _VNLJSTS_T, True, cache, **_LAMMPS_KWARGS)
        em_off = einstein.calc_em_a0(cell, _VNLJSTS_T, _VNLJSTS_K_E_MAX, cell.atom_count(), 1, True, (0,), cache, **_LAMMPS_KWARGS)
        ex_off = em_off - ig_off

        einstein.echo_cache(cache)
        return ex_abs - ex_off, cache

    test_reference_states.criterion = lambda value: numpy.isclose(value, 0)

    @staticmethod
    def test_calc_ig_a0():
        cell, pair = _make_vnljsts()

        cache = []

        energy = einstein.calc_ig_a0(cell, _VNLJSTS_T, True, cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return energy, cache

    test_calc_ig_a0.criterion = lambda value: numpy.isclose(value, -1.5062798441369485)

    @staticmethod
    def test_calc_em_a0():
        cell, pair = _make_vnljsts()

        cache = []

        energy = einstein.calc_em_a0(cell, _VNLJSTS_T, _VNLJSTS_K_E_MAX, cell.atom_count(), 1, True, (0,), cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return energy, cache

    test_calc_em_a0.criterion = lambda value: numpy.isclose(value, 27.28, atol=0.01)
    
    @staticmethod
    def test_calc_em_a1():
        cell, pair = _make_vnljsts()

        cache = []

        energy = relax.energy(cell, pair, **_LAMMPS_KWARGS)
        delta = einstein.run_em_a1(cell, pair, _VNLJSTS_DT, *_VNLJSTS_STEPS, _VNLJSTS_N_DUMP, _VNLJSTS_T,
            _VNLJSTS_TAU_DAMP, _VNLJSTS_K_E_MAX, energy, (0,), cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return delta, cache

    test_calc_em_a1.criterion = lambda value: numpy.isclose(value[0], -6.28, atol=0.02)
    
    @staticmethod
    def test_call_with_atom_style_charge():
        cell, pair = _make_vnljsts()

        cache = []

        energy = relax.energy(cell, pair, **_LAMMPS_KWARGS, atom_style="charge")
        delta = einstein.run_em_a1(cell, pair, _VNLJSTS_DT, *_VNLJSTS_STEPS, _VNLJSTS_N_DUMP, _VNLJSTS_T,
            _VNLJSTS_TAU_DAMP, _VNLJSTS_K_E_MAX, energy, (0,), cache, **_LAMMPS_KWARGS, atom_style="charge")

        einstein.echo_cache(cache)
        return delta, cache

    test_call_with_atom_style_charge.criterion = lambda value: numpy.isclose(value[0], -6.28, atol=0.02)

    @staticmethod
    def test_integrate_em_a2_parallel():
        cell, pair = _make_vnljsts()

        cache = []

        delta = einstein.integrate_em_a2(cell, pair, _VNLJSTS_DT, *_VNLJSTS_STEPS, _VNLJSTS_T, _VNLJSTS_TAU_DAMP,
            _VNLJSTS_K_E_MAX, _VNLJSTS_N_GL, _test_utilities.core_count(), _VNLJSTS_CD1, False, (0,), cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return delta, cache

    test_integrate_em_a2_parallel.criterion = lambda value: numpy.isclose(value[0], -14.8, atol=0.06)

    @staticmethod
    def test_integrate_em_a2_serial():
        cell, pair = _make_vnljsts()

        cache = []

        delta = einstein.integrate_em_a2(cell, pair, _VNLJSTS_DT, *_VNLJSTS_STEPS, _VNLJSTS_T, _VNLJSTS_TAU_DAMP,
            _VNLJSTS_K_E_MAX, _VNLJSTS_N_GL, 1, _VNLJSTS_CD1, False, (0,), cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return delta, cache

    test_integrate_em_a2_serial.criterion = lambda value: numpy.isclose(value[0], -14.8, atol=0.06)
    
    @staticmethod
    def test_run_em():
        cell, pair = _make_vnljsts()

        cache = []

        energy = relax.energy(cell, pair, **_LAMMPS_KWARGS)
        a = einstein.run_em(cell, pair, _VNLJSTS_DT, *_VNLJSTS_STEPS, _VNLJSTS_N_DUMP, _VNLJSTS_T, _VNLJSTS_TAU_DAMP,
            _VNLJSTS_K_E_MAX, energy, _VNLJSTS_N_GL, _test_utilities.core_count(), _VNLJSTS_CD1, cell.atom_count(), 1,
            einstein.ReferenceMode.WAVELENGTH_UNITY, False, (0,), cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return a, cache

    test_run_em.criterion = lambda value: numpy.isclose(value[0], 6.18, atol=0.08)
    
    @staticmethod
    def test_helmholtz():
        cell, pair = _make_vnljsts()

        cache = []

        a = einstein.helmholtz(cell, pair, _VNLJSTS_T, _test_utilities.core_count(), cell.atom_count(), 1,
            einstein.ReferenceMode.WAVELENGTH_UNITY, False, (0,), cache, **_LAMMPS_KWARGS)

        einstein.echo_cache(cache)
        return a, cache

    test_helmholtz.criterion = lambda value: numpy.isclose(value[0], 6.18, atol=0.08)

# Regression tests: these free energy values do not come from literature and are not verified
# through another method, but these tests should alert if something changes and breaks

_REGRESSION_PAIR = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1/2, 2**(5/6)),
    (2, 2): (1/2, 2**(5/6)), (1, 2): (1, 2**(5/6))}, style_args=(3*2**(5/6),))
_REGRESSION_CELLS = {
    "2_periodic_ab": crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Unit.alternating(), (10, 5)), 2),
    "2_crystallite_ab": einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
        structure.Unit.alternating(), (10, 5)), 2, move_atoms=False), 2), (False, False)),
    "2_slab_ab": einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale(crystal.CellTools.tile(
        structure.Unit.alternating(), (10, 5)), numpy.diag([10, 20]), move_atoms=False), 2), (True, False)),
    "3_periodic_ab": crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Primitive.CuAu(), (5, 5, 3)), 2),
    "3_crystallite_ab": einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
        structure.Primitive.CuAu(), (5, 5, 3)), 2, move_atoms=False), 2), (False, False, False)),
    "3_slab_ab": einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale(crystal.CellTools.tile(
        structure.Primitive.CuAu(), (5, 5, 3)), numpy.diag([5, 5, 10]), move_atoms=False), 2), (True, True, False))
}

class RegressionTestCases:
    @staticmethod
    def test_2_periodic():
        cache = []
        a = einstein.helmholtz(_REGRESSION_CELLS["2_periodic_ab"], _REGRESSION_PAIR, 1/20,
            _test_utilities.core_count(), 50, 1, einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache)
        return a, cache
    test_2_periodic.criterion = lambda value: numpy.isclose(value[0], -2.4706, atol=0.0003*5)

    @staticmethod
    def test_2_crystallite():
        cache = []
        a = einstein.helmholtz(_REGRESSION_CELLS["2_crystallite_ab"], _REGRESSION_PAIR, 1/20,
            _test_utilities.core_count(), 1, 2, einstein.ReferenceMode.PLANCK_UNITY, False, (0, 45), cache)
        return a, cache
    test_2_crystallite.criterion = lambda value: numpy.isclose(value[0], -2.0879, atol=0.0003*5)

    @staticmethod
    def test_2_slab():
        cache = []
        a = einstein.helmholtz(_REGRESSION_CELLS["2_slab_ab"], _REGRESSION_PAIR, 1/20,
            _test_utilities.core_count(), 5, 1, einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache)
        return a, cache
    test_2_slab.criterion = lambda value: numpy.isclose(value[0], -2.2453, atol=0.0003*5)

    @staticmethod
    def test_3_periodic():
        cache = []
        a = einstein.helmholtz(_REGRESSION_CELLS["3_periodic_ab"], _REGRESSION_PAIR, 1/20,
            _test_utilities.core_count(), 75, 1, einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache)
        return a, cache
    test_3_periodic.criterion = lambda value: numpy.isclose(value[0], -6.0071, atol=0.0004*5)

    @staticmethod
    def test_3_crystallite():
        cache = []
        # Set ESTIMATE_CONSTANT to non-zero value to prevent surface particles from "walking around"
        a = einstein.helmholtz(_REGRESSION_CELLS["3_crystallite_ab"], _REGRESSION_PAIR, 1/20,
            _test_utilities.core_count(), 1, 2, einstein.ReferenceMode.PLANCK_UNITY, False, (0, 60, 12), cache,
            overrides=dict(ESTIMATE_CONSTANT=0.1))
        return a, cache
    test_3_crystallite.criterion = lambda value: numpy.isclose(value[0], -3.9158, atol=0.0021*5)
    
    @staticmethod
    def test_3_slab():
        cache = []
        # Set ESTIMATE_CONSTANT to non-zero value to prevent surface particles from "walking around"
        a = einstein.helmholtz(_REGRESSION_CELLS["3_slab_ab"], _REGRESSION_PAIR, 1/20,
            _test_utilities.core_count(), 1, 2, einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache,
            overrides=dict(ESTIMATE_CONSTANT=0.1))
        return a, cache
    test_3_slab.criterion = lambda value: numpy.isclose(value[0], -5.0801, atol=0.0002*5)

def make_einstein_tests(*classes):
    test_methods = {}
    for test_class in classes:
        for key, static_method in test_class.__dict__.items():
            if key.startswith("test"):
                test_name = "test_{}_{}".format(test_class.__name__, key[len("test_"):])
                def test_method(self, name=test_name, function=static_method):
                    self.assertTrue(_run_test(name, function.__func__, function.criterion))
                test_methods[test_name] = test_method
    return type("EinsteinCaseTests", (unittest.TestCase,), test_methods)

EinsteinCaseTests = make_einstein_tests(VNLJSTSTestCases, RegressionTestCases)

class EinsteinTests(unittest.TestCase):
    def test_with_periodic(self):
        periodicity_1 = numpy.array([False, True, True, False])
        periodicity_2 = numpy.array([False, False, False, True])
        cell = crystal.Cell(numpy.eye(4), [numpy.ones((4, 4))] * 3, periodic=periodicity_1)
        
        # Assign all periodicities together
        self.assertTrue(numpy.all(einstein.with_periodic(cell, True).periodic))
        self.assertTrue(numpy.all(~einstein.with_periodic(cell, False).periodic))

        # Assign each periodicity individually
        self.assertTrue(numpy.all(einstein.with_periodic(cell, periodicity_2).periodic == periodicity_2))

    def test_calc_rot_term_none(self):
        # Some dimensionalities should have no rotational contribution
        self.assertTrue(numpy.isclose(numpy.exp(einstein.calc_rot_term(0)), 1))
        self.assertTrue(numpy.isclose(numpy.exp(einstein.calc_rot_term(1)), 1))

    def test_calc_rot_term_verify(self):
        # Some dimensionalities should have the correct rotational contribution to the partition function
        self.assertTrue(numpy.isclose(numpy.exp(einstein.calc_rot_term(2)), 2 * numpy.pi))
        self.assertTrue(numpy.isclose(numpy.exp(einstein.calc_rot_term(3)), 8 * numpy.pi ** 2))

    def test_calc_principal_moments_none(self):
        # Some cases should give no moments
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(2), ([[4, 4], [6, 6], [4, 6], [6, 4]],), periodic=[False, True]))
        self.assertEqual(len(moments), 0)
        
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(2), ([[4, 4], [6, 6], [4, 6], [6, 4]],)))
        self.assertEqual(len(moments), 0)

        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), ([[4, 4, 4], [6, 6, 4], [4, 6, 6], [6, 4, 6]],), periodic=[False, True, True]))
        self.assertEqual(len(moments), 0)
        
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), ([[4, 4, 4], [6, 6, 4], [4, 6, 6], [6, 4, 6]],)))
        self.assertEqual(len(moments), 0)
        
    def test_calc_principal_moments_2(self):
        # Reference cases in two dimensions
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(2), ([[5, 4], [5, 6]],), periodic=[False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [2])))

        l = numpy.sqrt(2) / 2
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(2), ([[5 - l, 5 - l], [5 + l, 5 + l]],), periodic=[False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [2])))
        
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(2), ([[5, 3], [5, 7]],), periodic=[False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [8])))
        
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(2), ([[5, 3], [5, 5], [5, 7]],), periodic=[False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [8])))

    def test_calc_principal_moments_3(self):
        # Reference cases in three dimensions
        # Tetrahedral molecule
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), ([[5, 5, 5], [4, 4, 4], [6, 6, 4], [4, 6, 6], [6, 4, 6]],),
            periodic=[False, False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [8, 8, 8])))

        # Octahedral molecule
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), ([[5, 5, 5], [6, 5, 5], [4, 5, 5], [5, 6, 5], [5, 4, 5], [5, 5, 6], [5, 5, 4]],),
            periodic=[False, False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [4, 4, 4])))

        # Octahedral molecule: not symmetrical
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), ([[5, 5, 5], [6, 5, 5], [4, 5, 5], [5, 7, 5], [5, 3, 5], [5, 5, 8], [5, 5, 2]],),
            periodic=[False, False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, [10, 20, 26])))

    def test_calc_principal_moments_shift(self):
        # Shifting everything by center of mass or adding particle types should not change rotational properties
        array = numpy.array([[5, 5, 5], [6, 5, 5], [4, 5, 5], [5, 7, 5], [5, 3, 5], [5, 5, 8], [5, 5, 2]])
        moments, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), (array,),
            periodic=[False, False, False]))
        moments_shifted, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), ([[6, 7, 8]], array + [1, 2, 3]),
            periodic=[False, False, False]))
        self.assertTrue(numpy.all(numpy.isclose(moments, moments_shifted)))

    def test_calc_principal_moments_reduce(self):
        # Make sure periodic dimensions are reduced down
        array = numpy.array([[5, 5, 5], [6, 5, 5], [4, 5, 5], [5, 7, 5], [5, 3, 5], [5, 5, 8], [5, 5, 2]])
        moments_x, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), (array,), periodic=[True, False, False]))
        moments_y, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), (array,), periodic=[False, True, False]))
        moments_z, _ = einstein.calc_principal_moments(crystal.Cell(10 * numpy.eye(3), (array,), periodic=[False, False, True]))

        self.assertTrue(numpy.all(numpy.isclose(moments_x, [26])))
        self.assertTrue(numpy.all(numpy.isclose(moments_y, [20])))
        self.assertTrue(numpy.all(numpy.isclose(moments_z, [10])))
    
    def test_calc_trans_rot_2(self):
        cell = crystal.Cell(10 * numpy.eye(2), (numpy.array([[5, 5], [4, 5], [6, 5], [5, 4], [5, 6]]),), periodic=[False, False])
        T, trans_symm, rot_symm = 0.12, 34, 56

        lambda_ = 1 / numpy.sqrt(2 * numpy.pi * T)
        Q_t_no_lambda = 100 * 5 / trans_symm
        Q_r_no_lambda = 2 * numpy.pi * numpy.sqrt(numpy.prod(einstein.calc_principal_moments(cell)[0])) / rot_symm

        # Lambda of one particle is 1
        A_t_calc, A_r_calc = einstein.calc_trans_rot(cell, T, trans_symm, rot_symm, True)
        self.assertTrue(numpy.isclose(A_t_calc * 5, -T * numpy.log(Q_t_no_lambda)))
        self.assertTrue(numpy.isclose(A_r_calc * 5, -T * numpy.log(Q_r_no_lambda)))
        
        # Lambda of one particle is true value in natural units (h = 1)
        A_t_calc, A_r_calc = einstein.calc_trans_rot(cell, T, trans_symm, rot_symm, False)
        self.assertTrue(numpy.isclose(A_t_calc * 5, -T * numpy.log(Q_t_no_lambda / lambda_ ** 2)))
        self.assertTrue(numpy.isclose(A_r_calc * 5, -T * numpy.log(Q_r_no_lambda / lambda_)))

    def test_calc_trans_rot_3(self):
        array = numpy.array([[5, 5, 5], [6, 5, 5], [4, 5, 5], [5, 7, 5], [5, 3, 5], [5, 5, 8], [5, 5, 2]])
        cell = crystal.Cell(10 * numpy.eye(3), (array,), periodic=[False, False, False])
        T, trans_symm, rot_symm = 0.12, 34, 56

        lambda_3 = (2 * numpy.pi * T) ** (-3 / 2)
        Q_t_no_lambda = 1000 * numpy.sqrt(343) / trans_symm
        Q_r_no_lambda = 8 * numpy.pi ** 2 * numpy.sqrt(numpy.prod(einstein.calc_principal_moments(cell)[0])) / rot_symm

        # Lambda of one particle is 1
        A_t_calc, A_r_calc = einstein.calc_trans_rot(cell, T, trans_symm, rot_symm, True)
        self.assertTrue(numpy.isclose(A_t_calc * 7, -T * numpy.log(Q_t_no_lambda)))
        self.assertTrue(numpy.isclose(A_r_calc * 7, -T * numpy.log(Q_r_no_lambda)))
        
        # Lambda of one particle is true value in natural units (h = 1)
        A_t_calc, A_r_calc = einstein.calc_trans_rot(cell, T, trans_symm, rot_symm, False)
        self.assertTrue(numpy.isclose(A_t_calc * 7, -T * numpy.log(Q_t_no_lambda / lambda_3)))
        self.assertTrue(numpy.isclose(A_r_calc * 7, -T * numpy.log(Q_r_no_lambda / lambda_3)))

    def test_calc_em_a0_2_periodic(self):
        # Square lattice, N=10x10, sigma=2
        cell = crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Primitive.square(), (10, 10)), 2)
        T = 1.234
        k = 98.76

        lambda_200 = 1 / (2 * numpy.pi * T) ** 100
        Q_no_lambda = 4 * (2 * numpy.pi * T / k) ** 99

        A_calc = einstein.calc_em_a0(cell, T, k, 100, 1, True, (11,), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 100, 1, False, (11,), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda / lambda_200)))
    
    def test_calc_em_a0_2_crystallite(self):
        # Square lattice, N=10x10, sigma=2, crystallite in box 2x2 times crystallite size
        cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
            structure.Primitive.square(), (10, 10)), 2), 2, move_atoms=False), False)
        T = 1.234
        k = 98.76

        lambda_200 = 1 / (2 * numpy.pi * T) ** 100
        Q_no_lambda = (1600 * numpy.sqrt(5) * numpy.pi) * (2 * numpy.pi * T / k) ** (197 / 2)

        # Particle 11 is at (2, 2) and 23 at (4, 6)
        A_calc = einstein.calc_em_a0(cell, T, k, 1, 4, True, (11, 23), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 1, 4, False, (11, 23), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda / lambda_200)))
    
    def test_calc_em_a0_2_slab(self):
        # Square lattice, N=10x10, sigma=2, slab in box 1x2 times crystallite size
        cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale(crystal.CellTools.tile(
            structure.Primitive.square(), (10, 10)), numpy.diag([10, 20])), 2, move_atoms=False), (True, False))
        T = 1.234
        k = 98.76

        lambda_200 = 1 / (2 * numpy.pi * T) ** 100
        Q_no_lambda = 80 * (2 * numpy.pi * T / k) ** 99

        A_calc = einstein.calc_em_a0(cell, T, k, 10, 1, True, (11,), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 10, 1, False, (11,), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda / lambda_200)))

    def test_calc_em_a0_2_crystallite_binary(self):
        # Square lattice, N=10x10, sigma=2, crystallite in box 2x2 times crystallite size
        cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
            structure.Unit.square_ab(), (5, 5)), 2), 2, move_atoms=False), False)
        T = 1.234
        k = 98.76

        lambda_200 = 1 / (2 * numpy.pi * T) ** 100
        Q_no_lambda = (3200 * numpy.sqrt(5) * numpy.pi) * (2 * numpy.pi * T / k) ** (197 / 2)

        # Particle 25 is at (2, 2) and 81 at (4, 6)
        A_calc = einstein.calc_em_a0(cell, T, k, 1, 2, True, (25, 81), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 1, 2, False, (25, 81), [])
        self.assertTrue(numpy.isclose(A_calc * 100, -T * numpy.log(Q_no_lambda / lambda_200)))
    
    def test_calc_em_a0_3_periodic(self):
        # Simple cubic lattice, N=5x5x5, sigma=2
        cell = crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Primitive.simple_cubic(), (5, 5, 5)), 2)
        T = 1.234
        k = 98.76

        lambda_375 = 1 / (2 * numpy.pi * T) ** (375 / 2)
        Q_no_lambda = 8 * (2 * numpy.pi * T / k) ** 186

        A_calc = einstein.calc_em_a0(cell, T, k, 125, 1, True, (56,), [])
        self.assertTrue(numpy.isclose(A_calc * 125, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 125, 1, False, (56,), [])
        self.assertTrue(numpy.isclose(A_calc * 125, -T * numpy.log(Q_no_lambda / lambda_375)))

    def test_calc_em_a0_3_crystallite(self):
        # Simple cubic lattice, N=5x5x5, sigma=2, crystallite in box 2x2x2 times crystallite size
        cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
            structure.Primitive.simple_cubic(), (5, 5, 5)), 2), 2, move_atoms=False), False)
        T = 1.234
        k = 98.76

        lambda_375 = 1 / (2 * numpy.pi * T) ** (375 / 2)
        Q_no_lambda = (64000 * numpy.sqrt(42) * numpy.pi ** 2 / 3) * (2 * numpy.pi * T / k) ** (369 / 2)

        # Particle 56 is at (4, 2, 2), 87 is at (6, 4, 4) and 42 is at (2, 6, 4)
        A_calc = einstein.calc_em_a0(cell, T, k, 1, 24, True, (56, 87, 42), [])
        self.assertTrue(numpy.isclose(A_calc * 125, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 1, 24, False, (56, 87, 42), [])
        self.assertTrue(numpy.isclose(A_calc * 125, -T * numpy.log(Q_no_lambda / lambda_375)))

    def test_calc_em_a0_3_slab(self):
        # Simple cubic lattice, N=5x5x5, sigma=2, slab in box 1x1x2 times crystallite size
        cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale(crystal.CellTools.tile(
            structure.Primitive.simple_cubic(), (5, 5, 5)), numpy.diag([5, 5, 10])), 2, move_atoms=False), (True, True, False))
        T = 1.234
        k = 98.76

        lambda_375 = 1 / (2 * numpy.pi * T) ** (375 / 2)
        Q_no_lambda = 80 * (2 * numpy.pi * T / k) ** 186

        A_calc = einstein.calc_em_a0(cell, T, k, 25, 1, True, (56,), [])
        self.assertTrue(numpy.isclose(A_calc * 125, -T * numpy.log(Q_no_lambda)))
        A_calc = einstein.calc_em_a0(cell, T, k, 25, 1, False, (56,), [])
        self.assertTrue(numpy.isclose(A_calc * 125, -T * numpy.log(Q_no_lambda / lambda_375)))

    def test_prepare_constrain_2_periodic(self):
        # Square lattice, N=10x10, sigma=2
        in_cell = crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Unit.square_ab(), (5, 5)), 2)
        constrain = (25,)
        test_pair = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1.23, 1.7), (2, 2): (3.45, 1.8), (1, 2): (4.65, 1.94)}, style_args=(5.5,))

        dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, out_cell, out_dists = einstein.prepare_constrain(in_cell, constrain)
        self.assertEqual((dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof), (2, 2, 0, 200, 2, 0, 198))
        # Easiest way to check equivalence up to translation and rotation is to do energy evaluation
        self.assertTrue(numpy.isclose(relax.energy(in_cell, test_pair), relax.energy(out_cell, test_pair)))

        # Cell should not be rotated
        in_atoms = numpy.concatenate(in_cell.atom_lists)
        out_atoms = numpy.concatenate(out_cell.atom_lists)
        self.assertEqual(out_dists.shape, (0,))
        self.assertTrue(numpy.all(numpy.isclose(in_atoms[1:] - in_atoms[0], out_atoms[1:] - out_atoms[0])))
        self.assertTrue(numpy.all(numpy.isclose(out_atoms[constrain], 0)))
    
    def test_prepare_constrain_2_crystallite(self):
        # Square lattice, N=10x10, sigma=2, crystallite in box 2x2 times crystallite size
        in_cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
            structure.Unit.square_ab(), (5, 5)), 2), 2, move_atoms=False), False)
        constrain = (25, 81)
        test_pair = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1.23, 1.7), (2, 2): (3.45, 1.8), (1, 2): (4.65, 1.94)}, style_args=(5.5,))
        
        dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, out_cell, out_dists = einstein.prepare_constrain(in_cell, constrain)
        self.assertEqual((dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof), (2, 0, 2, 200, 2, 1, 197))
        # Easiest way to check equivalence up to translation and rotation is to do energy evaluation
        self.assertTrue(numpy.isclose(relax.energy(in_cell, test_pair), relax.energy(out_cell, test_pair)))

        # Cell should be rotated correctly
        out_atoms = numpy.concatenate(out_cell.atom_lists)
        self.assertTrue(numpy.all(numpy.isclose(out_dists, [out_atoms[constrain[1], 0]])))
        self.assertTrue(numpy.all(numpy.isclose(out_atoms[constrain[0]], 0)))
        self.assertFalse(numpy.isclose(out_atoms[constrain[1], 0], 0))
        self.assertTrue(numpy.isclose(out_atoms[constrain[1], 1], 0))
    
    def test_prepare_constrain_2_slab(self):
        # Square lattice, N=10x10, sigma=2, slab in box 1x2 times crystallite size
        in_cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale(crystal.CellTools.tile(
            structure.Unit.square_ab(), (5, 5)), numpy.diag([10, 20])), 2, move_atoms=False), (True, False))
        constrain = (25,)
        test_pair = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1.23, 1.7), (2, 2): (3.45, 1.8), (1, 2): (4.65, 1.94)}, style_args=(5.5,))

        dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, out_cell, out_dists = einstein.prepare_constrain(in_cell, constrain)
        self.assertEqual((dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof), (2, 1, 1, 200, 2, 0, 198))
        # Easiest way to check equivalence up to translation and rotation is to do energy evaluation
        self.assertTrue(numpy.isclose(relax.energy(in_cell, test_pair), relax.energy(out_cell, test_pair)))

        # Cell should not be rotated
        in_atoms = numpy.concatenate(in_cell.atom_lists)
        out_atoms = numpy.concatenate(out_cell.atom_lists)
        self.assertEqual(out_dists.shape, (0,))
        self.assertTrue(numpy.all(numpy.isclose(in_atoms[1:] - in_atoms[0], out_atoms[1:] - out_atoms[0])))
        self.assertTrue(numpy.all(numpy.isclose(out_atoms[constrain], 0)))

    def test_prepare_constrain_3_periodic(self):
        # Simple cubic lattice, N=10x10x10, sigma=2
        in_cell = crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Unit.NaCl(), (5, 5, 5)), 2)
        constrain = (123,)
        test_pair = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1.23, 1.7), (2, 2): (3.45, 1.8), (1, 2): (4.65, 1.94)}, style_args=(5.5,))

        dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, out_cell, out_dists = einstein.prepare_constrain(in_cell, constrain)
        self.assertEqual((dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof), (3, 3, 0, 3000, 3, 0, 2997))
        # Easiest way to check equivalence up to translation and rotation is to do energy evaluation
        self.assertTrue(numpy.isclose(relax.energy(in_cell, test_pair), relax.energy(out_cell, test_pair)))

        # Cell should not be rotated
        in_atoms = numpy.concatenate(in_cell.atom_lists)
        out_atoms = numpy.concatenate(out_cell.atom_lists)
        self.assertEqual(out_dists.shape, (0,))
        self.assertTrue(numpy.all(numpy.isclose(in_atoms[1:] - in_atoms[0], out_atoms[1:] - out_atoms[0])))
        self.assertTrue(numpy.all(numpy.isclose(out_atoms[constrain], 0)))

    def test_prepare_constrain_3_crystallite(self):
        # Simple cubic lattice, N=10x10x10, sigma=2, crystallite in box 2x2x2 times system size
        in_cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale_by(crystal.CellTools.tile(
            structure.Unit.NaCl(), (5, 5, 5)), 2), 2, move_atoms=False), False)
        constrain = (123, 345, 567)
        test_pair = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1.23, 1.7), (2, 2): (3.45, 1.8), (1, 2): (4.65, 1.94)}, style_args=(5.5,))

        dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, out_cell, out_dists = einstein.prepare_constrain(in_cell, constrain)
        self.assertEqual((dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof), (3, 0, 3, 3000, 3, 3, 2994))
        # Easiest way to check equivalence up to translation and rotation is to do energy evaluation
        self.assertTrue(numpy.isclose(relax.energy(in_cell, test_pair), relax.energy(out_cell, test_pair)))

        # Cell should be rotated correctly
        out_atoms = numpy.concatenate(out_cell.atom_lists)
        self.assertTrue(numpy.all(numpy.isclose(out_dists, [out_atoms[constrain[2], 1], out_atoms[constrain[1], 0]])))
        self.assertTrue(numpy.all(numpy.isclose(out_atoms[constrain[0]], 0)))
        self.assertFalse(numpy.isclose(out_atoms[constrain[1], 0], 0))
        self.assertTrue(numpy.isclose(out_atoms[constrain[1], 1], 0))
        self.assertFalse(numpy.isclose(out_atoms[constrain[2], 0], 0))
        self.assertFalse(numpy.isclose(out_atoms[constrain[2], 1], 0))
        self.assertTrue(numpy.isclose(out_atoms[constrain[2], 2], 0))

    def test_prepare_constrain_3_slab(self):
        # Simple cubic lattice, N=10x10x10, sigma=2, slab in box 2x1x1 times system size
        in_cell = einstein.with_periodic(crystal.CellTools.scale_by(crystal.CellTools.scale(crystal.CellTools.tile(
            structure.Unit.NaCl(), (5, 5, 5)), numpy.diag([5, 5, 10])), 2, move_atoms=False), (True, True, False))
        constrain = (123,)
        test_pair = lammps.LAMMPSPair("lj/cut", 2, {(1, 1): (1.23, 1.7), (2, 2): (3.45, 1.8), (1, 2): (4.65, 1.94)}, style_args=(5.5,))

        dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, out_cell, out_dists = einstein.prepare_constrain(in_cell, constrain)
        self.assertEqual((dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof), (3, 2, 1, 3000, 3, 0, 2997))
        # Easiest way to check equivalence up to translation and rotation is to do energy evaluation
        self.assertTrue(numpy.isclose(relax.energy(in_cell, test_pair), relax.energy(out_cell, test_pair)))

        # Cell should not be rotated
        in_atoms = numpy.concatenate(in_cell.atom_lists)
        out_atoms = numpy.concatenate(out_cell.atom_lists)
        self.assertEqual(out_dists.shape, (0,))
        self.assertTrue(numpy.all(numpy.isclose(in_atoms[1:] - in_atoms[0], out_atoms[1:] - out_atoms[0])))
        self.assertTrue(numpy.all(numpy.isclose(out_atoms[constrain], 0)))

# pyphase_test.test_structure: Evan Pretti

import numpy
import unittest

from pyphase import crystal
from pyphase import structure

# Information on generated cells, along with the expected number of
# dimensions, components, and whether or not the cell must be orthorhombic
_GENERATED = [
    (structure.Primitive.open_honeycomb(), 2, 1, False),
    (structure.Primitive.open_honeycomb_ab(), 2, 2, False),
    (structure.Primitive.square(), 2, 1, False),
    (structure.Primitive.square_ab(), 2, 2, False),
    (structure.Primitive.rhombic(), 2, 1, False),
    (structure.Primitive.rhombic_ab(), 2, 2, False),
    (structure.Primitive.hexagonal(), 2, 1, False),
    (structure.Primitive.alternating(), 2, 2, False),
    (structure.Primitive.honeycomb(), 2, 2, False),
    (structure.Primitive.kagome(), 2, 2, False),
    (structure.Primitive.square_kagome(), 2, 2, False),
    (structure.Primitive.simple_cubic(), 3, 1, False),
    (structure.Primitive.CsCl(), 3, 2, False),
    (structure.Primitive.CuAu(), 3, 2, False),
    (structure.Unit.alternating(), 2, 2, True),
    (structure.Unit.square_ab(), 2, 2, True),
    (structure.Unit.fcc(), 3, 1, True),
    (structure.Unit.NaCl(), 3, 2, True)
]

class GenerationTests(unittest.TestCase):
    def test_dimensions(self):
        # Ensure that generated cells have the correct number of dimensions
        for cell, dimensions, _, _ in _GENERATED:
            self.assertEqual(cell.dimensions, dimensions)

    def test_components(self):
        # Ensure that generated cells have the correct number of atom types
        for cell, _, components, _ in _GENERATED:
            self.assertEqual(cell.atom_types, components)

    def test_orthorhombic(self):
        # Ensure that generated cells are orthorhombic if necessary
        for cell, _, _, orthorhombic in _GENERATED:
            if orthorhombic:
                vectors = crystal.CellTools.normalize(cell).vectors
                self.assertTrue(numpy.all(numpy.isclose(vectors, numpy.diag(numpy.diag(vectors)))))

_METRICS = [
    structure.Cluster.minkowski(3),
    structure.Cluster.minkowski(4),
    structure.Cluster.spherical(),
    structure.Cluster.rectangular(),
    structure.Cluster.diamond(),
    structure.Cluster.polygon(3, rotate=False),
    structure.Cluster.polygon(4, rotate=False),
    structure.Cluster.polygon(5, rotate=False),
    structure.Cluster.polygon(3, rotate=True),
    structure.Cluster.polygon(4, rotate=True),
    structure.Cluster.polygon(5, rotate=True)]

class ClusterTests(unittest.TestCase):
    def test_min_stoich(self):
        # Check that stoichiometric ratios are reduced properly
        self.assertEqual(structure.Cluster.min_stoich([1, 3]), [1, 3])
        self.assertEqual(structure.Cluster.min_stoich([8, 8]), [1, 1])
        self.assertEqual(structure.Cluster.min_stoich([8, 12]), [2, 3])
        self.assertEqual(structure.Cluster.min_stoich([8, 13]), [8, 13])
        self.assertEqual(structure.Cluster.min_stoich([8, 14]), [4, 7])

    def test_make_radius(self):
        # Ensures that the maximum desired radius is never exceeded
        for cell, _, _, _ in _GENERATED:
            for radius in (3, 5, 7):
                metric = structure.Cluster.spherical()
                cluster = structure.Cluster.make(cell, metric, 0.1, radius=radius)

                # Filtering with metric is based on center of box, not center of mass
                box_center = numpy.sum(cluster.vectors, axis=0) / 2
                max_metric = max(metric(atom - box_center) for atom_list in cluster.atom_lists for atom in atom_list)
                self.assertTrue(max_metric < radius or numpy.isclose(max_metric, radius))

                # Center of mass should not be more than a unit out from center of box
                mass_center = numpy.sum([numpy.sum(atom_list, axis=0) for atom_list in cluster.atom_lists], axis=0) / cluster.atom_count()
                self.assertLessEqual(metric(mass_center - box_center), 1)

    def test_make_count(self):
        # Ensures that the maximum desired count is never exceeded
        for cell, _, _, _ in _GENERATED:
            for count in (100, 256, 307):
                metric = structure.Cluster.spherical()
                cluster = structure.Cluster.make(cell, metric, 0.1, count=count)

                # There may be less than the desired number of atoms due to stoichiometric
                # constraints: this will be tested in a different routine
                self.assertGreaterEqual(cluster.atom_count(), count - cell.atom_count())
                self.assertLessEqual(cluster.atom_count(), count)
                
                # Center of mass should not be more than a unit out from center of box
                box_center = numpy.sum(cluster.vectors, axis=0) / 2
                mass_center = numpy.sum([numpy.sum(atom_list, axis=0) for atom_list in cluster.atom_lists], axis=0) / cluster.atom_count()
                self.assertLessEqual(metric(mass_center - box_center), 1)

    def test_make_radius_stoichiometry(self):
        # Ensures that stoichiometric ratio is maintained when radius is constrained
        for cell, _, _, _ in _GENERATED:
            for radius in (3, 5, 7):
                metric = structure.Cluster.spherical()
                cluster = structure.Cluster.make(cell, metric, 0.1, radius=radius)

                cell_ratios = numpy.array([value / cell.atom_counts[0] for value in cell.atom_counts[1:]])
                cluster_ratios = numpy.array([value / cluster.atom_counts[0] for value in cluster.atom_counts[1:]])
                self.assertTrue(numpy.all(numpy.isclose(cell_ratios, cluster_ratios)))

    def test_make_count_stoichiometry(self):
        # Ensures that stoichiometric ratio is maintained when radius is constrained
        for cell, _, _, _ in _GENERATED:
            for count in (100, 256, 307):
                metric = structure.Cluster.spherical()
                cluster = structure.Cluster.make(cell, metric, 0.1, count=count)

                cell_ratios = numpy.array([value / cell.atom_counts[0] for value in cell.atom_counts[1:]])
                cluster_ratios = numpy.array([value / cluster.atom_counts[0] for value in cluster.atom_counts[1:]])
                self.assertTrue(numpy.all(numpy.isclose(cell_ratios, cluster_ratios)))

    def test_make_radius_density(self):
        # Check that a density can be specified and is achieved in the box
        for cell, _, _, _ in _GENERATED:
            for density in [0.001, 0.01, 0.1]:
                self.assertTrue(numpy.isclose(density, structure.Cluster.make(cell, structure.Cluster.spherical(), density, radius=5).density))

    def test_make_count_density(self):
        for cell, _, _, _ in _GENERATED:
            for density in [0.001, 0.01, 0.1]:
                self.assertTrue(numpy.isclose(density, structure.Cluster.make(cell, structure.Cluster.spherical(), density, count=307).density))

    def test_make_radius_metric(self):
        # Ensures that the maximum desired radius is never exceeded
        radius = 5
        for cell, _, _, _ in _GENERATED:
            for metric in _METRICS:
                cluster = structure.Cluster.make(cell, metric, 0.1, radius=radius)

                # Filtering with metric is based on center of box, not center of mass
                box_center = numpy.sum(cluster.vectors, axis=0) / 2
                max_metric = max(metric(atom - box_center) for atom_list in cluster.atom_lists for atom in atom_list)
                self.assertTrue(max_metric < radius or numpy.isclose(max_metric, radius))

                # Center of mass should not be more than a unit out from center of box
                mass_center = numpy.sum([numpy.sum(atom_list, axis=0) for atom_list in cluster.atom_lists], axis=0) / cluster.atom_count()
                self.assertLessEqual(metric(mass_center - box_center), 1)

    def test_make_count_metric(self):
        # Ensures that the maximum desired count is never exceeded
        count = 307
        for cell, _, _, _ in _GENERATED:
            for metric in _METRICS:
                cluster = structure.Cluster.make(cell, metric, 0.1, count=count)

                # There may be less than the desired number of atoms due to stoichiometric
                # constraints: this will be tested in a different routine
                self.assertGreaterEqual(cluster.atom_count(), count - cell.atom_count())
                self.assertLessEqual(cluster.atom_count(), count)
                
                # Center of mass should not be more than a unit out from center of box
                box_center = numpy.sum(cluster.vectors, axis=0) / 2
                mass_center = numpy.sum([numpy.sum(atom_list, axis=0) for atom_list in cluster.atom_lists], axis=0) / cluster.atom_count()
                self.assertLessEqual(metric(mass_center - box_center), 1)

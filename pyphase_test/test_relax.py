# pyphase_test.test_relax: Evan Pretti

import numpy
import unittest

from pyphase import crystal
from pyphase import structure
from pyphase import lammps
from pyphase import relax
from . import test_lammps

class MeasureTests(unittest.TestCase):
    def test_energy_lammps(self):
        # FCC Lennard-Jones (LAMMPS implementation)
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        self.assertTrue(numpy.isclose(relax.energy(cell, lj), -8.129509137272153))

    def test_energy_aperiodic(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        cell_aperiodic = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[False] * 3)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        self.assertTrue(numpy.isclose(relax.energy(cell_aperiodic, lj), -5.904186151271344))

    def test_energy_aperiodic_partial(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        
        # Set up the cells
        cell_fff = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[False, False, False])
        cell_ffp = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[False, False, True])
        cell_fpf = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[False, True, False])
        cell_fpp = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[False, True, True])
        cell_pff = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[True, False, False])
        cell_pfp = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[True, False, True])
        cell_ppf = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[True, True, False])
        cell_ppp = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[True, True, True])

        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # Measure energies
        energy_fff = relax.energy(cell_fff, lj)
        energy_ffp = relax.energy(cell_ffp, lj)
        energy_fpf = relax.energy(cell_fpf, lj)
        energy_fpp = relax.energy(cell_fpp, lj)
        energy_pff = relax.energy(cell_pff, lj)
        energy_pfp = relax.energy(cell_pfp, lj)
        energy_ppf = relax.energy(cell_ppf, lj)
        energy_ppp = relax.energy(cell_ppp, lj)
        
        # Switching which directions are periodic vs. aperiodic should not change anything
        self.assertTrue(numpy.isclose(energy_ffp, energy_fpf))
        self.assertTrue(numpy.isclose(energy_ffp, energy_pff))
        self.assertTrue(numpy.isclose(energy_fpp, energy_pfp))
        self.assertTrue(numpy.isclose(energy_fpp, energy_ppf))
        
        # As periodic boundaries are added, energy should get lower
        self.assertGreater(energy_fff, energy_ffp)
        self.assertGreater(energy_ffp, energy_fpp)
        self.assertGreater(energy_fpp, energy_ppp)

    def test_energy_python(self):
        # FCC Lennard-Jones (Python implementation)
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.PythonPair(1, {(1, 1): lambda r: tuple(array[0] for array in \
            test_lammps._lj(numpy.array([r]), 1, 1, 4))}, 0.5, 3, 4096)

        # Energy will not match exactly from tabulation but should be close to within tolerance
        self.assertTrue(numpy.isclose(relax.energy(cell, lj), -8.129509137272153))

    def test_energy_binary(self):
        # 2D binary system example
        cell = crystal.CellTools.scale_by(crystal.CellTools.tile(structure.Unit.square_ab(), (8, 8)), 2 ** (1 / 6))
        lj = lammps.PythonPair(2, {(i, j): lambda r, i=i, j=j: tuple(array[0] * (0.5 if i == j else 1) for array in \
            test_lammps._lj(numpy.array([r]), 1, 1, 4)) for i, j in [(1, 1), (2, 2), (1, 2)]}, 0.5, 3, 4096)

        self.assertTrue(numpy.isclose(relax.energy(cell, lj), -2.329113140068386))
    
    def test_energy_relax(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # Relaxed energy without box move should be equal to unrelaxed energy
        self.assertTrue(numpy.isclose(
            relax.energy(cell, lj, relax_cell=True),
            relax.energy(cell, lj)))

    def test_energy_relax_box(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # Relaxed energy with box move should be lower as relaxed solid has density > 1
        self.assertLess(
            relax.energy(cell, lj, relax_cell=True, box_mode=relax.RelaxType.ISOTROPIC),
            relax.energy(cell, lj, relax_cell=True))

    def test_enthalpy(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # U + PV > U if P > 0
        self.assertGreater(
            relax.enthalpy(cell, lj, 1),
            relax.energy(cell, lj))

    def test_enthalpy_zero(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # U + PV = U if P = 0
        self.assertTrue(numpy.isclose(
            relax.enthalpy(cell, lj, 0),
            relax.energy(cell, lj)))

    def test_enthalpy_relax(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # Relaxed enthalpy with box move should be lower than without box move
        self.assertLess(
            relax.enthalpy(cell, lj, 10, relax_cell=True, box_mode=relax.RelaxType.ISOTROPIC),
            relax.enthalpy(cell, lj, 10, relax_cell=True))
    
class RelaxTests(unittest.TestCase):
    def test_no_step(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        # If the number of steps is set to zero, nothing should change
        energy = relax.energy(cell, lj)
        cell_out, energy_out = relax.relax(cell, lj, box_mode=relax.RelaxType.TRICLINIC, pressure=10, steps=0)
        self.assertTrue(crystal.CellTools.similar(cell, cell_out))
        self.assertTrue(numpy.isclose(energy, energy_out))

    def test_no_step_atom_style_charge(self):
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(structure.Unit.fcc(), (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        # If the number of steps is set to zero, nothing should change
        energy = relax.energy(cell, lj, atom_style="charge")
        cell_out, energy_out = relax.relax(cell, lj, box_mode=relax.RelaxType.TRICLINIC,
            pressure=10, steps=0, atom_style="charge")
        self.assertTrue(crystal.CellTools.similar(cell, cell_out))
        self.assertTrue(numpy.isclose(energy, energy_out))

    def test_frozen(self):
        # Shift one FCC atom off slightly
        fcc = structure.Unit.fcc()
        atoms = fcc.atom_lists[0]
        atoms[0] = numpy.array([0.01, 0.02, 0.03])
        fcc = crystal.Cell(fcc.vectors, [atoms])

        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(fcc, (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        energy = relax.energy(cell, lj)
        cell_out, energy_out = relax.relax(cell, lj, box_mode=relax.RelaxType.FROZEN)
        
        # Particles only should move to lower the energy
        self.assertFalse(crystal.CellTools.similar(cell, cell_out))
        self.assertTrue(numpy.allclose(cell.vectors, cell_out.vectors))
        self.assertLess(energy_out, energy)

    def test_aperiodic(self):
        # Shift one FCC atom off slightly
        fcc = structure.Unit.fcc()
        atoms = fcc.atom_lists[0]
        atoms[0] = numpy.array([0.01, 0.02, 0.03])
        fcc = crystal.Cell(fcc.vectors, [atoms])
        
        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(fcc, (4, 4, 4)), 1)
        cell_aperiodic = crystal.Cell(cell.vectors, cell.atom_lists, periodic=[False] * 3)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        energy = relax.energy(cell_aperiodic, lj)
        cell_out, energy_out = relax.relax(cell_aperiodic, lj, box_mode=relax.RelaxType.FROZEN)
        
        # Particles only should move to lower the energy
        self.assertFalse(crystal.CellTools.similar(cell_aperiodic, cell_out))
        self.assertTrue(numpy.allclose(cell_aperiodic.vectors, cell_out.vectors))
        self.assertLess(energy_out, energy)

    def test_isotropic(self):
        # Shift one FCC atom off slightly
        fcc = structure.Unit.fcc()
        atoms = fcc.atom_lists[0]
        atoms[0] = numpy.array([0.01, 0.02, 0.03])
        fcc = crystal.Cell(fcc.vectors, [atoms])

        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(fcc, (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        cell_out, energy_out = relax.relax(cell, lj, box_mode=relax.RelaxType.ISOTROPIC, pressure=10)

        # Particles and vectors should have moved, and lattice should have compressed
        self.assertFalse(crystal.CellTools.similar(cell, cell_out))
        self.assertFalse(numpy.allclose(cell.vectors, cell_out.vectors))
        self.assertGreater(cell_out.density, cell.density)

    def test_anisotropic(self):
        # Shift one FCC atom off slightly
        fcc = structure.Unit.fcc()
        atoms = fcc.atom_lists[0]
        atoms[0] = numpy.array([0.01, 0.02, 0.03])
        fcc = crystal.Cell(fcc.vectors, [atoms])

        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(fcc, (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

        cell_out, energy_out = relax.relax(cell, lj, box_mode=relax.RelaxType.ANISOTROPIC, pressure=10)

        # Particles and vectors should have moved, and lattice should have compressed
        self.assertFalse(crystal.CellTools.similar(cell, cell_out))
        self.assertFalse(numpy.allclose(cell.vectors, cell_out.vectors))
        self.assertGreater(cell_out.density, cell.density)

    def test_triclinic(self):
        # Shift FCC vectors slightly (and bring atoms with them)
        fcc = structure.Unit.fcc()
        vectors = fcc.vectors
        vectors = vectors @ numpy.array([[1.01, 0.02, 0.04], [0.07, 0.99, -0.03], [-0.05, 0.01, 1.03]])
        fcc = crystal.Cell(fcc.vectors, fcc.atom_lists)
        fcc = crystal.CellTools.scale(fcc, vectors)

        cell = crystal.CellTools.scale_to(crystal.CellTools.tile(fcc, (4, 4, 4)), 1)
        lj = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        
        # Vectors should have been "squared" and lattice should have compressed
        energy = relax.energy(cell, lj)
        cell_out, energy_out = relax.relax(cell, lj, box_mode=relax.RelaxType.TRICLINIC, pressure=10)
        self.assertFalse(crystal.CellTools.similar(cell, cell_out))
        self.assertFalse(numpy.allclose(cell.vectors, cell_out.vectors))
        self.assertTrue(numpy.all(numpy.abs(numpy.diag(numpy.diag(cell_out.vectors)) - cell_out.vectors) < 1e-6))
        self.assertGreater(cell_out.density, cell.density)

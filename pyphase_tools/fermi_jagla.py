# pyphase_tools.fermi_jagla: Evan Pretti
"""
Tools for constructing, analyzing, optimizing and visualizing
Fermi-Jagla-type potentials.  Note that in addition to importing this module and
using its functions directly, a command-line interface is available.  Run:

.. code-block:: none

    $ python -m pyphase_tools.fermi_jagla

to see a list of available commands.  Arguments are accepted in a similar manner
to those within the module functions themselves, although any appearance of a
:py:class:`ParamSet` is replaced by a sequence of the parameters themselves.
"""

import collections
import functools
import numpy
import scipy.optimize
import sys
import warnings

class default:
    __slots__ = []
    def __repr__(self):
        return "<default>"
#: Default value for optional keyword arguments.
default = default()

class ParamSet(collections.namedtuple("ParamSet",
        ("eps", "sig", "n", "s", "a0", "a1", "a2", "b0", "b1", "b2", "cut"))):
    """
    Represents a set of Fermi-Jagla parameters.  The particular form of
    the potential represented is:

    .. math::

        u(r) = \\epsilon{\\left(\\frac{\\sigma}{r-s}\\right)}^n+\\frac{A_0}{1+\\exp\\left[A_1\\left(r-A_2\\right)\\right]}+\\frac{B_0}{1+\\exp\\left[B_1\\left(r-B_2\\right)\\right]}

    Parameters
    ----------
    eps : float
        Core repulsion energy scale :math:`\\epsilon`.
    sig : float
        Core repulsion length scale :math:`\\sigma`.
    n : float
        Core repulsion exponent :math:`n`.
    s : float
        Core repulsion shift factor :math:`s`.
    a0 : float
        Repulsive sigmoid energy scale :math:`A_0`.
    a1 : float
        Repulsive sigmoid length scale :math:`A_1`.
    a2 : float
        Repulsive sigmoid shift factor :math:`A_2`.
    b0 : float
        Attractive sigmoid energy scale :math:`B_0`.
    b1 : float
        Attractive sigmoid length scale :math:`B_1`.
    b2 : float
        Attractive sigmoid shift factor :math:`B_2`.
    cut : float
        Outer cutoff for potential.
    """

    __slots__ = []


def fermi_jagla(params, r):
    """
    Evaluates a Fermi-Jagla potential over a range of separation distances.

    Parameters
    ----------
    params : ParamSet
        The potential.
    r : float or numpy.ndarray
        The separation distance or distances.

    Returns
    -------
    (float, float, float) or (numpy.ndarray, numpy.ndarray, numpy.ndarray)
        The potential :math:`u`, force :math:`-du/dr`, and spring constant :math:`d^2u/dr^2`.
    """

    if numpy.isscalar(r):
        u, f, k = fermi_jagla(params, numpy.array([r]))
        return u[0], f[0], k[0]

    # Precompute common terms
    shift_term = 1.0 / (r - params.s)
    power_term = params.eps * numpy.power(params.sig * shift_term, params.n)
    exp_term_a = numpy.exp(params.a1 * (r - params.a2))
    exp_term_b = numpy.exp(params.b1 * (r - params.b2))

    # Evaluate potential term by definition
    potential = power_term \
              + (params.a0 / (1.0 + exp_term_a)) \
              - (params.b0 / (1.0 + exp_term_b))
    potential[r <= params.s] = numpy.inf

    # Evaluate force term (note sigmoid derivatives are evaluated in a way that
    # should be more numerically stable than a direct chain rule application)
    force = (params.n * shift_term * power_term) \
          + (params.a0 * params.a1 / (2.0 + exp_term_a + (1.0 / exp_term_a))) \
          - (params.b0 * params.b1 / (2.0 + exp_term_b + (1.0 / exp_term_b)))
    force[r <= params.s] = numpy.inf

    # Evaluate effective spring constant in potential well
    spring = (params.n * (params.n + 1) * shift_term * shift_term * power_term) \
           + (params.a0 * params.a1 * params.a1 / (4.0 + exp_term_a + ((7 + (1.0 / exp_term_a)) / (exp_term_a - 1.0)))) \
           - (params.b0 * params.b1 * params.b1 / (4.0 + exp_term_b + ((7 + (1.0 / exp_term_b)) / (exp_term_b - 1.0))))
    spring[r <= params.s] = numpy.inf

    # Apply specified cutoff to match LAMMPS behavior exactly
    cutoff = r <= params.cut
    return (potential * cutoff, force * cutoff, spring * cutoff)

def get_lammps(params, style):
    """
    Generates a LAMMPS `pair_coeff` line from a Fermi-Jagla potential.

    Parameters
    ----------
    params : ParamSet
        The potential.
    style : str
        Currently supports `jagla` and `fermi_jagla`.  Consult the source for details.
    """

    if style == "jagla":
        # pair_style jagla (Yajun Ding, group standard LAMMPS)
        return " ".join("{:.16e}".format(value) for value in
            (params.a0, params.a1, params.a2, params.b0, params.b1, params.b2,
                params.sig, params.eps, params.n, params.s, params.cut))
    elif style == "fermi_jagla":
        # pair_style fermi_jagla (Evan Pretti, 20180316 LAMMPS for pyphase)
        # Note that ordering is slightly different.
        return " ".join("{:.16e}".format(value) for value in
            (params.eps, params.sig, params.n, params.s, params.a0, params.a1,
                params.a2, params.b0, params.b1, params.b2, params.cut))
    else:
        raise ValueError("Unknown pair_style for LAMMPS input")

#: Default plot resolution.
PLOT_RES = 1024
#: Default plot absolute padding factor.
PLOT_ABS_PAD = 0.5
#: Default plot relative padding factor.
PLOT_REL_PAD = 1.5
#: Default plot parameter labels.
PLOT_LABELS = [
    "$U/\\epsilon$",
    "$F\\sigma/\\epsilon$",
    "$\\kappa\\sigma^2/\\epsilon$",
]

def plot(*param_sets, r_range=default, u_range=default, labels=default, force_mode=0):
    """
    Plots Fermi-Jagla potentials.

    Parameters
    ----------
    param_sets : tuple(ParamSet)
        Potentials to plot.
    r_range : tuple(float, float)
        Range of :math:`r` to plot.
    u_range : tuple(float, float)
        Range of ordinates to plot.
    labels : list(str)
        Legend labels to use.
    force_mode : int
        Use 0 for potential energy, 1 for force, and 2 for spring constant.
    """

    # Determine abscissa range
    if r_range is default:
        r_min = min((params.s + params.cut) / 2.0 for params in param_sets)
        r_max = max(params.cut for params in param_sets)
        r_range = (r_min, r_max)

    # Populate default legend labels 1, 2, ...
    if labels is default:
        labels = [str(index + 1) for index in range(len(param_sets))]
    
    # Evaluate all curves
    import matplotlib.pyplot as plt
    r = numpy.linspace(*r_range, PLOT_RES)
    u_min_all = 0.0
    for params, label in zip(param_sets, labels):
        components = fermi_jagla(params, r)
        # Keep track of minimum ordinate values for each case if needed
        u_min = min(components[force_mode])
        if u_min_all > u_min:
            u_min_all = u_min
        plt.plot(r, components[force_mode], label=label)

    # Determine ordinate range based on collected minima
    if u_range is default:
        cut_low = min(u_min_all - PLOT_ABS_PAD, u_min_all * PLOT_REL_PAD)
        u_range = (cut_low, -cut_low)

    # Set plot parameters
    plt.xlim(r_range)
    plt.ylim(u_range)
    plt.xlabel("$r/\\sigma$")
    plt.ylabel(PLOT_LABELS[force_mode])
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.show()

@functools.lru_cache(maxsize=None)
def shift_jagla(params, shift_by):
    """
    Shifts a Fermi-Jagla potential horizontally.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    shift_by : float
        Horizontal shift amount.
    
    Returns
    -------
    ParamSet
        Output potential.
    """

    return ParamSet(params.eps, params.sig, params.n, params.s + shift_by,
        params.a0, params.a1, params.a2 + shift_by,
        params.b0, params.b1, params.b2 + shift_by,
        params.cut + shift_by)

@functools.lru_cache(maxsize=None)
def scale_jagla(params, scale_by, scale_about=default):
    """
    Scales a Fermi-Jagla potential horizontally.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    scale_by : float
        Horizontal scale amount.
    scale_about : float
        If specified, scales about the given horizontal position.  The
        default behavior is to scale about the origin.

    Returns
    -------
    ParamSet
        Output potential.
    """

    if scale_about is default:
        return ParamSet(params.eps, params.sig * scale_by, params.n, params.s * scale_by,
            params.a0, params.a1 / scale_by, params.a2 * scale_by,
            params.b0, params.b1 / scale_by, params.b2 * scale_by,
            params.cut * scale_by)
    else:
        return shift_jagla(scale_jagla(shift_jagla(params, -scale_about), scale_by), scale_about)

#: Epsilon for bracketing searches and stepping away from singularities.
EPS = 4 * numpy.finfo(float).eps
#: Maximum number of evaluations when seeking for attraction.
SPLIT_MAX = 65536
#: Maximum number of bisections which can be made.
CRIT_PREC = -numpy.finfo(float).machep - 2

@functools.lru_cache(maxsize=None)
def find_brackets(params):
    """
    Finds suitable brackets to perform root-finding in a Fermi-Jagla potential.

    Parameters
    ----------
    params : ParamSet
        The potential.

    Returns
    -------
    float, float, float
        A point to the left of the local minimum, a point between the local
        extrema, and a point to the right of the local maximum.
    """

    # Find left side (when values become finite)
    step = 1
    while True:
        left = (params.s * (1.0 + (step * EPS))) + (step * EPS)
        if numpy.all(numpy.isfinite(fermi_jagla(params, left))):
            break
        else:
            step *= 2

    # Find right side (when values become non-zero)
    step = 1
    while True:
        right = (params.cut * (1.0 - (step * EPS))) - (step * EPS)
        if numpy.all(fermi_jagla(params, right) != 0):
            break
        else:
            step *= 2

    # Look for attraction (negative force)
    split = 2
    while True:
        r = numpy.linspace(left, right, split + 1)[1:-1]
        i = numpy.nonzero(fermi_jagla(params, r)[1] < 0)[0]
        if i.shape[0]:
            negative = r[i[0]]
            break
        else:
            split *= 2
            if split > SPLIT_MAX:
                return None

    return left, negative, right
    
@functools.lru_cache(maxsize=None)
def extrema(params):
    """
    Finds the local extrema (a minimum and a maximum) in a Fermi-Jagla potential.

    Parameters
    ----------
    params : ParamSet
        The potential.

    Returns
    -------
    float, float
        The positions of the minimum and the maximum, respectively.
    """

    brackets = find_brackets(params)
    if brackets is None:
        return None
    left, negative, right = brackets
    jagla_eval = lambda r: fermi_jagla(params, r)[1]
    # van Wijngaarden-Dekker-Brent algorithm: signs of boundary points must be opposite
    r_min = scipy.optimize.brentq(jagla_eval, left, negative, xtol=EPS, disp=True)
    r_max = scipy.optimize.brentq(jagla_eval, negative, right, xtol=EPS, disp=True)
    return r_min, r_max

@functools.lru_cache(maxsize=None)
def with_b0(params, b0):
    """
    Modifies the value :math:`B_0` in a parameter set.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    b0 : float
        The new value of :math:`B_0`.

    Returns
    -------
    ParamSet
        Output potential.
    """

    return ParamSet(params.eps, params.sig, params.n, params.s,
        params.a0, params.a1, params.a2,
        b0, params.b1, params.b2,
        params.cut)

@functools.lru_cache(maxsize=None)
def with_cut(params, cut):
    """
    Modifies the value of the cutoff in a parameter set.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    cut : float
        The new value for the cutoff.

    Returns
    -------
    ParamSet
        Output potential.
    """

    return ParamSet(params.eps, params.sig, params.n, params.s,
        params.a0, params.a1, params.a2,
        params.b0, params.b1, params.b2,
        cut)

@functools.lru_cache(maxsize=None)
def critical_b0(params):
    """
    Estimates the critical value of :math:`B_0` in a Fermi-Jagla potential
    (where the local minimum and maximum merge together).

    Parameters
    ----------
    params : ParamSet
        The potential.

    Returns
    -------
    float
        The critical value of :math:`B_0`.
    """

    low = 0.0
    high = 1.0
    # Keep increasing B0 if necessary
    while True:
        if extrema(with_b0(params, high)) is None:
            high += 1.0
        else:
            break
    # Search for the critical value by bisection
    for division in range(CRIT_PREC):
        middle = (low + high) / 2.0
        if extrema(with_b0(params, middle)) is None:
            low, high = middle, high
        else:
            low, high = low, middle
    return high

@functools.lru_cache(maxsize=None)
def optimize_b0(params, depth):
    """
    Finds a value of :math:`B_0` to set the depth of a minimum in
    a Fermi-Jagla potential.

    Parameters
    ----------
    params : ParamSet
        The potential.
    depth : float
        The well depth.

    Returns
    -------
    float
        The optimal value of :math:`B_0`.
    """

    # Try to find bounds (note that extrema() will still evaluate at critical B0
    # as the value returned is just higher than the actual critical point)
    target = lambda b0: depth + fermi_jagla(with_b0(params, b0), extrema(with_b0(params, b0))[0])[0]
    lower_bound = critical_b0(params)
    upper_bound = lower_bound + 1.0

    # van Wijngaarden-Dekker-Brent algorithm: signs of boundary points must be opposite
    step = 1
    while target(upper_bound) > 0:
        upper_bound += step
        step *= 2
    return scipy.optimize.brentq(target, lower_bound, upper_bound, xtol=EPS, disp=True)

@functools.lru_cache(maxsize=None)
def widen_jagla(params, widen):
    """
    Widens the potential well of a Fermi-Jagla potential by modifying its sigmoidal terms.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    widen : float
        A widening factor (depending on how the width of the well is measured, it
        should be widened by approximately this amount).

    Returns
    -------
    ParamSet
        Output potential.
    """

    return ParamSet(params.eps, params.sig, params.n, params.s,
        params.a0, params.a1 / widen, params.a2 - ((widen - 1) * (1 - params.a2)),
        params.b0, params.b1 / widen, params.b2 + ((widen - 1) * (params.b2 - 1)),
        params.cut)

@functools.lru_cache(maxsize=None)
def widen_renormalize_jagla(params, widen):
    """
    Widens the potential well of a Fermi-Jagla potential using the approximate
    :py:func:`widen_jagla` routine, and then adjusts the horizontal scaling
    and :math:`B_0` parameters to match the original, unwidened, potential.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    widen : float
        A widening factor (see :py:func:`widen_jagla`).

    Returns
    -------
    ParamSet
        Output potential.
    """

    widened = widen_jagla(params, widen)
    optimized = with_b0(widened, optimize_b0(widened, -fermi_jagla(params, extrema(params)[0])[0]))
    return scale_jagla(optimized, extrema(params)[0] / extrema(optimized)[0])

#: Tolerance for Newton's method.
EPSNEWTON = 1e-14

@functools.lru_cache(maxsize=None)
def widen_jagla_to(params, widen_to):
    """
    Widens a Fermi-Jagla potential to place the maximum at a given location.  The widening
    is performed using :py:func:`widen_renormalize_jagla`.

    Parameters
    ----------
    params : ParamSet
        Input potential.
    widen_to : float
        The location to place the maximum at.

    Returns
    -------
    ParamSet
        Output potential.
    """

    minimum, maximum = extrema(params)
    target = lambda widen: extrema(widen_renormalize_jagla(params, widen))[1] - widen_to
    widen = scipy.optimize.newton(target, (widen_to - minimum) / (maximum - minimum), tol=EPSNEWTON)
    return widen_renormalize_jagla(params, widen)

@functools.lru_cache(maxsize=None)
def optimal_cutoff(params, cut_u):
    """
    Finds an optimal cutoff for a Fermi-Jagla potential based on ensuring that the
    potential energy is sufficiently small in magnitude.

    Parameters
    ----------
    params : ParamSet
        The potential.
    cut_u : float
        The minimum magnitude of the potential energy for which the pair
        potential should not be cut off.

    Returns
    -------
    float
        The cutoff determined.
    """

    params_no_cut = with_cut(params, numpy.inf)
    extrema_res = extrema(params_no_cut)
    if extrema_res is None:
        step = 1
        while True:
            left = (params_no_cut.s * (1.0 + (step * EPS))) + (step * EPS)
            if numpy.all(numpy.isfinite(fermi_jagla(params_no_cut, left))):
                break
            else:
                step *= 2
    else:
        left = extrema_res[1]
    if fermi_jagla(params_no_cut, left)[0] < cut_u:
        return left
    right = params.cut
    step = 1
    while fermi_jagla(params_no_cut, right)[0] >= cut_u:
        right = (right * (1.0 + (step * EPS))) + (step * EPS)
        step *= 2
    step = 1
    while True:
        right = (right * (1.0 - (step * EPS))) - (step * EPS)
        if numpy.all(fermi_jagla(params_no_cut, right) != 0):
            break
        else:
            step *= 2
    target = lambda cut_r: fermi_jagla(params_no_cut, cut_r)[0] - cut_u
    return scipy.optimize.brentq(target, left, right, xtol=EPS, disp=True)

def make_standard_jagla():
    """
    Prints information on "standard" Fermi-Jagla potentials.
    """

    # "Standard" micron-sized Jagla potential
    micro_jagla_rep = ParamSet(10.0, 0.2, 36.0, 0.8, 11.0346, 404.396, 1.0174094, 0.0, 1044.5, 1.0305952, 1.1)
    micro_jagla_att = with_b0(micro_jagla_rep, optimize_b0(micro_jagla_rep, 1))
    # "Unshifted" Jagla potential
    unsh_jagla = shift_jagla(micro_jagla_att, -micro_jagla_att.s)
    unsh_jagla_att = scale_jagla(unsh_jagla, 1.0 / extrema(unsh_jagla)[0])
    unsh_jagla_rep = with_b0(unsh_jagla_att, 0.0)
    # Jagla optimized to nanoparticle size
    nano_jagla_att = widen_jagla_to(unsh_jagla_att, 26.5 / 23)
    nano_jagla_rep = with_b0(nano_jagla_att, 0.0)
    att_cut = optimal_cutoff(nano_jagla_att, 1e-6)
    rep_cut = optimal_cutoff(nano_jagla_rep, 1e-6)
    max_cut = max(att_cut, rep_cut)

    print("micro_jagla_rep = {!r}".format(micro_jagla_rep))
    print("micro_jagla_att = {!r}".format(micro_jagla_att))
    print("nano_jagla_rep = {!r}".format(with_cut(nano_jagla_rep, max_cut)))
    print("nano_jagla_att = {!r}".format(with_cut(nano_jagla_att, max_cut)))

# Cached expected values from make_standard_jagla.
#: Repulsive micron-sized Fermi-Jagla potential.
micro_jagla_rep = ParamSet(eps=10.0, sig=0.2, n=36.0, s=0.8, a0=11.0346, a1=404.396, a2=1.0174094, b0=0.0, b1=1044.5, b2=1.0305952, cut=1.1)
#: Attractive micron-sized Fermi-Jagla potential.
micro_jagla_att = ParamSet(eps=10.0, sig=0.2, n=36.0, s=0.8, a0=11.0346, a1=404.396, a2=1.0174094, b0=1.3218525572535642, b1=1044.5, b2=1.0305952, cut=1.1)
#: Repulsive nanoparticle-scale Fermi-Jagla potential.
nano_jagla_rep = ParamSet(eps=10.0, sig=0.8724334772157049, n=36.0, s=0.0, a0=11.0346, a1=22.888094271381036, a2=0.808359168735757, b0=0.0, b1=59.116842071774926, b2=1.0413311546763873, cut=1.5178450222815683)
#: Attractive nanoparticle-scale Fermi-Jagla potential.
nano_jagla_att = ParamSet(eps=10.0, sig=0.8724334772157049, n=36.0, s=0.0, a0=11.0346, a1=22.888094271381036, a2=0.808359168735757, b0=1.3142076043026603, b1=59.116842071774926, b2=1.0413311546763873, cut=1.5178450222815683)

def get_standard_jagla(index):
    """
    Retrieves `micro_jagla_rep`, `micro_jagla_att`, `nano_jagla_rep`, or `nano_jagla_att`.
    These are the cached, not the computed, versions of these potentials.  Unless the
    parameters within :py:func:`make_standard_jagla` are changed, the results from the
    two routines should match.

    Parameters
    ----------
    index : int
        The index of the standard potential to retrieve.
    """

    return (micro_jagla_rep, micro_jagla_att, nano_jagla_rep, nano_jagla_att)[index]

def plot_standard_jagla():
    """
    Plots "standard" Fermi-Jagla potentials.

    .. plot::

        from pyphase_tools import fermi_jagla
        fermi_jagla.plot_standard_jagla()
    """

    plot(shift_jagla(micro_jagla_att, 1 - extrema(micro_jagla_att)[0]), shift_jagla(micro_jagla_rep, 1 - extrema(micro_jagla_att)[0]), nano_jagla_att, nano_jagla_rep)

def plot_nano_jagla():
    """
    Plots nanoparticle-scale Fermi-Jagla potentials.

    .. plot::

        from pyphase_tools import fermi_jagla
        fermi_jagla.plot_nano_jagla()
    """

    E_list = numpy.linspace(0, 1, 6)
    jaglas = [with_b0(nano_jagla_att, optimize_b0(nano_jagla_att, E)) for E in E_list]
    plot(*jaglas, labels=["{:.1f}".format(E) for E in E_list])

def plot_compare_jagla():
    """
    Makes comparison plots between micron-sized and nanoparticle-scale Fermi-Jagla potentials.

    .. plot::

        from pyphase_tools import fermi_jagla
        fermi_jagla.plot_compare_jagla()
    """

    nano_jaglas = [nano_jagla_rep, with_b0(nano_jagla_att, optimize_b0(nano_jagla_att, 0.5)), with_b0(nano_jagla_att, optimize_b0(nano_jagla_att, 1))]
    micro_jaglas = [micro_jagla_rep, with_b0(micro_jagla_att, optimize_b0(micro_jagla_att, 0.5)), with_b0(micro_jagla_att, optimize_b0(micro_jagla_att, 1))]

    r_range = (0.9, 1.2)
    u_range = (-1.5, 1.5)
    r = numpy.linspace(*r_range, PLOT_RES)
    
    import matplotlib.pyplot as plt
    plt.figure(figsize=(8, 3))
    plt.subplot(1, 2, 1)
    for micro_jagla in micro_jaglas:
        plt.plot(r, fermi_jagla(micro_jagla, r)[0])
    plt.title("Micron-scale")
    plt.xlim(r_range)
    plt.ylim(u_range)
    plt.xlabel("$r/\\sigma$")
    plt.ylabel("$U/\\epsilon$")
    plt.grid()

    plt.subplot(1, 2, 2)
    for nano_jagla in nano_jaglas:
        plt.plot(r, fermi_jagla(nano_jagla, r)[0])
    plt.title("Nano-scale")
    plt.xlim(r_range)
    plt.ylim(u_range)
    plt.xlabel("$r/\\sigma$")
    plt.grid()

    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    # Command-line interface allowed commands and their arguments
    allowed_commands = {
        "fermi_jagla": (float,) * 12,
        "get_lammps": (float,) * 11 + (str,),
        "shift_jagla": (float,) * 12,
        "scale_jagla": (float,) * 13,
        "find_brackets": (float,) * 11,
        "extrema": (float,) * 11,
        "with_b0": (float,) * 12,
        "with_cut": (float,) * 12,
        "critical_b0": (float,) * 11,
        "optimize_b0": (float,) * 12,
        "widen_jagla": (float,) * 12,
        "widen_renormalize_jagla": (float,) * 12,
        "widen_jagla_to": (float,) * 12,
        "optimal_cutoff": (float,) * 12,
        "make_standard_jagla": (),
        "get_standard_jagla": (int,),
        "plot_standard_jagla": (),
        "plot_nano_jagla": (),
        "plot_compare_jagla": ()
    }
    # Make sure a command name is given
    if len(sys.argv) < 2:
        sys.exit("Specify a command for fermi_jagla: one of\n{}".format("\n".join("\t{}".format(key) for key in sorted(allowed_commands))))
    # Get the command and zero or more arguments
    command, arguments = sys.argv[1].lower(), sys.argv[2:]
    # The command must be known
    if command not in allowed_commands:
        raise ValueError("Invalid command {!r}".format(command))
    # The correct number of arguments must be given
    argument_data = allowed_commands[command]
    if len(arguments) != len(argument_data):
        raise ValueError("Received {} arguments to command {!r} (expected {})".format(len(arguments), command, len(argument_data)))
    # Suppress numpy warnings and look up the function to call with converted arguments
    with warnings.catch_warnings():
        numpy.seterr(all="ignore")
        result = globals()[command](*(converter(argument) for argument, converter in zip(arguments, argument_data)))
    # Print the output only if there was one
    if result is not None:
        print(result)

# pyphase_tools._test_utilities: Evan Pretti
"""
Miscellaneous utilities used during the unit testing suite :py:mod:`pyphase_test`.
"""

import contextlib
import inspect
import os
import stat
import subprocess
import tempfile
import warnings

from pyphase import lammps

def get_self_path():
    """
    Retrieves the absolute path to the Python file containing this routine
    (:py:func:`pyphase_tools._test_utilities.get_self_path`).

    Returns
    -------
    str
        Absolute path to `_test_utilities.py`, with all symbolic links resolved.
    """
    
    return os.path.realpath(inspect.getfile(inspect.currentframe()))

def get_root_path():
    """
    Retrieves the PyPhase root directory.

    Returns
    -------
    str
        Absolute path to the PyPhase root directory, with all symbolic links
        resolved.
    """

    return os.path.dirname(os.path.dirname(get_self_path()))

def get_tmp_path():
    """
    Retrieves a temporary directory path.

    Returns
    -------
    str
        Absolute path to a temporary directory.  This directory is not guaranteed
        to exist.  See :py:func:`ensure_tmp_path` if this behavior is desired.
    """

    return os.path.join(get_root_path(), "tmp")

def get_cache_path():
    """
    Retrieves a cache directory path.

    Returns
    -------
    str
        Absolute path to a cache directory.  This directory is not guaranteed
        to exist.  See :py:func:`ensure_cache_path` if this behavior is desired.
    """

    return os.path.join(get_tmp_path(), "cache")

def get_scratch_path():
    """
    Retrieves a scratch directory path.

    Returns
    -------
    str
        Absolute path to a scratch directory.  This directory is not guaranteed
        to exist.  See :py:func:`ensure_scratch_path` if this behavior is desired.
    """

    return os.path.join(get_tmp_path(), "scratch")

def ensure_path(path):
    """
    Ensures that a directory path exists.  The parent directory should
    exist.  If the directory itself does not exist, it will be created.

    Parameters
    ----------
    path : str
        Absolute path to the directory to check.

    Returns
    -------
    path
        The path passed in.

    Raises
    ------
    OSError
        The directory did not exist but could not be created.
    """

    if not os.path.exists(path):
        os.mkdir(path)

    if not os.path.isdir(path):
        raise OSError("File or other object exists in place of directory")

    return path

def ensure_tmp_path():
    """
    Retrieves a temporary directory path.  The directory will be
    created if it does not exist.

    Returns
    -------
    str
        Absolute path to a temporary directory.  It should exist, but no
        guarantees are made as to its contents.

    Raises
    ------
    OSError
        The directory did not exist but could not be created.
    """
    
    return ensure_path(get_tmp_path())

def ensure_cache_path():
    """
    Retrieves a cache directory path.  The directory will be
    created if it does not exist.

    Returns
    -------
    str
        Absolute path to a cache directory.  It should exist, but no
        guarantees are made as to its contents.

    Raises
    ------
    OSError
        The directory did not exist but could not be created.
    """
    
    # Make sure that parent temporary directory already exists
    ensure_tmp_path()
    return ensure_path(get_cache_path())

def ensure_scratch_path():
    """
    Retrieves a scratch directory path.  The directory will be
    created if it does not exist.

    Returns
    -------
    str
        Absolute path to a scratch directory.  It should exist, but no
        guarantees are made as to its contents.

    Raises
    ------
    OSError
        The directory did not exist but could not be created.
    """
    
    # Make sure that parent temporary directory already exists
    ensure_tmp_path()
    return ensure_path(get_scratch_path())

def is_lammps(path):
    """
    Determines whether the provided path points to a LAMMPS executable.

    Parameters
    ----------
    path : str
        The path to check.

    Returns
    -------
    bool
        Whether or not the executable was able to start, produce a status line
        identifying itself, and produce a LAMMPS log file.
    """

    with tempfile.TemporaryDirectory(prefix="pyphase-test-", dir=ensure_scratch_path()) as temp_path:
        try:
            data = subprocess.run([path], cwd=temp_path, check=True, input=b"quit", stdout=subprocess.PIPE).stdout
        except subprocess.CalledProcessError:
            # Older LAMMPS versions might return 1 unconditionally
            # If the process is actually not found, something other than a CalledProcessError should result
            return False

        if not data.decode().startswith("LAMMPS"):
            # Something other than LAMMPS got executed
            return False
        
        log_file = os.path.join(temp_path, "log.lammps")
        if not os.path.isfile(log_file):
            # By default, a log file should be created
            return False

        with open(log_file, "r") as log_file:
            if not log_file.read().startswith("LAMMPS"):
                # Log file might be empty
                return False

        return True

@contextlib.contextmanager
def reset_lookup():
    """
    Resets global state that would affect LAMMPS lookup.  Calling this
    routine in a `with` block restores the global state.
    """
    
    try:
        # Get old values
        global_lammps = lammps._LAMMPS_OVERRIDE
        env_path = os.environ.get(lammps._PYPHASELMPPATH, None)
        env_name = os.environ.get(lammps._PYPHASELMPNAME, None)
        
        # Reset state
        lammps.set_lammps_global()
        os.environ.pop(lammps._PYPHASELMPPATH, None)
        os.environ.pop(lammps._PYPHASELMPNAME, None)
    
        yield
    except lammps.LAMMPSNotFoundError as lammps_not_found:
        if env_path is not None and is_lammps(env_path):
            # Tests failed because a LAMMPS override was reset
            warnings.warn("Some tests could not be completed as LAMMPS was unavailable on the system path")
        else:
            raise RuntimeError("LAMMPS not found but available at PYPHASELMPPATH") from lammps_not_found
    finally:
        # Restore values
        lammps.set_lammps_global(global_lammps)
        if env_path is not None:
            os.environ[lammps._PYPHASELMPPATH] = env_path
        else:
            os.environ.pop(lammps._PYPHASELMPPATH, None)
        if env_name is not None:
            os.environ[lammps._PYPHASELMPNAME] = env_name
        else:
            os.environ.pop(lammps._PYPHASELMPNAME, None)

@contextlib.contextmanager
def mock_lammps(name):
    """
    Creates an empty executable file in a temporary directory.  Calling
    this routine in a `with` block should clean up the temporary
    directory afterwards.

    Parameters
    ----------
    name : str
        The name of the empty file.

    Returns
    -------
    str
        An absolute path.
    """

    with tempfile.TemporaryDirectory(prefix="pyphase-test-", dir=ensure_scratch_path()) as temp_path:
        target = os.path.join(temp_path, name)
        with open(target, "wb") as temp_file:
            pass
        os.chmod(target, os.stat(target).st_mode | stat.S_IEXEC)
        yield target

def core_count():
    """
    Retrieves the number of cores which can be used for testing parallel routines.

    Returns
    -------
    int
        Number of available cores.
    """

    return len(os.sched_getaffinity(0))

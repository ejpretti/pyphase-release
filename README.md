PyPhase
=======

Free energy and phase behavior calculation tools.  Currently, can perform local energy and enthalpy minimizations and absolute Helmholtz free energy calculations of periodic, semi-periodic and non-periodic solids using [LAMMPS](https://lammps.sandia.gov/), the Large-scale Atomic/Molecular Massively Parallel Simulator.

Citation
--------

*   E. Pretti and J. Mittal.  "Extension of the Einstein molecule method for solid free energy calculation to non-periodic and semi-periodic systems."  _J. Chem. Phys._, __2019__ ([doi:10.1063/1.5100960](https://dx.doi.org/10.1063/1.5100960))

Setup information
-----------------

In order to use PyPhase, you should make sure that [Python 3](https://www.python.org/), [NumPy](https://www.numpy.org/), [SciPy](https://www.scipy.org/scipylib/), [Matplotlib](https://matplotlib.org/), and [Sphinx](http://www.sphinx-doc.org/) are installed.  You should also have [LAMMPS](https://lammps.sandia.gov/) installed.  Consult the [LAMMPS documentation](https://lammps.sandia.gov/doc/Manual.html) for information on how to build LAMMPS.  Note that you do not need to build LAMMPS as a shared library in order to use PyPhase; the `pyphase.lammps` module takes care of locating and communicating with LAMMPS as a standard executable.

Once you have PyPhase and prerequisites installed, you can build and view the documentation files and run the tests:

```
~/pyphase/ $ ./build
```

You can now open `doc/build/html/index.html` in your browser; more information on running PyPhase as well as comprehensive API documentation is provided within the documentation pages.  Please note that you must have LAMMPS available: the documentation will not successfully build without it, nor will the tests successfully pass.  If LAMMPS cannot be found, set the environment variable `PYPHASELMPPATH` to the absolute path to the desired LAMMPS executable.  Consult the documentation for more information on this behavior.

License and disclaimer
----------------------

Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that this statement and the following disclaimer are retained.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

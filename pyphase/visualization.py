# pyphase.visualization: Evan Pretti
"""
Contains routines dealing with inspection and visualization of output from
PyPhase.  Note that for those routines requiring `matplotlib`, a
:py:class:`matplotlib.axes.Axes` object is accepted and will be drawn to.
In this way, the handling of the plotting can be dealt with by the caller.

Also, importing this module (and by extension, importing :py:mod:`pyphase`)
will not import any `matplotlib` modules.  These are only imported in methods
where they are actually required.  Thus, `matplotlib` need not be installed to
run PyPhase unless specific visualization routines are called which require it.
This can make setup and use of PyPhase on remote and HPC machines easier.
"""

import numpy
import scipy.stats

from . import crystal

def show(routine, *args, **kwargs):
    """
    A helper routine which generates a :py:class:`matplotlib.figure.Figure`
    and :py:class:`matplotlib.axes.Axes` for drawing, and displays the figure.
    For example::

        show(cell, crystal.Cell(...), (1, 1), tile=(2, 2))

    Parameters
    ----------
    routine : callable
        The visualization routine to call, accepting a :py:class:`matplotlib.axes.Axes`
        as its first argument.  The additional positional and keyword arguments will
        be passed through to the routine.
    """

    import matplotlib.pyplot

    figure = matplotlib.pyplot.figure()
    axes = figure.add_subplot(1, 1, 1)
    routine(axes, *args, **kwargs)
    figure.tight_layout()
    matplotlib.pyplot.show()

_PADDING = 0.1

def cell(axes, cell, radii, box=False, tile=None):
    """
    Generates a two-dimensional representation of a cell using `matplotlib`.

    Parameters
    ----------
    axes : matplotlib.axes.Axes
        The target axes.
    cell : crystal.Cell
        The cell to draw.
    radii : tuple(float)
        The radii of the atom types.
    box : bool
        Whether or not to draw the cell vectors.
    tile : tuple(int) or None
        Tiles using :py:meth:`crystal.CellTools.tile` if specified.
    """
    
    import matplotlib
    import matplotlib.patches

    if cell.dimensions != 2:
        raise NotImplementedError("only two-dimensional renderings supported")

    # Plot box
    if box:
        axes.plot(*numpy.array((numpy.zeros((2,)), cell.vectors[0],
            numpy.sum(cell.vectors, axis=0), cell.vectors[1], numpy.zeros((2,)))).T,
            color="black")

    # Do tiling if specified
    if tile is not None:
        cell = crystal.CellTools.tile(cell, tile)
    
    # Plot atoms (use default Matplotlib colors)
    colors = matplotlib.rcParams["axes.prop_cycle"].by_key()["color"]
    for atom_list, radius, color in zip(cell.atom_lists, radii, colors):
        for atom in atom_list:
            axes.add_artist(matplotlib.patches.Circle(atom, radius, color=color, linewidth=0))

    # Determine extents
    x_min = numpy.min([atom[0] - radius for atom_list, radius in zip(cell.atom_lists, radii) for atom in atom_list])
    x_max = numpy.max([atom[0] + radius for atom_list, radius in zip(cell.atom_lists, radii) for atom in atom_list])
    y_min = numpy.min([atom[1] - radius for atom_list, radius in zip(cell.atom_lists, radii) for atom in atom_list])
    y_max = numpy.max([atom[1] + radius for atom_list, radius in zip(cell.atom_lists, radii) for atom in atom_list])
    x_diff, y_diff = x_max - x_min, y_max - y_min
    
    # Set styles for axes
    axes.set_xlim(x_min - x_diff * _PADDING, x_max + x_diff * _PADDING)
    axes.set_ylim(y_min - y_diff * _PADDING, y_max + y_diff * _PADDING)
    axes.set_aspect("equal")
    axes.set_xticks([])
    axes.set_yticks([])

def pairs(axes, pairs, r_min, r_max, y_min, y_max, force=False, points=100, **lammps_kwargs):
    """
    Generates a plot of pair potentials using `matplotlib`.  This requires LAMMPS
    availability; it will be invoked to evaluate the potentials.

    Parameters
    ----------
    axes : matplotlib.axes.Axes
        The target axes.
    pairs : list(tuple(lammps.Pair, int, int))
        The pair potentials to plot, along with the :math:`(i,j)` pairs to use in evaluation.
    r_min : float
        Minimum separation distance.
    r_max : float
        Maximum separation distance.
    y_min : float
        Minimum ordinate value.
    y_max : float
        Maximum ordinate value.
    force : bool
        If set, the ordinate is the force instead of the potential.
    points : int
        Number of sample points to use in the plot.
    lammps_kwargs : dict
        Keyword arguments to provide to :py:func:`.lammps.run_lammps`.
    """
    
    for pair, i, j in pairs:
        r, u, f = pair.evaluate(i, j, r_min, r_max, points, **lammps_kwargs)
        y = f if force else u
        axes.plot(r, y)
    axes.set_xlabel("$r/\\sigma$")
    axes.set_ylabel("$f\\sigma/\\epsilon$" if force else "$u/\\epsilon$")
    axes.set_xlim(r_min, r_max)
    axes.set_ylim(y_min, y_max)

def integrand(caches, names=None, confidence=0.95):
    """
    Extracts thermodynamic integration data from results caches and plots them.
    This routine helps in visualizing the output of a run from :py:mod:`einstein`.

    Parameters
    ----------
    caches : list
        The caches to process.
    names : list
        Names for plot legends, if desired.
    confidence : float
        Confidence level to use for error bars (the default is 0.95, giving the
        95% confidence level).
    """

    import matplotlib.pyplot

    def get_data_single(cache):
        try:
            # A full helmholtz routine run was conducted, so run_em_a2 may have been
            # called both to set a sufficient run length and to do the integration
            quadrature_points = next(value for routine, quantity, value in cache
                if routine == "helmholtz" and quantity == "N_quad")
        except StopIteration:
            # The cache is from a call to integrate_em_a2 only, so the only data
            # from run_em_a2 will be from the quadrature
            quadrature_points = sum(1 for routine, quantity, value in cache
                if routine == "run_em_a2" and quantity == "<Sum|r-r_0|^2>/N")

        # Plot estimated 95% confidence level
        ci_factor = scipy.stats.norm.interval(confidence)[1]

        # Pull out the last quadrature results (they should be in order even if the
        # integrand was evaluated in parallel: the cache data is resynchronized once
        # all runs have completed)
        msd_data = [(value[0], ci_factor * value[1]) for routine, quantity, value in cache
            if routine == "run_em_a2" and quantity == "<Sum|r-r_0|^2>/N"][-quadrature_points:]
        k_E_data = [value for routine, quantity, value in cache
            if routine == "integrate_em_a2" and quantity.startswith("k_E_") and quantity != "k_E_max"]
        f_data = [(value[0], ci_factor * value[1]) for routine, quantity, value in cache
            if routine == "integrate_em_a2" and quantity.startswith("f(u_")]
        u_data = [value for routine, quantity, value in cache
            if routine == "integrate_em_a2" and quantity.startswith("u_")]

        return k_E_data, msd_data, u_data, f_data

    # Make a plot
    figure = matplotlib.pyplot.figure(figsize=(8, 4))
    left = figure.add_subplot(1, 2, 1)
    right = figure.add_subplot(1, 2, 2)
   
    for index, cache in enumerate(caches):
        k_E_data, msd_data, u_data, f_data = get_data_single(cache)
        label = "#{}".format(index + 1) if names is None else names[index]
        left.errorbar(k_E_data, *zip(*msd_data), fmt="+-", linewidth=1, capsize=3, label=label)
        right.errorbar(u_data, *zip(*f_data), fmt="+-", linewidth=1, capsize=3)
    
    # Plotting in spring constant / MSD space
    left.set_xscale("log")
    left.set_xlabel("$k_E\\sigma^2/\\epsilon$")
    left.set_ylabel("$\\left<\\sum{\\left|r-r_0\\right|}^2\\right>/N\\sigma^2$")
    left.grid()
    if len(caches) > 1:
        left.legend()
    
    # Plotting in abscissa / integrand space
    right.set_xlabel("$\\ln\\left(c-\\lambda\\right)$")
    right.set_ylabel("$k_E\\left(c-\\lambda\\right){\\left<\\sum{\\left|r-r_0\\right|}^2\\right>}_{\\lambda}/2N\\epsilon$")
    right.grid()
    
    figure.tight_layout()
    matplotlib.pyplot.show()

def _cells(*cells):
    """
    Helper routine for generating figures in documentation.
    Displays images of periodic primitive or unit cells in two dimensions.

    Parameters
    ----------
    cells : tuple(crystal.Cell)
        Cells to plot, one subplot per cell.
    """

    import matplotlib.pyplot
    
    figure = matplotlib.pyplot.figure(figsize=(3 * len(cells), 3))
    for index, current_cell in enumerate(cells):
        radii = (1,) * current_cell.atom_types
        axes = figure.add_subplot(1, len(cells), index + 1)
        current_cell = crystal.CellTools.scale_by_factor(current_cell, radii)
        cell(axes, current_cell, radii, box=True, tile=(3, 3))
    figure.tight_layout()
    matplotlib.pyplot.show()

def _clusters(*cells):
    """
    Helper routine for generating figures in documentation.
    Displays images of two-dimensional clusters.

    Parameters
    ----------
    cells : tuple(crystal.Cell)
        Cells to plot, one subplot per cell.
    """

    import matplotlib.pyplot
    
    figure = matplotlib.pyplot.figure(figsize=(3 * len(cells), 3))
    for index, current_cell in enumerate(cells):
        radii = (1,) * current_cell.atom_types
        axes = figure.add_subplot(1, len(cells), index + 1)
        current_cell = crystal.CellTools.scale_by_factor(current_cell, radii)
        cell(axes, current_cell, radii)
    figure.tight_layout()
    matplotlib.pyplot.show()

def _pairs(*pair_plots):
    """
    Helper routine for generating figures in documentation.
    Displays pair potentials.

    Parameters
    ----------
    pair_plots : list(dict)
        Keyword arguments for :py:func:`pairs`.
    """

    import matplotlib.pyplot

    figure = matplotlib.pyplot.figure(figsize=(4 * len(pair_plots), 3))
    for index, pair_kwargs in enumerate(pair_plots):
        axes = figure.add_subplot(1, len(pair_plots), index + 1)
        pairs(axes, **pair_kwargs)
    figure.tight_layout()
    matplotlib.pyplot.show()

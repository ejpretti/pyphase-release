# pyphase.crystal: Evan Pretti
"""
Contains routines to perform manipulations on periodic crystal systems.  Most of
this module was originally developed during the (ongoing) development of the PACCS
package.  Not all of the available functionality will typically be required for
a PyPhase run, but many of the routines are useful for manipulating crystal
structures.
"""

import collections
import contextlib
import itertools
import math
import numpy
import pickle
import scipy.spatial
import warnings

class Cell:
    """
    Creates a cell with the specified parameters.

    Parameters
    ----------
    vectors : numpy.ndarray
        Cell vectors as a square matrix of row vectors.  The size of this matrix
        determines the number of dimensions of the cell, which must be at least 2.
    atom_lists : list(numpy.ndarray)
        Atom coordinates as matrices of row vectors.  Each matrix in the list should
        specify the coordinates of one type of atoms.
    names : list(str)
        Names of types of atoms (letters will be used if no names are provided).
    periodic : numpy.ndarray
        Boolean flags indicating periodicity in each direction.  If this option
        is not specified, the created cell will be periodic along all axes.

    Raises
    ------
    ValueError
        Invalid or inconsistent dimensions or values.
    """

    __slots__ = ["__vectors", "__dimensions", "__atom_lists",
        "__atom_types", "__names", "__periodic", "__rdfs", "__shells_scanned"]

    def __init__(self, vectors, atom_lists, names=None, periodic=None):
        # Process the vector list and retrieve the number of dimensions
        self.__vectors = numpy.array(vectors, dtype=float)
        if self.__vectors.ndim != 2 or self.__vectors.shape[0] != self.__vectors.shape[1]:
            raise ValueError("vectors must be provided as a square matrix")
        self.__dimensions = self.__vectors.shape[1]
        if self.__dimensions < 2:
            raise ValueError("at least two dimensions must be present")

        # Process the atom lists containing coordinates of atoms of different types
        self.__atom_lists = []
        for atom_list in atom_lists:
            atom_list = numpy.array(atom_list, dtype=float)
            if atom_list.ndim != 2 or atom_list.shape[1] != self.__dimensions:
                raise ValueError("atom coordinates must be consistent with cell dimensions")
            self.__atom_lists.append(atom_list)
        self.__atom_types = len(self.__atom_lists)

        # Process the list of names
        if names is None:
            names = [chr(ord("A") + index) for index in range(self.__atom_types)]
        self.__names = list(names)
        if len(set(self.__names)) != len(self.__names):
            raise ValueError("all atom type names must be unique")
        if len(self.__names) != self.__atom_types:
            raise ValueError("number of atom type names must match number of atom types")

        # Process the periodicity specifications
        if periodic is not None:
            self.__periodic = numpy.array(periodic, dtype=bool)
            if self.__periodic.ndim != 1 or self.__periodic.shape[0] != self.__dimensions:
                raise ValueError("periodicity specifications must be consistent with cell dimensions")
        else:
            self.__periodic = numpy.ones((self.__dimensions,), dtype=bool)

        # Set up RDF array
        self.__rdfs = [[collections.Counter() \
            for target_type_index in range(source_type_index + 1)] \
            for source_type_index in range(self.__atom_types)]
        # Store number of shells scanned (0 = current image alone)
        self.__shells_scanned = -1

    def __repr__(self):
        return "<pyphase.crystal.Cell at 0x{:x} in {}D {}{}>".format(id(self), self.__dimensions, \
            "".join("p" if periodic else "n" for periodic in self.__periodic), \
            " with {}".format(", ".join("{} {}".format(self.__atom_lists[type_index].shape[0], self.__names[type_index]) \
            for type_index in range(len(self.__atom_lists)))) if self.__atom_types else "")

    def __eq__(self, other):
        if not isinstance(other, Cell):
            return NotImplemented
        return CellTools.identical(self, other)

    def __ne__(self, other):
        if not isinstance(other, Cell):
            return NotImplemented
        return not self.__eq__(other)

    def __hash__(self):
        return hash((bytes(self.__vectors.data), \
            tuple(bytes(atom_list.data) for atom_list in self.__atom_lists), tuple(self.__names),
            bytes(self.__periodic.data)))
    
    @property
    def dimensions(self):
        """
        Retrieves the number of spatial dimensions of the cell.

        Returns
        -------
        int
            The number of dimensions.
        """

        return self.__dimensions
    
    @property
    def vectors(self):
        """
        Retrieves the cell vectors.

        Returns
        -------
        numpy.ndarray
            Cell vectors as a matrix of row vectors.
        """

        return self.__vectors.copy()
    
    def vector(self, vector_index):
        """
        Retrieves a cell vector.

        Parameters
        ----------
        vector_index : int
            Index of the cell vector to retrieve.

        Returns
        -------
        numpy.ndarray
            Row vector requested.
        """

        return self.__vectors[vector_index].copy()
    
    @property
    def periodic(self):
        """
        Retrieves the periodicity specifications.

        Returns
        -------
        numpy.ndarray
            Whether or not each direction is periodic.
        """

        return self.__periodic.copy()
    
    @staticmethod
    def __space(vectors):
        """
        Retrieves the space enclosed by a parallelotope.

        Parameters
        ----------
        vectors : numpy.ndarray
            A matrix of row vectors defining the edges of the parallelotope, which
            need not be square.

        Returns
        -------
        float
            The space (area for a two-dimensional parallelotope, volume for a three-
            dimensional parallelotope) enclosed.
        """

        return numpy.sqrt(numpy.linalg.det(numpy.dot(vectors, vectors.T)))

    @property
    def enclosed(self):
        """
        Retrieves the space enclosed by the cell.  If the number of dimensions for
        the cell is :math:`N`, this quantity is :math:`N`-dimensional.

        Returns
        -------
        float
            The space (area for a two-dimensional cell, volume for a three-
            dimensional cell) enclosed.
        """

        return Cell.__space(self.__vectors)
    
    @property
    def surface(self):
        """
        Retrieves the surface space of the cell.  If the number of dimensions for
        the cell is :math:`N`, this quantity is :math:`(N-1)`-dimensional.

        Returns
        -------
        float
            The space (length for a two-dimensional cell, area for a three-
            dimensional cell) of the surface.
        """

        return 2 * sum(Cell.__space(numpy.array([self.vectors[vector_index] \
            for vector_index in range(self.__dimensions) \
            if vector_index != dimension_index])) \
            for dimension_index in range(self.__dimensions))

    @property
    def distortion_factor(self):
        """
        Retrieves the normalized distortion factor of the cell.  This parameter
        is discussed in de Graaf *et al.*, J. Chem. Phys. 137, 214101, 2012
        (`doi:10.1063/1.4767529 <https://dx.doi.org/10.1063/1.4767529>`_).

        Returns
        -------
        float
            The distortion factor (1 for a cell whose vectors are all orthogonal
            to each other and of equal length, or greater than 1 otherwise).
        """
        
        # Calculate the distortion factor and divide by the normalization factor
        # of 2N such that the lower bound on the result is always 1
        average_length = sum(numpy.linalg.norm(self.__vectors[dimension_index]) \
            for dimension_index in range(self.__dimensions)) / self.__dimensions
        distortion_factor = average_length * self.surface / (2 * self.enclosed * self.__dimensions)
        return max(1.0, distortion_factor) # in case of transient rounding errors

    @property
    def normals(self):
        """
        Retrieves normals to faces of cell planes.  For a cell with a diagonal
        vector matrix, these normals will point in the same direction as the vectors.

        Returns
        -------
        numpy.ndarray
            A matrix of normals as row vectors, beginning with the normal of the
            plane such that all but the first of the cell vectors are coplanar.
        """

        # Calculate normals to cell planes
        return numpy.array([numpy.array([numpy.linalg.det( \
            numpy.array([self.__vectors[index] for index in range(self.__dimensions) if index != dimension_index]) \
            [:, [index for index in range(self.__dimensions) if index != component_index]]) * (-1 if component_index % 2 else 1) \
            for component_index in range(self.__dimensions)]) * (-1 if dimension_index % 2 else 1) \
            for dimension_index in range(self.__dimensions)])
    
    @property
    def atom_types(self):
        """
        Retrieves the number of atom types.

        Returns
        -------
        int
            Number of types.
        """

        return self.__atom_types
    
    @property
    def atom_counts(self):
        """
        Retrieves the number of atoms of all types.

        Returns
        -------
        list(int)
            Numbers of atoms.
        """

        return [self.__atom_lists[type_index].shape[0] \
            for type_index in range(self.__atom_types)]

    def atom_count(self, type_specifier=None):
        """
        Retrieves the number of atoms of a given type, or all types.

        Parameters
        ----------
        type_specifier : int or str or None
            The index or name of the atom type.  If None, the total number of atoms
            is returned.

        Returns
        -------
        int
            Number of atoms.
        """
        
        if type_specifier is None:
            return sum(self.atom_counts)
        if not isinstance(type_specifier, int):
            type_specifier = self.index(type_specifier)
        return self.__atom_lists[type_specifier].shape[0]

    @property
    def specific_enclosed(self):
        """
        Retrieves the specific enclosed space of the cell.

        Returns
        -------
        float
            Enclosed space, divided by the number of atoms in the cell.
        """

        return self.enclosed / self.atom_count()

    @property
    def specific_surface(self):
        """
        Retrieves the specific surface space of the cell.

        Returns
        -------
        float
            Surface space, divided by the number of atoms in the cell.
        """

        return self.surface / self.atom_count()
    
    @property
    def density(self):
        """
        Retrieves the number density of the cell.

        Returns
        -------
        float
            Number density, with units of inverse :math:`N`-dimensional volume.
        """

        return 1 / self.specific_enclosed
    
    @property
    def atom_lists(self):
        """
        Retrieves the coordinates of all atoms.

        Returns
        -------
        list(numpy.ndarray)
            List of matrices of row vectors.
        """

        return [self.__atom_lists[type_index].copy() for type_index in range(self.__atom_types)]
    
    def atoms(self, type_specifier):
        """
        Retrieves the coordinates of atoms of a given type.

        Parameters
        ----------
        type_specifier : int or str
            The index or name of the atom type.

        Returns
        -------
        numpy.ndarray
            Matrix of row vectors containing the coordinates.
        """
        
        if not isinstance(type_specifier, int):
            type_specifier = self.index(type_specifier)
        return self.__atom_lists[type_specifier].copy()

    def atom(self, type_specifier, atom_index):
        """
        Retrieves the coordinates of a given atom.

        Parameters
        ----------
        type_specifier : int or str
            The index or name of the atom type.
        atom_index : int
            The index of the atom.

        Returns
        -------
        numpy.ndarray
            Row vector of the atomic coordinates.
        """
        
        if not isinstance(type_specifier, int):
            type_specifier = self.index(type_specifier)
        return self.__atom_lists[type_specifier][atom_index].copy()
    
    @property
    def names(self):
        """
        Retrieves the names of all atom types.

        Returns
        -------
        list(str)
            Names of atom types.
        """

        return list(self.__names)

    def name(self, type_index):
        """
        Retrieves the name of an atom type.

        Parameters
        ----------
        type_index : int
            The index of the atom type.

        Returns
        -------
        str
            Name of the atom type.
        """

        return self.__names[type_index]
    
    def index(self, type_name):
        """
        Retrieves the index of an atom type.

        Parameters
        ----------
        type_name : str
            The name of the atom type.

        Returns
        -------
        int
            Index of the atom type.
        """
        
        return self.__names.index(type_name)

    def __scan_shell(self, source_type_index, target_type_index, shell):
        """
        Scans a specific shell of periodic images for contacts between a
        certain pair of atom types.

        Parameters
        ----------
        source_type_index : int
            The index of the source atom type.
        target_type_index : int
            The index of the target atom type.
        shell : int
            Which shell of periodic images to scan.  Specifying 0 scans
            within the cell itself, specifying 1 scans the immediate neighbors
            (8 in 2D, 26 in 3D) around the cell, specifying 2 scans their
            neighbors (16 in 2D, 98 in 3D), and so on.

        Returns
        -------
        collections.Counter(float)
            The generated un-normalized RDF as a :py:class:`collections.Counter`
            object representing a sum of Dirac delta functions.
        """
        
        # Initialize
        rdf = collections.Counter()
        source_list = self.__atom_lists[source_type_index]
        target_list = self.__atom_lists[target_type_index]

        for cell_coordinates in itertools.product(*(range(-shell, shell + 1) \
            if self.__periodic[dimension_index] else (0,) \
            for dimension_index in range(self.__dimensions))):

            # Ignore cell images not in the desired shell
            if numpy.max(numpy.abs(cell_coordinates)) != shell:
                continue

            # Perform the calculations for the current cell image
            shift_vector = numpy.dot(self.__vectors.T, cell_coordinates)
            for source_atom in source_list:
                for target_atom in target_list:
                    distance = numpy.linalg.norm(shift_vector + target_atom - source_atom)
                    if distance:
                        rdf[distance] += 1

        return rdf

    def measure_to(self, *, shell_cutoff=None, distance_cutoff=None):
        """
        Scans pairwise separation distances for all pairs of atoms of all types.
        The measurement can be specified to stop after scanning a certain explicitly
        specified number of periodic image shells, or after scanning enough shells to
        take measurements to a certain distance.  At least one of the two cutoffs must
        be specified; if both are specified, scanning will stop whenever the first
        cutoff is reached.  The generated RDFs will be stored internally within the
        :py:class:`Cell` instance.  Although this method will be invoked automatically
        when contact information or an RDF is requested, it can be called
        manually to generate data that can be saved using :py:meth:`write_rdf`.

        Parameters
        ----------
        shell_cutoff : int or None
            Cutoff in terms of periodic image shells.
        distance_cutoff : float or None
            Cutoff in terms of pairwise separation distance.

        Raises
        ------
        ValueError
            Neither cutoff was specified.

        Notes
        -----
        Measurement may be quite slow for large values of the distance cutoff
        parameter and/or cells with a large distortion factor.  A calculation is
        performed to ensure that enough periodic image shells will always be scanned
        to generate accurate information; in some cases, this can be a large number of
        shells.  Consider using :py:meth:`CellTools.reduce` to reduce distortion
        and improve performance if necessary.
        """
        
        # Make sure a cutoff is specified
        if shell_cutoff is None and distance_cutoff is None:
            raise ValueError("one of the two cutoffs must be specified")

        # Determine number of shells to scan
        if distance_cutoff is not None:
            minimum_distance = min([abs(numpy.dot(plane_normal, self.__vectors[index]) / numpy.linalg.norm(plane_normal)) \
                for index, plane_normal in enumerate(self.normals)])
            shell_stop = int(numpy.ceil(distance_cutoff / minimum_distance))
            if shell_cutoff is not None:
                shell_stop = min(shell_stop, shell_cutoff)
        else:
            shell_stop = shell_cutoff
        
        # Scan shells
        for self.__shells_scanned in range(self.__shells_scanned + 1, shell_stop + 1):
            for source_type_index in range(self.__atom_types):
                for target_type_index in range(source_type_index + 1):
                    self.__rdfs[source_type_index][target_type_index] += \
                        self.__scan_shell(source_type_index, target_type_index, self.__shells_scanned)

    def read_rdf(self, binary_file):
        """
        Reads RDF information from a binary file.

        Parameters
        ----------
        binary_file : str or io.BufferedIOBase
            The file from which to read information.

        Notes
        -----
        Reading RDF information from a cell with different vectors and atomic
        coordinates may not result in immediate errors if the dimensions and numbers
        of atoms in the cells are equal; however, RDFs and contact information may
        be invalid.
        """
        
        with CellCodecs._file(binary_file, "rb") as real_file:
            self.__rdfs, self.__shells_scanned = pickle.load(real_file)
    
    def write_rdf(self, binary_file):
        """
        Writes RDF information to a binary file.

        Parameters
        ----------
        binary_file : str or io.BufferedIOBase
            The file to which to write information.
        """
        
        with CellCodecs._file(binary_file, "wb") as real_file:
            pickle.dump((self.__rdfs, self.__shells_scanned), real_file)

    def rdf(self, source_type_specifier, target_type_specifier, distance):
        """
        Retrieves a discrete RDF.  No normalization is applied.

        Parameters
        ----------
        source_type_specifier : int or str
            The index or name of the source atom type.
        target_type_specifier : int or str
            The index or name of the target atom type.
        distance : float
            The distance to which the RDF should be measured.

        Returns
        -------
        dict(float, int)
            RDF as a dictionary.  Each entry has as its key a distance, and
            as its value the number of atoms of the target type found at a
            distance away from atoms of the source type.
        """
        
        if not isinstance(source_type_specifier, int):
            source_type_specifier = self.index(source_type_specifier)
        if not isinstance(target_type_specifier, int):
            target_type_specifier = self.index(target_type_specifier)
        if target_type_specifier > source_type_specifier:
            source_type_specifier, target_type_specifier = target_type_specifier, source_type_specifier

        self.measure_to(distance_cutoff=distance)
        return { key: value for key, value in self.__rdfs[source_type_specifier][target_type_specifier].items() if key <= distance }

    def contact(self, source_type_specifier, target_type_specifier):
        """
        Retrieves atomic minimum contact information.

        Parameters
        ----------
        source_type_specifier : int or str
            The index or name of the source atom type.
        target_type_specifier : int or str
            The index or name of the target atom type.

        Returns
        -------
        float
            The minimum distance between atoms of the two types.
        """
        
        if not isinstance(source_type_specifier, int):
            source_type_specifier = self.index(source_type_specifier)
        if not isinstance(target_type_specifier, int):
            target_type_specifier = self.index(target_type_specifier)
        if target_type_specifier > source_type_specifier:
            source_type_specifier, target_type_specifier = target_type_specifier, source_type_specifier

        self.measure_to(shell_cutoff=1)
        return min(self.__rdfs[source_type_specifier][target_type_specifier])

    def scale_factor(self, radii):
        """
        Calculates (based on minimum contact information) a scale factor between
        the length scale of provided atomic radii and the length scale of cell
        vectors and atomic positions.

        Parameters
        ----------
        radii : tuple(float)
            The radii of the atom types.
        
        Raises
        ------
        ValueError
            An invalid number of radii were provided.

        Returns
        -------
        float
            The requested scale factor.

        Notes
        -----
        Suppose that the cell vectors' coordinates have physically significant units
        and the atomic radii provided are relative.  In this case, multiply the radii
        by the scale factor to obtain their values in the units of the cell vectors.

        Alternatively, suppose that the atomic radii provided are in physically
        significant units and the cell vectors' coordinates are relative.  Dividing
        the coordinates of the cell vectors by the scale factor will yield their values
        in the units of the atomic radii.
        """

        # Check number of radii
        if len(radii) != len(self.__atom_lists):
            raise ValueError("Number of radii must correspond with number of atom types")

        # Rescale radii for proper cutoff information
        ratio = lambda indices: self.contact(*indices) / sum(radii[index] for index in indices)
        source_type_index, target_type_index = min(((source_type_index, target_type_index) \
            for source_type_index in range(len(self.__atom_lists)) \
            for target_type_index in range(len(self.__atom_lists))), key=ratio)
        return ratio((source_type_index, target_type_index))

class CellTools:
    """
    Contains tools for performing simple manipulations on cells, such as scaling and shearing cells,
    generating periodic supercells, wrapping atoms in accordance with periodic boundary conditions,
    and optimization of sheared cell representations.
    """

    @staticmethod
    def similar(cell_1, cell_2, tolerance=1e-6):
        """
        Tests whether or not two cells are equal.  The comparison of numerical values
        between the two cells is performed with some amount of tolerance.  Note that this
        is not a generalized similarity test and does not compensate for rotation, scaling,
        and different numbers of periodic images between the cells.

        Parameters
        ----------
        cell_1 : Cell
            The first cell to compare.
        cell_2 : Cell
            The second cell to compare.
        tolerance : float
            The tolerance to use during comparison of cell vectors and atom
            coordinates.

        Returns
        -------
        bool
            Whether or not the cells are equal.
        """

        return cell_1.dimensions == cell_2.dimensions and cell_1.atom_counts == cell_2.atom_counts and \
            numpy.allclose(cell_1.vectors, cell_2.vectors, rtol=tolerance, atol=tolerance) and \
            all(numpy.allclose(cell_1.atoms(index), cell_2.atoms(index), rtol=tolerance, atol=tolerance) \
            for index in range(cell_1.atom_types)) and cell_1.names == cell_2.names \
            and numpy.all(cell_1.periodic == cell_2.periodic)

    @staticmethod
    def identical(cell_1, cell_2):
        """
        Tests whether or not two cells are equal.  The comparison of numerical values
        between the two cells is performed exactly.

        Parameters
        ----------
        cell_1 : Cell
            The first cell to compare.
        cell_2 : Cell
            The second cell to compare.

        Returns
        -------
        bool
            Whether or not the cells are equal.
        """
        
        return cell_1.dimensions == cell_2.dimensions and cell_1.atom_counts == cell_2.atom_counts and \
            numpy.array_equal(cell_1.vectors, cell_2.vectors) and \
            all(numpy.array_equal(cell_1.atoms(index), cell_2.atoms(index)) \
            for index in range(cell_1.atom_types)) and cell_1.names == cell_2.names \
            and numpy.all(cell_1.periodic == cell_2.periodic)
    
    @staticmethod
    def rename(cell, type_map):
        """
        Renames atom types in a cell.  The manner in which the atoms are stored internally will
        not be modified, only the names of the atom types will be reassigned.  It is not possible
        to merge two atom types into one using this routine; in order to do so, see
        :py:meth:`reassign`.

        Parameters
        ----------
        cell : Cell
            The cell to modify.
        type_map : dict(int or str, int or str)
            A dictionary mapping current atom type identifiers to new atom type identifiers.
            Any indices will be converted to their respective names before renaming.

        Raises
        ------
        TypeError
            Invalid or inconsistent types in type map.
        ValueError
            Unrecognized indices or names, or invalid type map specification.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        # Converts indices to names
        def normalize(item):
            if isinstance(item, int):
                return cell.name(item)
            return item

        # Check types in type map
        normalized_type_map = {}
        for key, value in type_map.items():
            normalized_type_map[normalize(key)] = normalize(value)
        type_map = normalized_type_map

        # Try to perform the renaming operations
        new_names = cell.names
        for key, value in type_map.items():
            if key not in cell.names:
                raise ValueError("invalid value encountered in type map")
            new_names[cell.index(key)] = value
        
        # Check result
        if len(set(new_names)) != len(new_names):
            raise ValueError("invalid value encountered in type map")

        return Cell(cell.vectors, cell.atom_lists, new_names, cell.periodic)


    @staticmethod
    def reassign(cell, type_map):
        """
        Reassigns or reorders atom types in a cell.  Multiple atom types can be merged
        into one, and atom types can be dropped completely if desired.  This routine has
        different behavior from :py:meth:`rename`; a call to :py:meth:`reassign` which
        does not merge any atom types together may still perform a different action from
        the corresponding call to :py:meth:`rename`.  To understand the differences
        between these two routines and to get a better sense of their use, consider the
        following representative examples given this cell::

            >>> cell
            <pyphase.crystal.Cell in 2D with 1 A, 2 B>

        Basic usage.  Renaming changes names only, while reassignment moves atoms around
        between the atom lists::

            >>> CellTools.rename(cell, {"A": "B", "B": "A"})
            <pyphase.crystal.Cell in 2D with 1 B, 2 A>
            >>> CellTools.reassign(cell, {"A": "B", "B": "A"})
            <pyphase.crystal.Cell in 2D with 2 B, 1 A>

        When renaming, destination names need not exist.  However, reassignment fails
        since there is no atom type C to add the A atoms to::

            >>> CellTools.rename(cell, {"A": "C", "B": "A"})
            <pyphase.crystal.Cell in 2D with 1 C, 2 A>
            >>> CellTools.reassign(cell, {"A": "C", "B": "A"})
            ValueError: 'C' is not in list

        Not all types need to be specified.  Renaming will occur unless this results in
        duplicated names.  Reassignment will occur as long as the resulting atom types
        form a contiguous range.  Note that reassignment may cause types to be deleted.
        Furthermore, reassignment can be used to merge types::

            >>> CellTools.rename(cell, {"A": "B"})
            ValueError: invalid value encountered in type map
            >>> CellTools.reassign(cell, {"A": "B"})
            ValueError: invalid entry encountered in type map

            >>> CellTools.rename(cell, {"B": "A"})
            ValueError: invalid value encountered in type map
            >>> CellTools.reassign(cell, {"B": "A"})
            <pyphase.crystal.Cell in 2D with 2 B>

            >>> CellTools.rename(cell, {"A", "C"})
            <pyphase.crystal.Cell in 2D with 1 C, 2 B>
            >>> CellTools.reassign(cell, {"A", "C"})
            ValueError: 'C' is not in list

            >>> CellTools.rename(cell, {"A": "A", "B": "A"})
            ValueError: invalid value encountered in type map
            >>> CellTools.reassign(cell, {"A": "A", "B": "A"})
            <pyphase.crystal.Cell in 2D with 3 A>

        Indices can be provided in place of names.  Note that when using :py:meth:`rename`,
        indices are converted to names, while with :py:meth:`reassign`, names are converted
        to indices::

            >>> CellTools.reassign(cell, {1: 0})
            <pyphase.crystal.Cell in 2D with 2 B>

            >>> CellTools.rename(cell, {0: "C", 1: 0})
            <pyphase.crystal.Cell in 2D with 1 C, 2 A>

            >>> CellTools.reassign(cell, {0: 1})
            ValueError: invalid entry encountered in type map

        Parameters
        ----------
        cell : Cell
            The cell to modify.
        type_map : dict(int or str, int or str)
            A dictionary mapping current atom type identifiers to new atom type identifiers.
            Any names will be converted to their respective indices before renaming.

        Raises
        ------
        TypeError
            Invalid or inconsistent types in type map.
        ValueError
            Unrecognized indices or names, or invalid type map specification.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        # Converts names to indices
        def normalize(item):
            if not isinstance(item, int):
                return cell.index(item)
            return item

        # Check types in type map
        normalized_type_map = {}
        for key, value in type_map.items():
            normalized_type_map[normalize(key)] = normalize(value)
        type_map = normalized_type_map

        # Check indices for validity
        sources, destinations = set(type_map.keys()), set(type_map.values())
        if len(sources) > cell.atom_types or len(destinations) != max(destinations) + 1 \
            or any(source not in range(cell.atom_types) for source in sources) \
            or min(destinations) != 0:
                raise ValueError("invalid entry encountered in type map")

        # Reorder atoms, possibly with condensation
        atom_lists = [numpy.array([cell.atom(source_type_index, atom_index) \
            for source_type_index in range(max(sources) + 1) \
            for atom_index in range(cell.atom_count(source_type_index)) \
            if source_type_index in type_map and type_map[source_type_index] == target_type_index]) \
            for target_type_index in range(max(destinations) + 1)]

        # Generate names, possibly losing names if atom types are being lost
        names = [cell.name(min(source_type_index \
            for source_type_index in range(max(sources) + 1) \
            if source_type_index in type_map and type_map[source_type_index] == target_type_index)) \
            for target_type_index in range(max(destinations) + 1)]

        return Cell(cell.vectors, atom_lists, names, cell.periodic)

    @staticmethod
    def scale(cell, new_vectors, move_vectors=True, move_atoms=True):
        """
        Rescales a cell by applying new vectors.

        Parameters
        ----------
        cell : Cell
            The cell to scale.
        new_vectors : numpy.ndarray
            A matrix containing new row vectors.
        move_vectors : bool
            Whether or not to apply the scaling to the vectors of the cell.
        move_atoms : bool
            Whether or not to apply the scaling to the atoms of the cell.

        Raises
        ------
        ValueError
            Invalid vectors were received.

        Returns
        -------
        Cell
            The modified cell.
        """

        # Check the vectors
        vectors = cell.vectors
        if new_vectors.shape != vectors.shape:
            raise ValueError("vectors must be provided as a square matrix")

        # Generate transformation matrices from old coordinate space to normalized space
        # (in which all coordinates inside the cell are in the range [0, 1]) and back to
        # the new coordinate space with new vectors
        normalized_to_coordinate = new_vectors.T
        coordinate_to_normalized = numpy.linalg.inv(vectors.T)

        # Adjust the coordinates
        atom_lists = [numpy.dot(normalized_to_coordinate, numpy.dot(coordinate_to_normalized, cell.atoms(type_index).T)).T \
            for type_index in range(cell.atom_types)] \
            if move_atoms else cell.atom_lists
        
        # Adjust the vectors
        return Cell(new_vectors if move_vectors else vectors, atom_lists, cell.names, cell.periodic)

    @staticmethod
    def scale_by(cell, scale_factor, move_vectors=True, move_atoms=True):
        """
        Rescales a cell isotropically.

        Parameters
        ----------
        cell : Cell
            The cell to scale.
        scale_factor : float
            An isotropic scale factor.
        move_vectors : bool
            Whether or not to apply the scaling to the vectors of the cell.
        move_atoms : bool
            Whether or not to apply the scaling to the atoms of the cell.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        return CellTools.scale(cell, cell.vectors * scale_factor, move_vectors, move_atoms)
    
    @staticmethod
    def scale_by_factor(cell, radii, move_vectors=True, move_atoms=True):
        """
        Rescales a cell based on :py:meth:`Cell.scale_factor`.

        Parameters
        ----------
        cell : Cell
            The cell to scale.
        radii : tuple(float)
            The radii of the atom types.
        move_vectors : bool
            Whether or not to apply the scaling to the vectors of the cell.
        move_atoms : bool
            Whether or not to apply the scaling to the atoms of the cell.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        return CellTools.scale_by(cell, 1 / cell.scale_factor(radii), move_vectors, move_atoms)

    @staticmethod
    def scale_to(cell, density, move_vectors=True, move_atoms=True):
        """
        Rescales a cell isotropically to match a specified number density.

        Parameters
        ----------
        cell : Cell
            The cell to scale.
        density : float
            The targeted number density.
        move_vectors : bool
            Whether or not to apply the scaling to the vectors of the cell.
        move_atoms : bool
            Whether or not to apply the scaling to the atoms of the cell.

        Returns
        -------
        Cell
            The modified cell.
        """

        return CellTools.scale_by(cell, (cell.density / density) ** (1 / cell.dimensions), move_vectors, move_atoms)

    @staticmethod
    def normalize(cell):
        """
        Normalizes the vectors of a cell such that the vector matrix is triangular.
        The atoms within are moved to compensate such that histograms should not be
        changed. This is the format for triclinic cells used by LAMMPS.

        Parameters
        ----------
        cell : Cell
            The cell whose vectors are to be normalized.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        # Retrieve cell vectors and create array for new vectors
        vectors = cell.vectors
        new_vectors = numpy.zeros_like(vectors)

        # Before beginning, check sign of determinant and flip a vector if needed
        if numpy.linalg.det(vectors) < 0:
            vectors[0] *= -1
        
        # Populate matrix
        for component_index in range(cell.dimensions):
            # Fill diagonal element of this column
            new_vectors[component_index][component_index] = \
                numpy.sqrt((numpy.linalg.norm(vectors[component_index]) ** 2) - \
                sum(new_vectors[component_index][index] ** 2 \
                for index in range(component_index)))
            
            # Fill off diagonal (lower triangular) elements
            for vector_index in range(component_index, cell.dimensions):
                new_vectors[vector_index][component_index] = \
                    (numpy.dot(vectors[component_index], vectors[vector_index]) - \
                    sum(new_vectors[component_index][index] * new_vectors[vector_index][index] \
                    for index in range(component_index))) / new_vectors[component_index][component_index]
        
        return CellTools.scale(cell, new_vectors)

    @staticmethod
    def wrap(cell):
        """
        Wraps atoms within a cell based on periodic boundary conditions.  If periodicity
        is disabled in a particular direction, wrapping will not occur in that direction.

        Parameters
        ----------
        cell : Cell
            The cell whose atoms are to be wrapped.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        # Generate transformation matrices from coordinate space to normalized space
        # (in which all coordinates inside the cell are in the range [0, 1]) and back
        normalized_to_coordinate = cell.vectors.T
        coordinate_to_normalized = numpy.linalg.inv(normalized_to_coordinate)

        # Transform, wrap, and transform back
        def wrap_reduced(type_index):
            # Reduced coordinates are in [0, 1]
            reduced = numpy.dot(coordinate_to_normalized, cell.atoms(type_index).T)

            # Dimensions have been transposed: filter by dimension index
            reduced[cell.periodic, :] %= 1
            reduced[cell.periodic, :] %= 1
            # It is necessary to do this twice if we wish to be certain the reduced
            # coordinates lie exactly in [0, 1).  For example, if x = -2 ** -53,
            # x % 1 = x % 1 % 1 = 0.9999999999999999.  However, if x = -2 ** -54,
            # x % 1 = 1.0 while x % 1 % 1 = 0.0.
            
            return numpy.dot(normalized_to_coordinate, reduced).T
        atom_lists = [wrap_reduced(type_index) for type_index in range(cell.atom_types)]
        return Cell(cell.vectors, atom_lists, cell.names, cell.periodic)

    @staticmethod
    def condense(cell, wrap=True, tolerance=1e-6):
        """
        Removes duplicate atoms from a cell.  Only atoms of the same type are
        candidates for being duplicates.
        
        Parameters
        ----------
        cell : Cell
            The cell from which to remove duplicate atoms.
        wrap : bool
            Whether or not to apply :py:meth:`wrap` before condensing (if atoms exist
            outside the cell, duplicate detection may function improperly).
        tolerance : float
            The distance two atoms of the same type must be from each other
            to be considered separate atoms.

        Returns
        -------
        Cell
            The modified cell.
        """

        # Wrap first if desired
        if wrap:
            cell = CellTools.wrap(cell)

        atom_lists = []
        for type_index in range(cell.atom_types):
            # Find all overlaps within this atom type
            distances = scipy.spatial.distance.squareform( \
                scipy.spatial.distance.pdist(cell.atoms(type_index), metric=lambda u, v: \
                min(numpy.linalg.norm(u - v + numpy.dot(cell.vectors.T, numpy.array(offset))) \
                for offset in itertools.product(*(range(-1, 2) if cell.periodic[dimension_index] else (0,) \
                for dimension_index in range(cell.dimensions))))))
            overlaps = distances < tolerance

            # Look at all pairs of atoms
            atoms_to_delete = set()
            for source_atom_index in range(cell.atom_count(type_index)):
                for target_atom_index in range(cell.atom_count(type_index)):
                    # Don't process pairs with the same atom selected twice
                    if source_atom_index == target_atom_index:
                        continue

                    # If an overlap is found, remove one atom and update the array
                    if overlaps[source_atom_index][target_atom_index]:
                        atoms_to_delete.add(target_atom_index)
                        overlaps[source_atom_index, target_atom_index] = False
                        overlaps[target_atom_index, :] = False

            # Rebuild the atom list for this atom type
            atom_lists.append(numpy.array([cell.atom(type_index, atom_index) \
                for atom_index in range(cell.atom_count(type_index)) \
                if atom_index not in atoms_to_delete]))
        return Cell(cell.vectors, atom_lists, cell.names, cell.periodic)
    
    @staticmethod
    def shift(cell, atom_type=0, atom_index=0, position=None):
        """
        Shifts atoms in a cell such that the specified atom is at a
        desired position.

        Parameters
        ----------
        cell : Cell
            The cell to modify.
        atom_type : int or str
            The type (index or name) of the atom to select as a reference
            point.  If not specified, defaults to the first type.
        atom_index : int
            The index of the atom (of the selected type) to choose as
            a reference point.  If not specified, defaults to the first
            atom of the selected type.
        position : numpy.ndarray
            A row vector specifying the desired new position of the
            reference atom.  All atoms in the cell will be shifted by
            the same vector to effect this change.  If unspecified, the
            selected atom will be moved to the origin.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        # Check arguments
        if isinstance(atom_type, str):
            atom_type = cell.index(atom_type)
        if position is None:
            position = numpy.zeros((1, cell.dimensions))

        # Determine required vector and shift the atoms
        shift_vector = position - cell.atom(atom_type, atom_index)
        return Cell(cell.vectors, [cell.atoms(type_index) + \
            numpy.tile(shift_vector, cell.atom_count(type_index)).reshape(-1, cell.dimensions) \
            for type_index in range(cell.atom_types)], cell.names, cell.periodic)
    
    @staticmethod
    def tile(cell, repeats, partial_radii=None, partial_tolerance=1e-6):
        """
        Generates a periodic supercell.  If desired, additional atoms can
        be added at the edges of the resulting cell (for example, when visualizing
        a unit cell).

        Parameters
        ----------
        cell : Cell
            The cell to tile.
        repeats : tuple(int)
            The number of repeats to make in each direction.
        partial_radii : tuple(float)
            The radii of atoms of each type in the cell, if it is desired to
            add additional atoms.  These values are used to determine whether
            an atom whose center lies outside the final supercell is still
            partially within its bounds and should be included.  If this parameter
            is not specified, no additional atoms will be added to the supercell.
        partial_tolerance : float
            An adjustment value used when determining whether an atom whose center
            is outside of a cell still rests within it.  The distance from the
            center of a sphere to the plane cutting through it must be this much
            less than the radius of the sphere.

        Raises
        ------
        ValueError
            Inconsistent dimensions or invalid specifications.

        Returns
        -------
        Cell
            The generated supercell.
        """

        # Check the repeat counts
        if len(repeats) != cell.dimensions:
            raise ValueError("repeats must be consistent with cell dimensions")
        for repeat in repeats:
            if repeat != int(repeat) or repeat < 1:
                raise ValueError("repeat values must be positive integers")
        
        # Add extra repeats at beginning and end if partials are requested
        repeats = numpy.array(repeats)
        new_vectors = cell.vectors * numpy.repeat(repeats, cell.dimensions).reshape(cell.dimensions, cell.dimensions)
        if partial_radii is not None:
            transformation = numpy.linalg.inv(new_vectors.T)
            repeats += 2

            # Rescale radii for proper cutoff information
            partial_radii = numpy.array(partial_radii) * cell.scale_factor(partial_radii)

            # Precompute plane normals
            precomputed_normals = cell.normals

            # Determines the nearest distance from a plane to a point
            plane_point = lambda plane_origin, plane_normal, target_point: \
                abs(numpy.dot(plane_normal, target_point - plane_origin) / numpy.linalg.norm(plane_normal))

            # Define criterion for retaining atom
            def good_partial(type_index, atom_coordinates):
                normalized_coordinates = numpy.dot(transformation, atom_coordinates.T).T
                under_range, over_range = normalized_coordinates < 0, normalized_coordinates > 1
                # Check distance to all planes in question
                if numpy.any(under_range):
                    for dimension_index in range(cell.dimensions):
                        if not cell.periodic[dimension_index]:
                            continue
                        if under_range[dimension_index]:
                            if plane_point(numpy.zeros(cell.dimensions), precomputed_normals[dimension_index], \
                                atom_coordinates) >= partial_radii[type_index] - partial_tolerance:
                                return False
                if numpy.any(over_range):
                    for dimension_index in range(cell.dimensions):
                        if not cell.periodic[dimension_index]:
                            continue
                        if over_range[dimension_index]:
                            if plane_point(new_vectors[dimension_index], precomputed_normals[dimension_index], \
                                atom_coordinates) >= partial_radii[type_index] - partial_tolerance:
                                return False
                # Atom was in contact with all planes for which it was out of range, or is within cell
                return True
        
        # Generate new cell vectors and atom lists
        vectors = cell.vectors.T
        atom_lists = [numpy.array([coordinates + numpy.dot(vectors, (numpy.array(offset) - (1 if partial_radii is not None else 0))) \
            for coordinates in cell.atoms(type_index) \
            for offset in itertools.product(*(range(repeat) \
            for repeat in repeats)) \
            if partial_radii is None or good_partial(type_index, coordinates + numpy.dot(vectors, (numpy.array(offset) - (1 if partial_radii is not None else 0))))]) \
            for type_index in range(cell.atom_types)]
        
        return Cell(new_vectors, atom_lists, cell.names, cell.periodic)
    
    @staticmethod
    def reduce(cell, max_distortion=1, max_iterations=256, normalize=True, shift=None, wrap=True, condense=False, tolerance=1e-6):
        """
        Reduces a sheared cell based on the algorithm discussed in de Graaf *et al.*, J. Chem. Phys. 137, 214101, 2012
        (`doi:10.1063/1.4767529 <https://dx.doi.org/10.1063/1.4767529>`_).  Also can perform other
        normalizations automatically if desired.

        Parameters
        ----------
        cell : Cell
            The cell to reduce.
        max_distortion : float
            The maximum permissible distortion factor.  This value is equal to 1 for
            a non-sheared cell.  This may not be reached if the maximum number of iterations
            is exceeded or the minimum possible distortion factor is greater.
        max_iterations : int
            The maximum number of iterations that will be executed.  Attempts to reduce
            the cell will stop when either the distortion factor decreases below the
            specified threshold or the number of iterations is exceeded.  This algorithm
            should terminate, so exceeding this limit is treated as an error.
        normalize : bool
            Whether or not to normalize cell vectors and atom positions such that the
            cell vector matrix is triangular.
        shift : bool
            Whether or not to shift atoms within the cell such that the first atom of
            the first type is at the origin.  The default behavior will be True if all
            directions in the cell are periodic, and False otherwise.
        wrap : bool
            Whether or not to clean up the cell using :py:meth:`wrap` after optimization.
        condense: bool
            Whether or not to clean up the cell using :py:meth:`condense` after optimization.

        Returns
        -------
        Cell
            The modified cell.
        """
        
        # Prepare a special cell containing vectors alone to avoid overhead of
        # copying around atom position arrays
        vector_cell = Cell(cell.vectors, [], [], cell.periodic)
        # Perform at maximum the specified number of iterations
        for iteration in range(max_iterations):
            # If the cell is not particularly distorted at this point, stop
            if cell.distortion_factor <= max_distortion:
                break

            # Calculate the initial surface space and look for a reduction
            initial_surface = vector_cell.surface
            new_cells = [Cell(numpy.array([ \
                    vector_cell.vector(vector_index) + (direction * vector_cell.vectors[modifying_vector_index] \
                    if modified_vector_index == vector_index else 0) \
                    for vector_index in range(cell.dimensions)
                ]), [], [], cell.periodic) \
                for direction in [-1, 1]
                for modified_vector_index in range(cell.dimensions) \
                for modifying_vector_index in range(cell.dimensions) \
                if modified_vector_index != modifying_vector_index \
                    and cell.periodic[modifying_vector_index] \
                    and cell.periodic[modified_vector_index]]
            
            # Find the best modified cell
            old_vector_cell = vector_cell
            vector_cell = min(new_cells + [vector_cell], key=lambda cell: cell.surface)
            # No further iterations will improve outcome
            if vector_cell.surface >= old_vector_cell.surface:
                break
        else:
            raise RuntimeError("exceeded number of iterations in reduction")

        # Perform cleaning operations if desired and deliver the result
        cell = Cell(vector_cell.vectors, cell.atom_lists, cell.names, cell.periodic)
        if shift is None:
            shift = numpy.all(cell.periodic)
        if shift:
            cell = CellTools.shift(cell)
        if normalize:
            cell = CellTools.normalize(cell)
        if wrap:
            cell = CellTools.wrap(cell)
        if condense:
            cell = CellTools.condense(cell, wrap=False, tolerance=tolerance)
        return cell
  
class CellCodecs:
    """
    Contains various codecs for loading and saving :py:class:`Cell` objects' data in different formats.
    Currently LAMMPS format reading and writing are supported, along with XYZ format writing.  There is
    also a custom text-based ".cell" format recommended for use if it is desired to save and load
    :py:class:`Cell` objects losslessly and the :py:mod:`pickle` format cannot be employed.
    """
    
    #: LAMMPS headers to skip.
    __LAMMPS_HEADERS_SKIP = ["bonds", "angles", "dihedrals", "impropers", "bond types", "angle types",
    "dihedral types", "improper types", "extra bond per atom", "extra angle per atom",
    "extra dihedral per atom", "extra improper per atom", "extra special per atom", "ellipsoids",
    "lines", "triangles", "bodies"]
    #: Lower bound on z-axis for 2D systems.
    __LAMMPS_Z_MIN = -0.5
    #: Upper bound on z-axis for 2D systems.
    __LAMMPS_Z_MAX = 0.5

    @staticmethod
    @contextlib.contextmanager
    def _file(file_spec, mode):
        """
        Handles opening and closing of a file from a provided path, or passes through
        a provided file.  Allows for cell reading and writing from file objects or
        paths without duplicating logic.

        Parameters
        ----------
        file_spec : str or io.IOBase
            The path or file.
        mode : str
            The file mode to use if a path is provided.
        """

        if isinstance(file_spec, str):
            with open(file_spec, mode) as file_obj:
                yield file_obj
        else:
            yield file_spec

    @staticmethod
    def read_cell(text_file, sequence=False):
        """
        Reads cell data from an ASCII formatted ".cell" file.  The format is documented
        with the :py:meth:`write_cell` function.

        Parameters
        ----------
        text_file : str or io.TextIOBase
            The input file.
        sequence : bool
            Whether or not to read multiple cells.
        
        Raises
        ------
        ValueError
            Invalid data were encountered.

        Returns
        -------
        Cell or list(Cell)
            The cell or cells created from the file.
        """
        
        with CellCodecs._file(text_file, "r") as real_file:
            lines = (line for line in (line.strip() for line in real_file) if line)

            cells = []
            while True:
                # Read the heading line
                try:
                    lengths = [int(value.strip()) for value in next(lines).split()]
                except StopIteration as stop_iteration:
                    if sequence:
                        # Was the end of the sequence
                        break
                    else:
                        # File was empty or ended early
                        raise stop_iteration
                dimensions, list_lengths = lengths[0], lengths[1:]
                if dimensions < 0:
                    raise ValueError("number of dimensions must not be negative")
                for list_length in list_lengths:
                    if list_length < 0:
                        raise ValueError("number of atoms must not be negative")

                # Read atom names
                names = [None] * len(list_lengths)
                for name_index in range(len(list_lengths)):
                    names[name_index] = next(lines).encode().decode("unicode_escape")

                # Read periodicity data
                periodic_lookup = {"n": False, "p": True}
                periodic = [periodic_lookup[character] for character in next(lines)]

                # Read vector data
                vectors = [None] * dimensions
                for vector_index in range(dimensions):
                    vector = [float(value.strip()) for value in next(lines).split()]
                    if len(vector) != dimensions:
                        raise ValueError("vector coordinates must be consistent with cell dimensions")
                    vectors[vector_index] = vector

                # Read atom data
                atom_lists = [None] * len(list_lengths)
                for type_index in range(len(list_lengths)):
                    atom_list = [None] * list_lengths[type_index]
                    for atom_index in range(list_lengths[type_index]):
                        coordinates = [float(value.strip()) for value in next(lines).split()]
                        if len(coordinates) != dimensions:
                            raise ValueError("atom coordinates must be consistent with cell dimensions")
                        atom_list[atom_index] = coordinates
                    atom_lists[type_index] = atom_list
         
                cell = Cell(vectors, atom_lists, names, periodic)
                
                if sequence:
                    # Keep going
                    cells.append(cell)
                else:
                    # Ensure that the end of the file has been reached
                    try:
                        next(lines)
                        raise ValueError("unexpected additional data encountered")
                    except StopIteration:
                        return cell
            return cells

    @staticmethod
    def write_cell(cell, text_file):
        """
        Writes cell data to an ASCII formatted ".cell" file.  These files consist of lines of
        either text (escaped with backslashes in standard Python format) or numbers (separated
        from each other on a line by whitespace).  Comments are not supported but blank lines
        are.

        The first (non-blank) line is a header line, containing the number of dimensions (almost
        always 2 or 3) followed by the number of atoms of each type.  The number of values following
        the dimensions thus specifies the number of atom types in the cell.  This line is followed
        by a number of lines specifying the names of each atom type.  This is followed by a
        specification of the periodicity of the cell.

        This is followed by :math:`N` lines containing :math:`N` values each, where :math:`N` is
        the number of dimensions.  These lines specify the cell vectors (in row vector format).
        Finally, some number of lines with :math:`N` values each follow.  These lines specify the
        positions of atoms (also in row vector format).  All of the atoms of the first type are
        positioned, followed by the atoms of the second type, and so on.

        For example, the following cell::

            Cell(numpy.eye(2), [numpy.zeros((1, 2)), numpy.ones((1, 2)) / 2], ["Cs", "Cl"])

        can be represented as follows:

        .. code-block:: none

            2 1 1
            Cs
            Cl
            ppp
            1 0
            0 1
            0 0
            0.5 0.5

        Parameters
        ----------
        cell : Cell or iterable(Cell)
            The cell or cells to save.
        text_file : str or io.TextIOBase
            The output file.
        """
        
        with CellCodecs._file(text_file, "w") as real_file:
            cells = (cell,) if isinstance(cell, Cell) else cell
            for cell in cells:
                # Write the heading line
                real_file.write("{} {}\n".format(cell.dimensions, " ".join(str(count) for count in cell.atom_counts)))

                # Write atom names
                for name in cell.names:
                    real_file.write("{}\n".format(name.encode("unicode_escape").decode()))

                # Write periodicity data
                real_file.write("{}\n".format("".join("p" if periodic else "n" for periodic in cell.periodic)))

                # Write vector data
                for vector_index in range(cell.dimensions):
                    real_file.write("{}\n".format(" ".join("{:.16e}".format(coordinate) \
                        for coordinate in cell.vector(vector_index))))

                # Write atom data
                for type_index in range(cell.atom_types):
                    for atom_index in range(cell.atom_count(type_index)):
                        real_file.write("{}\n".format(" ".join("{:.16e}".format(coordinate) \
                            for coordinate in cell.atom(type_index, atom_index))))

    @staticmethod
    def read_lammps(text_file, dimensions=3):
        """
        Reads cell data from a LAMMPS file.

        Parameters
        ----------
        text_file : str or io.TextIOBase
            The input file.
        dimensions : int
            A number of dimensions less than 3 which the cell should have.  Specify
            2 to force all z-components to be discarded when reading a 2-dimensional configuration.

        Raises
        ------
        ValueError
            Invalid data were encountered.
        
        Returns
        -------
        Cell
            The cell created from the file.
        """
        
        with CellCodecs._file(text_file, "r") as real_file:
            lines = ((line[0].strip(), line[1].strip()) if len(line) > 1 else (line[0].strip(), "")
                for line in (line.split("#", 1) for line in real_file) if line[0].strip())

            # Read file comment line
            next(lines)

            # Set default values for variables
            atom_count, atom_types, xlo, xhi, ylo, yhi, zlo, zhi, xy, xz, yz = \
                0, 0, 0, 0, 0, 0, CellCodecs.__LAMMPS_Z_MIN, CellCodecs.__LAMMPS_Z_MAX, 0, 0, 0

            # Read header information
            while True:
                next_line, next_comment = next(lines)
                if next_line.endswith("atoms"):
                    atom_count = int(next_line.split("atoms")[0].strip())
                elif next_line.endswith("atom types"):
                    atom_types = int(next_line.split("atom types")[0].strip())
                elif next_line.endswith("xlo xhi"):
                    xlo, xhi = (float(x.strip()) for x in next_line.split("xlo xhi")[0].strip().split())
                elif next_line.endswith("ylo yhi"):
                    ylo, yhi = (float(y.strip()) for y in next_line.split("ylo yhi")[0].strip().split())
                elif next_line.endswith("zlo zhi"):
                    zlo, zhi = (float(z.strip()) for z in next_line.split("zlo zhi")[0].strip().split())
                elif next_line.endswith("xy xz yz"):
                    xy, xz, yz = (float(w.strip()) for w in next_line.split("xy xz yz")[0].strip().split())
                else:
                    for skip_header in CellCodecs.__LAMMPS_HEADERS_SKIP:
                        if next_line.endswith(skip_header):
                            break # for
                    else:
                        break # while

            # We are here, read until Atoms header is reached
            atom_style = None
            if next_line != "Atoms":
                while True:
                    next_line, next_comment = next(lines)
                    if next_line == "Atoms":
                        atom_style = next_comment
                        break
            
            # Read atomic data
            atom_type, x, y, z = [], [], [], []
            for atom_index in range(atom_count):
                if atom_style == "charge":
                    ai, qi, xi, yi, zi = next(lines)[0].split()[1:6]
                else:
                    if atom_style and atom_style != "atomic":
                        warnings.warn("Possibly unknown atom_style may yield invalid data")
                    ai, xi, yi, zi = next(lines)[0].split()[1:5]
                atom_type.append(int(ai))
                x.append(float(xi))
                y.append(float(yi))
                z.append(float(zi))

            # Compile cell object
            return Cell(numpy.array([[xhi - xlo, 0, 0], [xy, yhi - ylo, 0], [xz, yz, zhi - zlo]])[:dimensions,
                :dimensions], [numpy.array([[x[atom_index] - xlo, y[atom_index] - ylo, z[atom_index] - zlo]
                    for atom_index in range(atom_count) if atom_type[atom_index] == type_index + 1])[:, :dimensions]
                    for type_index in range(atom_types)])

    @staticmethod
    def write_lammps(cell, text_file):
        """
        Writes cell data to a LAMMPS file.  Atom positions are changed in accordance
        with LAMMPS' normalization scheme for triclinic cells.  Periodicity data is not
        written to the file.
        
        Raises
        ------
        NotImplementedError
            The cell does not have 2 or 3 dimensions.

        Parameters
        ----------
        cell : Cell
            The cell to export.
        text_file : str or io.TextIOBase
            The output file.
        """

        # Normalize the cell
        if cell.dimensions not in {2, 3}:
            raise NotImplementedError("LAMMPS export supported in 2D and 3D only")
        cell = CellTools.normalize(cell)
        
        with CellCodecs._file(text_file, "w") as real_file:
            # Write header information
            real_file.write("LAMMPS\n\n{} atoms\n{} atom types\n\n".format( \
                sum(cell.atom_counts), cell.atom_types))

            # Write box data
            A, B, C = cell.vectors if cell.dimensions == 3 else (tuple(cell.vectors) + (numpy.zeros((2,)),))
            real_file.write("0.0 {!r} xlo xhi\n".format(A[0]))
            real_file.write("0.0 {!r} ylo yhi\n".format(B[1]))
            real_file.write("{!r} {!r} zlo zhi\n".format(*((0.0, C[2])
                if cell.dimensions == 3 else (CellCodecs.__LAMMPS_Z_MIN, CellCodecs.__LAMMPS_Z_MAX))))
            real_file.write("{!r} {!r} {!r} xy xz yz\n\nAtoms\n\n".format(B[0], C[0], C[1]))

            # Write actual atom positions
            atom_counter = 0
            for type_index in range(cell.atom_types):
                atom_list = cell.atoms(type_index)
                for atom_index in range(cell.atom_count(type_index)):
                    real_file.write("{} {} {}{}\n".format( \
                        atom_counter + 1, type_index + 1, " ".join(repr(float(coordinate)) \
                        for coordinate in cell.atom(type_index, atom_index)), \
                        " 0.0" if cell.dimensions == 2 else ""))
                    atom_counter += 1

    @staticmethod
    def read_xyz(text_file, sequence=False):
        """
        Reads cell data from an XYZ file.

        Parameters
        ----------
        text_file : str or io.TextIOBase
            The input file.
        sequence : bool
            Whether or not to read multiple cells.

        Raises
        ------
        ValueError
            Invalid data were encountered.

        Returns
        -------
        Cell or list(Cell)
            The cell or cells created from the file.
        """

        with CellCodecs._file(text_file, "r") as real_file:
            # Do not allow blanks to be present in the file
            lines = (line.strip() for line in real_file)
            
            cells = []
            while True:
                # Read the heading line
                try:
                    atom_count = int(next(lines))
                except StopIteration as stop_iteration:
                    if sequence:
                        # Was the end of the sequence
                        break
                    else:
                        # File was empty or ended early
                        raise stop_iteration
                if atom_count < 0:
                    raise ValueError("number of atoms must not be negative")

                # Read the comment line
                comment = next(lines)
                # Check for extended XYZ format
                if comment.startswith("Lattice=\""):
                    comment = comment.split("\"")[1]
                # See if lattice vectors are present
                matrix_guessed = False
                try:
                    vectors = [float(value.strip()) for value in comment.split()]
                    dimensions = round(math.sqrt(len(vectors)))
                    if dimensions * dimensions == len(vectors):
                        matrix = numpy.array(vectors).reshape((dimensions, dimensions))
                    else:
                        # Wasn't a square matrix; perhaps it is just the diagonals
                        dimensions = len(vectors)
                        matrix = numpy.diag(numpy.array(vectors))
                except Exception:
                    # Vectors are in an unknown or unreadable format: guess and proceed
                    matrix_guessed = True
                    dimensions = 3
                    matrix = numpy.diag(numpy.eye(dimensions))

                # Accumulate type names
                type_names = []
                atom_lists = []
                for index, line in zip(range(atom_count), lines):
                    # Get data from this line
                    items = line.strip()
                    name, coordinates = items[0], items[1:]

                    # Handle name
                    try:
                        # Atom type which has already been seen
                        atom_list = atom_lists[type_names.index(name)]
                    except ValueError:
                        # Atom type which has not yet been seen
                        type_names.append(name)
                        atom_list = []
                        atom_lists.append(atom_list)

                    # If this is the first line and the number of dimensions were guessed,
                    # this may need to be changed at this point (this probably will never actually happen)
                    if index == 0 and matrix_guessed and len(coordinates) > dimensions:
                        dimensions = len(coordinates)
                        matrix = numpy.diag(numpy.eye(dimensions))
                    
                    # We expect XYZ files to have no less than three atomic coordinates
                    coordinates = [float(value.strip()) for value in coordinates.split()]
                    if len(coordinates) < 3 or (len(coordinates) >= 3 and len(coordinates) != dimensions):
                        raise ValueError("improper number of coordinates encountered for atom")
                    atom_list.append(coordinates[:dimensions])

                cell = Cell(matrix, atom_lists, type_names, [True] * dimensions)

                if sequence:
                    # Keep going
                    cells.append(cell)
                else:
                    # Ensure that the end of the file has been reached
                    try:
                        next(lines)
                        raise ValueError("unexpected additional data encountered")
                    except StopIteration:
                        return cell
            return cells

    @staticmethod
    def write_xyz(cell, text_file):
        """
        Writes cell data to an XYZ file.  Vector information is placed directly into the
        comment line of the file.

        Parameters
        ----------
        cell : Cell or iterable(Cell)
            The cell or cells to export.
        text_file : str or io.TextIOBase
            The output file.
        """
        
        with CellCodecs._file(text_file, "w") as real_file:
            cells = (cell,) if isinstance(cell, Cell) else cell
            for cell in cells:
                # Write header information
                real_file.write("{}\n".format(sum(cell.atom_counts)))
                real_file.write("{}\n".format(" ".join(repr(float(coordinate)) \
                    for coordinate in cell.vectors.flatten())))

                # Write atom data
                for type_index in range(cell.atom_types):
                    name = cell.name(type_index)
                    for atom_index in range(cell.atom_count(type_index)):
                        real_file.write("{} {}{}\n".format(name, " ".join(repr(float(coordinate)) \
                            for coordinate in cell.atom(type_index, atom_index)), \
                            " 0.0" if cell.dimensions == 2 else ""))

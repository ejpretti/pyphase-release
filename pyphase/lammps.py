# pyphase.lammps: Evan Pretti
"""
Contains routines to interface with LAMMPS by setting up run information and
invoking LAMMPS.  Note that this interface does not currently permit dynamic
extraction of data from LAMMPS in the middle of a run; rather, commands are
sequenced in a series of calls, which are all dispatched to LAMMPS at once.
Run data are stored in temporary directories which are automatically created,
and tools are provided to handle moving of large data files around without
reading and writing them into and out from memory.
"""

import contextlib
import io
import itertools
import numpy
import os
import random
import re
import shlex
import shutil
import subprocess
import sys
import tempfile
import warnings

from . import crystal

#: LAMMPS path environment variable name.
_PYPHASELMPPATH = "PYPHASELMPPATH"
#: LAMMPS name environment variable name.
_PYPHASELMPNAME = "PYPHASELMPNAME"
#: Temporary directory environment variable name.
_PYPHASETMPDIR = "PYPHASETMPDIR"

#: Current override path for LAMMPS.
_LAMMPS_OVERRIDE = None

def set_lammps_global(absolute_path=None):
    """
    Registers a global absolute path to LAMMPS used by all invocations.
    This overrides all other search logic.

    Parameters
    ----------
    absolute_path : str
        An absolute path to LAMMPS.  If this parameter is not specified,
        and the routine is instead called without arguments, any currently
        set override will be cleared and the normal search behavior will
        take place.
    """
    global _LAMMPS_OVERRIDE
    _LAMMPS_OVERRIDE = absolute_path

#: Default names to look for LAMMPS as on the system path.
_LAMMPS_NAMES = ["lmp_pyphase", "lmp_mpi", "lmp_serial", "lmp", "lammps"]

#: Printed at the beginning of a LAMMPS run.
_LAMMPS_BEGIN = ("=" * 31) + " LAMMPS RUN BEGIN " + ("=" * 31)
#: Printed at the end of a LAMMPS run.
_LAMMPS_END = ("=" * 32) + " LAMMPS RUN END " + ("=" * 32)

#: The minimum allowable random seed.
_RAN_MARS_MIN = 1
#: The maximum allowable random seed.
_RAN_MARS_MAX = 900000000

def random_seed():
    """
    Generates a random seed for the Marsaglia random number generator used by
    LAMMPS (`RanMars`).

    Returns
    -------
    int
        A random seed suitable for LAMMPS commands such as `fix langevin` or
        `velocity create` which utilize random numbers.
    """

    return random.randint(_RAN_MARS_MIN, _RAN_MARS_MAX)

def find_lammps(search_names=None):
    """
    Returns an absolute path to a LAMMPS executable.  If :py:func:`set_lammps_global`
    has been used to register an override location for LAMMPS, this will be returned
    directly without attempting any other search.  If the environment variable
    `PYPHASELMPPATH` is defined, it will be returned directly.  Otherwise, LAMMPS
    will be searched for on the system path under the names in `search_names`.

    Parameters
    ----------
    search_names : list(str)
        List of names to look for LAMMPS as.  Defaults to the names in
        :py:data:`_LAMMPS_NAMES`, prepended with the path in the environment
        variable `PYPHASELMPNAME` if it is defined.

    Returns
    -------
    str
        A full absolute path to a LAMMPS executable.
    """
    
    # Handle override unconditionally
    if _LAMMPS_OVERRIDE is not None:
        return _LAMMPS_OVERRIDE

    # Handle PYPHASELMPPATH
    lammps_path = os.environ.get(_PYPHASELMPPATH, None)
    if lammps_path is not None:
        return lammps_path
    
    # Handle PYPHASELMPNAME
    lammps_names = list(_LAMMPS_NAMES if search_names is None else search_names)
    lammps_name = os.environ.get(_PYPHASELMPNAME, None)
    if lammps_name is not None:
        lammps_names.insert(0, lammps_name)

    # Look for LAMMPS
    for lammps_name in lammps_names:
        lammps_path = shutil.which(lammps_name)
        if lammps_path is not None:
            return os.path.realpath(lammps_path)

    # LAMMPS was not found
    raise LAMMPSNotFoundError

class LAMMPSWarning(UserWarning):
    """
    Indicates a warning condition issued by LAMMPS that did not halt the run.
    """

class LAMMPSError(RuntimeError):
    """
    Indicates an error condition issued by LAMMPS that halted the run.
    """

class LAMMPSNotFoundError(LAMMPSError):
    """
    Indicates that LAMMPS could not be found.
    """

@contextlib.contextmanager
def PersistentStorage():
    """
    Provides persistent storage for temporary files.  Normally this is only used
    internally by the :py:func:`run_lammps` routine, but it may be desirable in some cases
    to use an explicitly passed PersistentStorage to pass in and out data in the
    filesystem without reading and writing large chunks in memory.  A directory
    will be created in `PYPHASETMPDIR` if specified as an environment variable,
    or `TMPDIR`, `TEMP`, or `TMP` if specified as environment variables, or a
    platform-dependent directory otherwise.  It may be desirable to use an explicit
    `PYPHASETMPDIR` if the default temporary directory is too small to hold the
    files expected to be created by LAMMPS and setting `TMPDIR` would interfere
    with the operation of another library (such as an MPI implementation).

    Returns
    -------
    callable
        When called with a file name, gives a path to a temporary file in the
        created directory.  If called with no arguments, gives the temporary
        directory path itself.  Should be used within a `with` block to clean
        up the temporary directory after use.
    """
    
    scratch_path = os.environ.get(_PYPHASETMPDIR, None)
    with tempfile.TemporaryDirectory(prefix="pyphase-", dir=scratch_path) as temp_path:
        def persistent_storage(name=None, temp_path=temp_path):
            if name is None: return temp_path
            return os.path.join(temp_path, name)
        yield persistent_storage

def run_lammps(input_script, output_names, *, auxiliary_data=None, move_in=None,
        move_out=None, search_names=None, quiet=False, debug_repl=False, atom_style=None,
        extra_lammps_args=()):
    """
    Performs a LAMMPS run.  The run will be conducted in a temporary directory
    and all output data not explicitly requested to be returned will be cleaned
    up after the run completes.

    Parameters
    ----------
    input_script : bytes or LAMMPS
        LAMMPS input script.
    output_names : list(str)
        List of names of output files to find and return.
    auxiliary_data : dict(str, bytes)
        Auxiliary input files to provide to LAMMPS.  Each key represents a filename
        which will be created in the LAMMPS directory.  Each value should be the
        binary data within the file.  File names starting with `__pyphase_internal`
        are reserved.
    move_in : list(tuple(str))
        List of files to move: from absolute paths to names which should be
        provided to LAMMPS.  These files will be moved, not copied, and when the
        run completes, they will be permanently deleted if not retrieved.  This
        operation is performed after `auxiliary_data` has been processed, and data
        passed in may be overwritten by files moved in as a result.
    move_out : list(tuple(str))
        List of files to move: from names in the LAMMPS working directory to
        absolute paths.  This operation may overwrite any existing files with
        names the same as specified.
    search_names : list(str)
        Search names for the `find_lammps` routine.
    quiet : bool
        Whether or not to suppress output.
    debug_repl : bool
        Whether or not to drop into an interactive session after the run ends
        and before the temporary files are deleted.
    atom_style : str or None
        Whether or not to specify a custom `atom_style` for compatibility with some
        `pair_style` options.  Currently a special case is present: if `charge` is used,
        charges of 0 will be added to the data file: full support for charged atoms is not
        present at this time.
    extra_lammps_args : tuple(str)
        Additional command-line arguments for LAMMPS (can be useful to, for example,
        enable OpenMP optimized `pair_style` variants).

    Raises
    ------
    LAMMPSError
        LAMMPS encountered an error and could not continue.

    Returns
    -------
    bytes, dict(str, bytes)
        The LAMMPS log file and any requested auxiliary output files.
    """

    def log(message):
        if not quiet:
            print(message, file=sys.stderr)
    
    log(_LAMMPS_BEGIN)

    try:
        # Seek for LAMMPS
        lammps_path = find_lammps(search_names)
        log("Using LAMMPS at {}".format(shlex.quote(lammps_path)))

        # Prepare a temporary environment
        with PersistentStorage() as temp_dir:
            log("Working in directory {}".format(shlex.quote(temp_dir())))

            # Make sure input file name is not used for any auxiliary filename
            input_name = "input"
            while auxiliary_data is not None and input_name in auxiliary_data:
                input_name += "_"

            # Do the same with log file name
            log_name = "log"
            while auxiliary_data is not None and log_name in auxiliary_data:
                log_name += "_"

            # Try to keep working if errors are encountered; report at the end
            error_lines = []

            # Write input data
            with open(temp_dir(input_name), "wb") as input_file:
                try:
                    if isinstance(input_script, LAMMPS):
                        # Merge in cell and pair data with auxiliary data
                        # It is possible for the dictionary to have not yet been created
                        if auxiliary_data is None:
                            auxiliary_data = {}
                        
                        for cell_file, cell_data in input_script._get_cell_data():
                            pass
                            if cell_file in auxiliary_data:
                                # If a file was already found, don't overwrite it
                                error_lines.append("Reserved name {!r} encountered in auxiliary data".format(cell_file))
                            else:
                                # A modification may need to be made here to the data file for some atom_styles
                                if atom_style == "atomic":
                                    # This is the default case
                                    pass
                                elif atom_style == "charge":
                                    # Need to modify the data file
                                    modify_io = io.StringIO()
                                    _preprocess_data(io.StringIO(cell_data.decode()), modify_io, add_charges=True)
                                    cell_data = modify_io.getvalue().encode()
                                elif atom_style is not None:
                                    # This might or might not work depending on the atom_style: warn the user
                                    # in any case to be more helpful when something goes wrong
                                    warnings.warn("atom_style unknown and may cause invalid data input", LAMMPSWarning)

                                # Add it in as a normal auxiliary file
                                auxiliary_data[cell_file] = cell_data

                        for pair_file, pair_data in input_script._get_pair_data():
                            pass
                            if pair_file in auxiliary_data:
                                # If a file was already found, don't overwrite it
                                error_lines.append("Reserved name {!r} encountered in auxiliary data".format(pair_file))
                            else:
                                # Add it in as a normal auxiliary file
                                auxiliary_data[pair_file] = pair_data

                        # Compile the script
                        input_script = input_script._compile()
                    
                    # Insert custom atom_style at top of script here if desired
                    if atom_style is not None:
                        atom_style_prefix = LAMMPS()
                        atom_style_prefix.atom_style(atom_style)
                        input_script = atom_style_prefix._compile() + input_script

                    input_file.write(input_script)
                except Exception:
                    error_lines.append("Failed to write input data {!r}".format(input_name))

            # Write auxiliary data
            if auxiliary_data is not None:
                for auxiliary_name, auxiliary_content in auxiliary_data.items():
                    try:
                        with open(temp_dir(auxiliary_name), "wb") as auxiliary_file:
                            auxiliary_file.write(auxiliary_content)
                    except Exception:
                        error_lines.append("Failed to write auxiliary data {!r}".format(auxiliary_name))

            # Process files to be moved in directly
            if move_in is not None:
                for source_path, destination_name in move_in:
                    try:
                        shutil.move(source_path, temp_dir(destination_name))
                    except Exception:
                        error_lines.append("Failed to move in file {!r}".format(destination_name))

            # Perform the run
            log("Running LAMMPS")
            try:
                subprocess.run([lammps_path,
                    "-in", temp_dir(input_name),
                    "-log", temp_dir(log_name),
                    "-echo", "both",
                    *extra_lammps_args],
                    check=True,
                    cwd=temp_dir(),
                    # When quiet mode is set, all output appears in log alone;
                    # otherwise, all output goes to standard error
                    stdout=subprocess.DEVNULL if quiet else sys.stderr.buffer,
                    stderr=subprocess.DEVNULL if quiet else None)
            except Exception:
                # Wait to check log files before giving up immediately
                error_lines.append("LAMMPS indicated failure upon exiting")

            # Process files to be moved out directly
            if move_out is not None:
                for source_name, destination_path in move_out:
                    try:
                        shutil.move(temp_dir(source_name), destination_path)
                    except Exception:
                        error_lines.append("Failed to move out file {!r}".format(source_name))

            # Read the log file
            try:
                with open(temp_dir(log_name), "rb") as log_file:
                    log_data = log_file.read()
            except Exception:
                # Something has gone very wrong if there is no log file
                error_lines.append("Failed to read log file")
                log_data = b""

            # Read the output file (do not give up if output files are unavailable;
            # wait to see if the log file has an error)
            output_data = {}
            for output_name in output_names:
                try:
                    with open(temp_dir(output_name), "rb") as output_file:
                        output_data[output_name] = output_file.read()
                except Exception:
                    error_lines.append("Failed to read output data {!r}".format(output_name))

            # Look for warnings or errors in the log file
            log_lines = [line.strip() for line in log_data.decode().split("\n")]
            for log_line in log_lines:
                if log_line.startswith("WARNING: "):
                    # Raise a LAMMPSWarning immediately (this will not interrupt control flow)
                    warnings.warn(log_line[len("WARNING: "):], LAMMPSWarning)
                if log_line.startswith("ERROR: "):
                    # Accumulate all errors and raise a LAMMPSError after we are done
                    # Normally LAMMPS should not issue multiple errors but in case this
                    # happens somehow, they will all be collected
                    error_lines.append(log_line[len("ERROR: "):])

            # Stop now if desired
            if debug_repl:
                print("Entering debug mode; LAMMPS data is in {}".format(temp_dir()),
                    file=sys.stderr)
                print("Notice: Exiting the interactive session will resume the run",
                    "and then delete the temporary directory.  Send Ctrl+D to exit.",
                    sep="\n", file=sys.stderr)
                if error_lines:
                    warnings.warn("Error(s) detected; delayed due to debug mode{}{}".format(
                        ": " if error_lines else "", "; ".join(error_lines)), LAMMPSWarning)
                try:
                    # See if the user has IPython installed
                    import IPython
                    interact = IPython.embed
                except ImportError:
                    # Fall back on the normal Python interpreter
                    import code
                    local_dict = locals()
                    interact = lambda: code.interact(local={**globals(), **local_dict})
                interact()
            
            # Even if no errors were found, there may be other error conditions
            # that could justify raising a LAMMPSError, so the message may be empty
            if error_lines:
                raise LAMMPSError("; ".join(error_lines))

        return log_data, output_data

    finally:
        log(_LAMMPS_END)

class LAMMPS:
    """
    A helper class which allows LAMMPS commands to be specified using a Python
    interface.  For example, the LAMMPS input commands:

    .. code-block:: none

        fix all box/relax iso 0
        neigh_modify delay 0 every 1

    can be represented by the following Python input::

        run = LAMMPS()
        run.fix("all", "box/relax", "iso", 0)
        run.neigh_modify("delay", 0, "every", 1)

    Arguments with spaces will automatically be quoted.
    """

    #: Internal filename used to store cell data.
    __CELL_FILE = "__pyphase_internal_Cell.dat"

    def __init__(self):
        self.__commands = []
        self.__cell_data = {}
        self.__pair_data = {}

    def cell(self, cell):
        """
        Initializes a simulation with a predefined configuration.

        Parameters
        ----------
        cell : crystal.Cell
            The cell defining the simulation conditions.
        """

        # Sequence commands to read the configuration
        self.dimension(cell.dimensions)
        # For some unusual reason, the default behavior for LAMMPS on a highly
        # tilted cell is to stop immediately instead of only warning
        self.box("tilt", "large")
        # Specify periodic boundary behavior
        periodic_lookup = {False: "f", True: "p"}
        if cell.dimensions == 2:
            # In two dimensions, the z-direction should always be marked periodic according to LAMMPS
            # documentation; the dimension 2 command prevents actual periodic interactions
            self.boundary(*(periodic_lookup[periodic] for periodic in cell.periodic), periodic_lookup[True])
        elif cell.dimensions == 3:
            self.boundary(*(periodic_lookup[periodic] for periodic in cell.periodic))
        else:
            raise NotImplementedError("Number of dimensions not understood")
        # Read the data after PBC specification is set
        self.read_data(LAMMPS.__CELL_FILE)
        # crystal.Cell currently specifies no mass information
        for type_index in range(cell.atom_types):
            self.mass(type_index + 1, 1)

        # Generate cell data
        cell_data = io.StringIO()
        # Automatically reduce the cell (this should not affect any properties of the system)
        # Note that an expensive call to condense() is avoided here by the default reduce() options
        crystal.CellCodecs.write_lammps(crystal.CellTools.reduce(cell, shift=False), cell_data)
        self._register_cell_data(LAMMPS.__CELL_FILE, cell_data.getvalue().encode())

    def _compile(self):
        """
        Compiles all commands which have been sequenced into a LAMMPS input script.

        Returns
        -------
        str
            Text representing a LAMMPS input script.
        """

        return "{}\n".format("\n".join((" ".join(_quote(str(argument)) for argument in command))
            for command in self.__commands)).encode()

    def _register_cell_data(self, name, data):
        """
        Registers data to be provided to LAMMPS.  This is used by :py:meth:`cell`
        in order to allow for easy specification of cells from within Python.

        Parameters
        ----------
        name : str
            Filename.  Note that this file will be merged into `auxiliary_data` and any
            conflicts will result in an error; the name should be chosen carefully.
        data : bytes
            The data to make available.
        """

        self.__cell_data[name] = data

    def _get_cell_data(self):
        """
        Returns registered cell data.

        Returns
        -------
        dict(str, bytes)
            Cell files and their data.
        """

        return self.__cell_data.items()

    def _register_pair_data(self, name, data):
        """
        Registers data to be provided to LAMMPS.  This is used by :py:class:`PythonPair`
        in order to allow for transparent specification of potentials from within Python.

        Parameters
        ----------
        name : str
            Filename.  Note that this file will be merged into `auxiliary_data` and any
            conflicts will result in an error; the name should be chosen carefully.
        data : bytes
            The data to make available.
        """

        self.__pair_data[name] = data

    def _get_pair_data(self):
        """
        Returns registered pair potential data.

        Returns
        -------
        dict(str, bytes)
            Pair files and their data.
        """

        return self.__pair_data.items()

    def __getattr__(self, name):
        def _append(*args):
            self.__commands.append([name] + list(args))
        return _append

def log_seek(log, phrase, skip, count=None):
    """
    Seeks in a LAMMPS log file to find the start of a set of thermodynamic data.

    Parameters
    ----------
    log : bytes
        A log file from :py:func:`run_lammps` to seek through.
    phrase : str
        The header line to search for in the log file indicating the start of data.
    skip : bool 
        Whether or not to skip over the first line of the data.
    count : int
        If specified, yields a finite number of items.
    
    Returns
    -------
    generator(tuple(float))
        A generator yielding lines of thermodynamic data.
    """
    
    # Warnings should have already been detected and their presence in the thermodynamic
    # data is thus superfluous
    log_lines = (line for line in log.decode().split("\n") if not line.startswith("WARNING"))
    
    # Seek through until the start of the desired section is found
    lower_phrase = phrase.lower()
    for line in log_lines:
        if line.strip().lower() == lower_phrase:
            break

    # Skip the first line if desired (this is often necessary if averages are being accumulated
    # since the first entry will be at timestep 0 before anything has been calculated
    if skip:
        next(log_lines)
    
    # Parse successive lines
    for line in log_lines if count is None else itertools.islice(log_lines, count):
        yield tuple(float(item.strip()) for item in line.split())

def _quote(argument):
    """
    Quotes a LAMMPS argument based on quoting rules (there are no
    escape sequences; this relies on using ', ", or \"\"\".

    Parameters
    ----------
    argument : str
        The LAMMPS argument to quote.

    Returns
    -------
    str
        An argument which can be safely passed as a LAMMPS command.

    Raises
    ------
    ValueError
        It was not possible to quote the argument.  Quotes should be
        removed if possible.
    """
    
    # Newlines must be handled using triple quoting
    if "\n" in argument:
        if "\"\"\"" not in argument and not argument.endswith("\""):
            return "\"\"\"{}\"\"\"".format(argument)
        raise ValueError("LAMMPS argument could not be formatted")

    # If quoting is unnecessary, return directly
    if " " not in argument and "\t" not in argument and "'" not in argument and "\"" not in argument:
        return argument

    # Check each option for quoting
    if "\"" not in argument:
        return "\"{}\"".format(argument)
    if "'" not in argument:
        return "'{}'".format(argument)
    if "\"\"\"" not in argument and not argument.endswith("\""):
        return "\"\"\"{}\"\"\"".format(argument)
    raise ValueError("LAMMPS argument could not be formatted")

# Sets masses and charges in a LAMMPS data file.
def _preprocess_data(input_file, output_file, *, add_masses=False, add_charges=False):
    """
    Edits a LAMMPS data file to add default information about masses and charges.

    Parameters
    ----------
    input_file : file
        The file to read from.
    output_file : file
        The file to write to.
    add_masses : bool
        Whether or not to add mass information.  The masses added will be 1.
    add_charges: bool
        Whether or not to add charge information.  The charges added will be 0.
    """

    input_lines = [line.rstrip("\n") for line in input_file]

    def find_header_value(pattern):
        line_pattern = re.compile("\\s*(\\d+)\\s+{}\\s*(?:#.*)?".format(pattern))
        for line in input_lines:
            match = line_pattern.fullmatch(line)
            if match is not None:
                return int(match.group(1))
        else:
            raise ValueError("header line for pattern {!r} not found in data file".format(pattern))

    # This is done somewhat naively; it is assumed no Masses section is already present.
    if add_masses:
        atom_types = find_header_value("atom types")
        input_lines.extend(("", "Masses", ""))
        # Assume a mass of 1 for all atom types (otherwise this should be
        # in the data file and add_masses should not have been set to True).
        for index in range(atom_types):
            input_lines.append("{} 1".format(index + 1))

    # Similarly, assume no charges are present and set all to zero.
    if add_charges:
        atoms = find_header_value("atoms")
        for section_index, line in enumerate(input_lines):
            if line.split("#")[0].strip() == "Atoms":
                break
        else:
            raise ValueError("Atoms section not found in data file")
        # Jump to first line of data.
        section_index += 2
        for index in range(section_index, section_index + atoms):
            # Assume we are switching from atom_style atomic to atom_style charge.
            components = input_lines[index].split()
            components[2:2] = "0"
            input_lines[index] = " ".join(components)

    with contextlib.redirect_stdout(output_file):
        for input_line in input_lines:
            print(input_line)

class Pair:
    """
    Base class for pairwise interaction potentials.  To insert pair commands into
    a LAMMPS run, use::
        
        run = LAMMPS()
        ...
        my_pair = ...
        my_pair(run)
    """

    def __init__(self, types):
        self.__types = types

    def __call__(self, run):
        raise NotImplementedError("instantiate an instance of a subclass instead")

    def evaluate(self, i, j, r_min, r_max, n_step, **lammps_kwargs):
        """
        Evaluates the potential energy and force over a range of distances.
        Note that this requires a call to LAMMPS, so it must be available
        in order to perform the evaluation.

        For example::

            >>> LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,)).evaluate(1, 1, 1, 4, 7)
            =============================== LAMMPS RUN BEGIN ===============================
            Using LAMMPS at ...
            Working in directory ...
            Running LAMMPS
            ...
            ================================ LAMMPS RUN END ================================
            (array([1. , 1.5, 2. , 2.5, 3. , 3.5, 4. ]),
             array([ 0.        , -0.32033659, -0.06152344, -0.01631689,  0.        ,
                     0.        ,  0.        ]),
             array([24.        , -1.15802883, -0.18164062, -0.03899948,  0.        ,
                     0.        ,  0.        ]))

        Parameters
        ----------
        i : int
            An atom type for the pairwise interaction.
        j : int
            An atom type for the pairwise interaction.
        r_min : float
            The lower bound of the range.
        r_max : float
            The upper bound of the range.
        n_step : int
            The number of elements in the range.
        lammps_kwargs : dict
            Keyword arguments to provide to :py:func:`run_lammps`.

        Returns
        -------
        numpy.ndarray, numpy.ndarray, numpy.ndarray
            The evaluated distances, potential, and force.
        """

        run = LAMMPS()

        # Create a dummy region to set the number of atom types
        run.region("label", "block", 0, 1, 0, 1, 0, 1)
        run.create_box(self.__types, "label")

        # Register the potential data and output a table
        self(run)
        run.pair_write(i, j, n_step, "r", r_min, r_max, "table", "keyword")
        table = numpy.loadtxt(io.BytesIO(run_lammps(run, ["table"], **lammps_kwargs)[1]["table"]), skiprows=4)
        return tuple(table.T[1:, :])
    
class LAMMPSPair(Pair):
    """
    Represents a pairwise interaction potential which is built in to LAMMPS.
    Such a potential cannot be evaluated within PyPhase without a call to
    LAMMPS, and the potential desired must be compiled in the copy of LAMMPS
    which is invoked.  The potential information here will be translated
    directly into `pair_style`, `pair_coeff`, and `pair_modify` commands.

    For example, a LAMMPS specification of:

    .. code-block:: none

        pair_style my_pair 12
        pair_coeff 1 1 3 4 5
        pair_coeff 2 2 6 7 8
        pair_coeff 1 2 9 10 11
        pair_modify shift yes

    can be represented by::

        LAMMPSPair("my_pair", 2, {
            (1, 1): (3, 4, 5),
            (2, 2): (6, 7, 8),
            (1, 2): (9, 10, 11)
        }, style_args=(12,),
        modifications=("shift", "yes"))

    Parameters
    ----------
    name : str
        The pair potential style name.
    types : int
        The number of atom types.
    pairs : dict((int, int), list)
        Pairwise potential coefficients.
    style_args : list
        Arguments to provide to `pair_style`.
    modifications : list
        Arguments to provide to `pair_modify`.
    """

    def __init__(self, name, types, pairs, style_args=None, modifications=None):
        super().__init__(types)
        self.__name = name
        self.__pairs = pairs
        self.__style_args = style_args
        self.__modifications = modifications

    def __call__(self, run):
        run.pair_style(self.__name, *self.__style_args)
        for (i, j), args in self.__pairs.items():
            run.pair_coeff(i, j, *args)
        if self.__modifications:
            run.pair_modify(*self.__modifications)

class PythonPair(Pair):
    """
    Represents a pairwise interaction potential which is specified in Python.
    Although this can be done by using `pair_style table` in a :py:class:`LAMMPSPair`,
    this requires manual construction and provision of the pair table data files
    to LAMMPS.  This class, on the other hand, automatically performs the tabulation.

    For example, a Lennard-Jones interaction could be defined as follows using
    the built-in LAMMPS pair style::

        >>> lmp_pair = LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))

    or using Python::

        >>> def lj(r):
        ...    potential = 4 * (r ** -12 - r ** -6)
        ...    force = 24 * (2 * r ** -13 - r ** -7)
        ...    return potential, force
        ... py_pair = PythonPair(1, {(1, 1): lj}, 0.5, 3, 4096)
    
    The pair functions are evaluated once when the potential is created, and
    the tables are stored internally for use in LAMMPS runs.
    Note that evaluation of the Python potential will involve linear interpolation
    in :math:`r^2`; make sure that `n_tab` is high enough to achieve the
    desired accuracy::

        >>> r_lmp, e_lmp, f_lmp = lmp_pair.evaluate(1, 1, 1, 2, 100)
        >>> r_py, e_py, f_py = py_pair.evaluate(1, 1, 1, 2, 100)
        >>> numpy.max(numpy.abs(r_lmp - r_py))
        0.0
        >>> numpy.max(numpy.abs(e_lmp - e_py))
        5.686377964500555e-05
        >>> numpy.max(numpy.abs(f_lmp - f_py))
        0.0010478899093016025

    The LAMMPS (left) and Python (right) pairs are plotted below:

    .. plot::

        from pyphase import *
        def lj(r):
            potential = 4 * (r ** -12 - r ** -6)
            force = 24 * (2 * r ** -13 - r ** -7)
            return potential, force
        lmp_pair = lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
        py_pair = lammps.PythonPair(1, {(1, 1): lj}, 0.5, 3, 4096)
        visualization._pairs(*(dict(pairs=[(pair, 1, 1)], r_min=0.8, r_max=2, y_min=-1.2, y_max=0.8) for pair in (lmp_pair, py_pair)))

    Parameters
    ----------
    types : int
        The number of atom types.
    pairs : dict((int, int), callable)
        Pairwise potential functions.  Each callable should accept a floating-
        point separation distance and yield the potential and force.  It is the
        responsibility of the caller to ensure that the potential and force are
        consistent with each other.
    r_int : float
        Inner separation distance cutoff for tabulation.
    r_cut : float
        Outer separation distance cutoff for tabulation.
    n_tab : int
        Number of table points to use.
    """
    
    #: Internal filename used to store the pair data.
    __PYTHON_PAIR_FILE = "__pyphase_internal_PythonPair.tab"
    #: Internal lookup key for pairwise table.
    __PYTHON_PAIR_KEY = "TABLE_{}_{}"

    def __init__(self, types, pairs, r_int, r_cut, n_tab):
        super().__init__(types)
        self.__pairs = pairs
        self.__r_int = r_int
        self.__r_cut = r_cut
        self.__n_tab = n_tab

        # Perform the tabulation
        rsq_int = self.__r_int * self.__r_int
        rsq_cut = self.__r_cut * self.__r_cut
        r_range = numpy.sqrt(numpy.linspace(rsq_int, rsq_cut, self.__n_tab))

        # Prepare the table file
        table = io.StringIO()
        for (i, j), potential in self.__pairs.items():
            print("#\n", file=table)
            print(PythonPair.__PYTHON_PAIR_KEY.format(i, j), file=table)
            print("N {} RSQ {} {}\n".format(self.__n_tab, self.__r_int, self.__r_cut), file=table)
            for index, r in enumerate(r_range):
                print("{} {} {} {}".format(index + 1, r, *potential(r)), file=table)
        self.__table = table.getvalue().encode()

    def __call__(self, run):
        run.pair_style("table", "linear", self.__n_tab)
        for i, j in self.__pairs:
            run.pair_coeff(i, j, PythonPair.__PYTHON_PAIR_FILE, PythonPair.__PYTHON_PAIR_KEY.format(i, j))
        run._register_pair_data(PythonPair.__PYTHON_PAIR_FILE, self.__table)

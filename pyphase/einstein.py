# pyphase.einstein: Evan Pretti
"""
Performs calculations to determine the free energies of solid phases.
This module invokes LAMMPS to perform calculations for periodic,
non-periodic, and semi-periodic geometries.
"""

import enum
import io
import multiprocessing
import numpy
import scipy.linalg
import scipy.special
import scipy.stats
import sys

from . import crystal
from . import lammps
from . import relax

#: Printed to mark the beginning of a calculation summary.
_ECHO_BEGIN = ("=" * 26) + " CALCULATION SUMMARY BEGIN " + ("=" * 27)
#: Printed to mark the end of a calculation summary.
_ECHO_END = ("=" * 27) + " CALCULATION SUMMARY END " + ("=" * 28)

class ReferenceMode(enum.Enum):
    """
    Used to specify the convention used to calculate the absolute magnitude
    of the free energy.
    """
    
    #: Assumes that the Planck constant :math:`h` is equal to 1 when performing
    #: calculations.  This will generally yield the least surprising
    #: results, and free energies should be properly comparable across
    #: temperatures and densities.
    PLANCK_UNITY = enum.auto()

    #: Assumes that the thermal de Broglie wavelength :math:`\Lambda` is
    #: equal to 1 when performing calculations.  Note that as :math:`\Lambda`
    #: is a function of temperature, this may result in unusual behavior
    #: when comparing calculated values at different temperatures.
    WAVELENGTH_UNITY = enum.auto()

    #: Causes the excess Helmholtz free energy of a solid phase relative
    #: to an ideal gas at the same temperature and density to be returned.
    #: Be careful when using this option with non-periodic and semi-periodic
    #: geometries; the density of the containing cell will be used.
    EXCESS = enum.auto()

class OptimizationDefaults:
    """
    Default optimization constants for selecting simulation parameters.
    """

    #: The minimum number of timesteps to use for the fastest period of
    #: oscillation in the system.  If this parameter is too low, the Verlet
    #: integrator will fail and LAMMPS may crash.  If this parameter is too high,
    #: the calculation will proceed very slowly.  Take note that for certain
    #: values of this parameter, the simulations will appear to run properly but
    #: the numerical value returned from the calculation will be incorrect.  For
    #: example, at 10 timesteps per period, the systematic error in
    #: :math:`A_\mathrm{LJ}/Nk_BT` is close to 0.1.
    TIMESTEPS_PER_PERIOD = 40
    
    #: The number of (fastest) periods to set the Langevin thermostat relaxation
    #: time to.  From a thermodynamic standpoint, this should not affect the
    #: results, but if this value is changed significantly (by perhaps an order
    #: of magnitude), sampling quality may be affected.  The default value performs
    #: reasonably well in reproducing the results of
    #: `Aragones et al., J. Chem. Phys. 2012, 137, 146101 <https://dx.doi.org/10.1063/1.4758700>`_.
    THERMOSTAT_PERIODS = 10
    
    #: Initial guess for the time to equilibrate based on the length of the
    #: fastest period.  See :py:data:`PROD_SHORT_PERIODS` for more details.
    EQUIL_SHORT_PERIODS = 100

    #: Initial guess for the time to run based on the length of the fastest
    #: period.  This parameter only comes into play when the MSD and effective
    #: spring constant in the real crystal are estimated.  This is used to
    #: determine the slowest, longest period which will appear in the crystal
    #: during real sampling.  This will ultimately determine the production run
    #: length.  This value also acts as a minimum production run length in the
    #: case that it already satisfies the desired sampling requirements in the
    #: crystal.  This and :py:data:`EQUIL_SHORT_PERIODS` should be reasonably
    #: larger than :py:data:`THERMOSTAT_PERIODS`.
    PROD_SHORT_PERIODS = 100

    #: Time to equilibrate during calculation based on the length of the slowest
    #: period.  Assuming that the slowest period is significantly longer than the
    #: fastest, it should be suitable for :py:data:`EQUIL_LONG_PERIODS` to be
    #: less than :py:data:`EQUIL_SHORT_PERIODS`.
    EQUIL_LONG_PERIODS = 10

    #: Time to run during calculation based on the length of the slowest period.
    #: Increase this value to lower the uncertainty in the final answer.
    PROD_LONG_PERIODS = 100

    #: Offset, in :math:`Nk_BT` units, for choosing the optimal maximum spring
    #: constant.  The default value is based on
    #: `Aragones et al., J. Chem. Phys. 2012, 137, 146101 <https://dx.doi.org/10.1063/1.4758700>`_.
    #: If this value is made slightly larger, a larger timestep can be used, which
    #: can improve sampling at low values of the spring constant during the integration.
    #: However, if this value is made too large, the free energy perturbation step
    #: of the calculation may yield inaccurate results.
    SPRING_OFFSET = 0.02

    #: Default number of sample points to use for Gaussian quadrature.  Since
    #: the integration shift constant should be set automatically to try to maintain
    #: linear behavior in the integrand, this should not strongly affect the results,
    #: but it should not be set too low.  It is recommended to inspect the output of
    #: the quadrature routine for a few representative cases manually to ensure that
    #: the selected number of points is sufficient and that the integrand does not
    #: exhibit a singularity.
    QUADRATURE_POINTS = 16

    #: Maximum number of iterations to attempt to automatically determine the optimal
    #: spring constant and simulation time.  An error will be raised if this limit
    #: is exceeded.  Due to potential issues in the automatic determination step, it
    #: is recommended that, for a given structure, this step be performed once and
    #: its results manually inspected, before choosing a set of parameters to perform
    #: runs over a range of conditions.
    MAX_ITERATIONS = 16

    #: Initial guess to use for the maximum spring constant.  The value used will be
    #: at least as large as this.
    INITIAL_CONSTANT = 1000

    #: Spring constant to use when estimating the mean square displacement in the
    #: real crystal.  Ideally this should be zero.  However, if the free energy of
    #: a metastable configuration is being determined, setting this to zero may result
    #: in the mean square displacement growing large.  The automatic constant
    #: determination process may exceed the maximum number of iterations or assign
    #: very long run times in this case.
    ESTIMATE_CONSTANT = 0

    #: Maximum number of simulation frames to output to a dump file.  To prevent
    #: dump files generated during the free energy perturbation step from growing
    #: too large, this value may need to be limited.  The thermodynamic integration
    #: steps do not need to output any data, and the data output during the
    #: perturbation runs is deleted once they complete, so large amounts of storage
    #: are not needed in the long term.  However, if the dump file size is unlimited,
    #: it may exceed the capacity of the filesystem in which the temporary directory
    #: resides.
    MAX_DUMP = 10000

    #: Overestimate guesses by this much during automatic constant determination.
    #: Setting this to a value greater than 1 provides a margin of safety and may
    #: help increase the speed of convergence for parameter determination.
    GUESS_FACTOR = 1

    #: Multiply the value :math:`c-1` for the integration shift constant by this
    #: factor.  This may help to improve the smoothness of the integrand if this
    #: value is chosen appropriately.  Ultimately, if everything else has been
    #: set up properly, the final result should be relatively insensitive to this
    #: parameter even though the nature of the integrand may change.  Setting this
    #: value to 1 recovers the recommendation of
    #: `Frenkel and Ladd, J. Chem. Phys. 1984, 81, 3188-3193 <https://dx.doi.org/10.1063/1.448024>`_.
    #: This value should only ever be decreased below 1.  Increasing it above 1
    #: may cause the integrand to exhibit severe divergence near one endpoint.
    C_FACTOR = 0.1

def log(message, lammps_kwargs):
    """
    Used to log selectively based on the `quiet` parameter seting.

    Parameters
    ----------
    message : str
        The message to display to the standard error stream as long as the
        `quiet` parameter is not set.
    lammps_kwargs : dict
        Keyword arguments to provide to :py:func:`.lammps.run_lammps`.
    """
    if not ("quiet" in lammps_kwargs and lammps_kwargs["quiet"]):
        print(message, file=sys.stderr)

def report(routine, quantity, value, cache, lammps_kwargs):
    """
    Used to issue a report, and also add it to a cache for later recall.

    Parameters
    ----------
    routine : str
        The name of the routine issuing the report.
    quantity : str
        The name of the quantity being logged.
    value : float or (float, float)
        A value, or a value with uncertainty, to log.
    cache : list
        The cache of results to append the result to.
    """

    log("# {:<24} ~ {:<24} = {}".format(routine, quantity,
        "{} +/- {}".format(*value) if type(value) is tuple else value), lammps_kwargs)
    cache.append((routine, quantity, value))

def echo_cache(cache, output_file=sys.stderr):
    """
    Provides a summary of a run based on a cache of results.

    Parameters
    ----------
    cache : list
        The cache of results to display.
    output_file : io.TextIOBase
        The file to which to display the results.  Defaults to the standard
        error stream.
    """

    print(_ECHO_BEGIN, file=output_file)
    last_routine = None
    for routine, quantity, value in cache:
        if last_routine != routine:
            last_routine = routine
            print("# {}".format(last_routine), file=output_file)
        report((" " * 4) + routine, quantity, value, [], {})
    print(_ECHO_END, file=output_file)

def helmholtz(cell, pair, T, N_pool, trans_symm, rot_symm, reference_mode,
    quad_err, constrain, cache, stop=False, overrides={}, **lammps_kwargs):
    """
    Calculates the absolute Helmholtz free energy of a solid using the Einstein
    molecule methodology, extended to work with non-periodic and semi-periodic as
    well as periodic solid systems.  For additional information in the
    literature on solid free energy methods, consult the following references:

    * `Hoover et al., J. Chem. Phys. 1972, 57, 1980-1985 <https://dx.doi.org/10.1063/1.1678518>`_
    * `Frenkel and Ladd, J. Chem. Phys. 1984, 81, 3188-3193 <https://dx.doi.org/10.1063/1.448024>`_
    * `Polson et al., J. Chem. Phys. 2000, 112, 5339-5342 <https://dx.doi.org/10.1063/1.481102>`_
    * `Frenkel and Smit, Academic Press, 2002 <https://dx.doi.org/10.1016/B978-0-12-267351-1.X5000-7>`_
    * `Vega and Noya, J. Chem. Phys. 2007, 127, 154113 <https://dx.doi.org/10.1063/1.2790426>`_
    * `Vega et al., J. Phys.: Condens. Matter 2008, 20, 153101 <https://dx.doi.org/10.1088/0953-8984/20/15/153101>`_
    * `Aragones et al., J. Chem. Phys. 2012, 137, 146101 <https://dx.doi.org/10.1063/1.4758700>`_

    Parts of the computation can be performed in parallel.  Scratch space must
    be available in the system temporary directory to hold the output.
    
    Parameters
    ----------
    cell : crystal.Cell
        The system to measure the Helmholtz free energy of.  This may be a
        periodic supercell or a finite crystallite in two or three dimensions.
        How to constrain the system will be determined based on the periodicity
        specifications of the :py:class:`.crystal.Cell`.
    pair : lammps.Pair
        The pairwise interactions :math:`U_\\mathrm{S}(\\vec{r}^N)` between the atoms.
    T : float
        The temperature :math:`T` at which to measure the free energy.
    N_pool : int
        Number of parallel processes to use for thermodynamic integration.  If this
        is set to a value greater than 1, a Python :py:class:`multiprocessing.pool.Pool`
        will be created: if it is desired to use parallelization in calling :py:func:`helmholtz`
        itself, a :py:class:`multiprocessing.pool.ThreadPool` must be used.  If enough
        runs are being performed, it is recommended to perform each individual run in
        serial (with `N_pool` equal to 1) so that a normal :py:class:`multiprocessing.pool.Pool`
        can be used to call :py:func:`helmholtz` in parallel.  This restriction is present
        as one process pool cannot be created from the worker process of another.
    trans_symm : int
        Translational symmetry number :math:`\\sigma_t`.  This should be equal to 1 for non-periodic
        systems.  For systems with periodicity, this will most likely be greater than 1.
        For fully periodic regular monatomic systems, this should be equal to the number
        of atoms in the system, `cell.atom_count()`.  As with `rot_symm`, it is the
        responsibility of the caller to set the translational symmetry number properly.
    rot_symm : int
        Rotational symmetry number :math:`\\sigma_r`.  This should be equal to 1 for periodic systems.
        For non-periodic and semi-periodic systems, this may be greater than or equal
        to 1.  It is the responsibility of the caller to set the rotational symmetry
        number properly if the system has rotational symmetry.  Currently there is no
        way to automatically determine this parameter.
    reference_mode : ReferenceMode
        Free energy reference mode.  See :py:class:`ReferenceMode` for details.
    quad_err : bool
        Whether or not to include an estimate of the error due to the finite number of
        sample points in the Gaussian quadrature.  Note that this is an approximation and
        simply uses a subset of the Gaussian quadrature points rather than something more
        rigorous such as Gauss-Kronrod quadrature.
    constrain : tuple(int)
        Indices of the atoms to use for applying constraints.  The number of atoms
        necessary will depend on the dimensionality and periodicity of the system.
        For systems which are not completely periodic, the input structure may be rotated
        and subsequently constrained based upon the positions of these atoms.  Care should
        be taken to select atoms for which this is a valid operation; e.g. if three atoms are
        required, they should not be collinear.
    cache : list
        Cache to which intermediate results will be appended.
    stop : bool
        Whether or not to halt after determining optimal parameters.  This can be useful to
        perform the automatic parameter determination once and use the parameters for a large
        number of runs.
    overrides : dict
        Overrides for default values in parameter estimation.  The keys should be names
        in :py:class:`OptimizationDefaults` and the values should be the desired overridden
        values.
    lammps_kwargs : dict
        Keyword arguments to provide to :py:func:`.lammps.run_lammps`.

    Returns
    -------
    float, float
        The absolute Helmholtz free energy per atom, :math:`A/N` of the system,
        along with an estimate of the error.
    """

    # Pull out all options and set up overrides
    O_defaults = {name: value for name, value in vars(OptimizationDefaults).items() if not name.startswith("__")}
    O = {**O_defaults, **overrides}
    
    # Measure energy of static lattice
    U_latt = relax.energy(cell, pair, **lammps_kwargs)

    # Determine optimal maximum spring constant: this will ultimately set all
    # other parameters.
    k_E = O["INITIAL_CONSTANT"]
    for iteration_count in range(O["MAX_ITERATIONS"]):
        report("helmholtz", "iter_1", iteration_count, cache, lammps_kwargs)

        # Use enough timesteps per fastest vibrational period
        period = 2 * numpy.pi / numpy.sqrt(k_E)
        dt = period / O["TIMESTEPS_PER_PERIOD"]
        tau_damp = O["THERMOSTAT_PERIODS"] * period

        # Set initial guesses for what run lengths should be
        N_equil = int(round(O["EQUIL_SHORT_PERIODS"] * period / dt))
        N_prod = int(round(O["PROD_SHORT_PERIODS"] * period / dt))
        N_block = int(numpy.ceil(numpy.sqrt(N_prod)))
    
        # Cap out dump interval to prevent files from becoming large
        N_dump = int(numpy.ceil(N_prod / O["MAX_DUMP"]))
        
        # Assume that this may be proportional to 1/k_E to make guesses
        report("helmholtz", "N_equil_1", N_equil, cache, lammps_kwargs)
        report("helmholtz", "N_prod_1", N_prod, cache, lammps_kwargs)
        report("helmholtz", "N_block_1", N_block, cache, lammps_kwargs)
        real_offset = (run_em_a1(cell, pair, dt, N_equil, N_prod, N_block, N_dump,
            T, tau_damp, k_E, U_latt, constrain, cache, **lammps_kwargs)[0] - U_latt) / T
        if real_offset <= O["SPRING_OFFSET"]:
            break
        else:
            k_E *= O["GUESS_FACTOR"] * real_offset / O["SPRING_OFFSET"]
    else:
        # Exceeded number of iterations
        raise RuntimeError("Exceeded MAX_ITERATIONS in guessing k_E (is SPRING_OFFSET too small?)")

    # Figure out what the slowest vibrational period will be so that the runs
    # with the lowest values of k_E / values of lambda closest to 1 will still
    # have adequate sampling
    for iteration_count in range(O["MAX_ITERATIONS"]):
        # Perform run to estimate MSD in real crystal, and get period
        report("helmholtz", "iter_2", iteration_count, cache, lammps_kwargs)
        report("helmholtz", "N_equil_2", N_equil, cache, lammps_kwargs)
        report("helmholtz", "N_prod_2", N_prod, cache, lammps_kwargs)
        report("helmholtz", "N_block_2", N_block, cache, lammps_kwargs)
        msd_real = run_em_a2(cell, pair, dt, N_equil, N_prod, N_block, T, tau_damp,
            O["ESTIMATE_CONSTANT"], constrain, cache, False, **lammps_kwargs)[0]
        # T = 2pi*sqrt(m/k_E), MSD = d/beta*k_E => T = 2pi*sqrt(m*MSD/dk_BT)
        period_real = 2 * numpy.pi * numpy.sqrt(msd_real / (cell.dimensions * T))

        # Determine what real N_equil and N_prod should be
        N_equil_desired = O["EQUIL_LONG_PERIODS"] * period_real / dt
        N_prod_desired = O["PROD_LONG_PERIODS"] * period_real / dt

        # If the actual values are not long enough, go around again
        if N_equil >= N_equil_desired and N_prod >= N_prod_desired:
            break
        else:
            N_equil = int(round(O["GUESS_FACTOR"] * N_equil_desired))
            N_prod = int(round(O["GUESS_FACTOR"] * N_prod_desired))
            N_block = int(numpy.ceil(numpy.sqrt(N_prod)))
    else:
        # Exceeded number of iterations
        raise RuntimeError("Exceeded MAX_ITERATIONS in guessing N_prod (is metastable system transforming?)")

    # Recalculate dump interval now that N_prod is known
    N_dump = int(numpy.ceil(N_prod / O["MAX_DUMP"]))

    # Use the crystal MSD to set the optimal integration shift constant
    # Based on recommendation of Frenkel and Ladd, J. Chem. Phys. 1984, 81, 3188-3193
    cd1 = 2 * O["C_FACTOR"] * T / (k_E * msd_real)

    # Constants are going to get rounded up in other routines, so just do it now
    N_block = int(numpy.ceil(N_block / N_dump)) * N_dump
    N_prod = int(numpy.ceil(N_prod / N_block)) * N_block

    # Report the optimized constants for this run
    report("helmholtz", "Delta_t", dt, cache, lammps_kwargs)
    report("helmholtz", "N_equil", N_equil, cache, lammps_kwargs)
    report("helmholtz", "N_prod", N_prod, cache, lammps_kwargs)
    report("helmholtz", "N_block", N_block, cache, lammps_kwargs)
    report("helmholtz", "N_dump", N_dump, cache, lammps_kwargs)
    report("helmholtz", "tau_damp", tau_damp, cache, lammps_kwargs)
    report("helmholtz", "k_E_max", k_E, cache, lammps_kwargs)
    report("helmholtz", "N_quad", O["QUADRATURE_POINTS"], cache, lammps_kwargs)
    report("helmholtz", "cd1", cd1, cache, lammps_kwargs)
    report("helmholtz", "call", ("run_em({}, {}, {!r}, {!r}, {!r}, {!r}, {!r}, {!r}, {!r}, {!r}, {!r}, {!r}, {!r}, " +
        "{!r}, {!r}, {!r}, {}, {!r}, {!r}, {}, **{!r})").format("cell", "pair", dt, N_equil, N_prod, N_block, N_dump, T,
        tau_damp, k_E, U_latt, O["QUADRATURE_POINTS"], N_pool, cd1, trans_symm, rot_symm, reference_mode, quad_err,
        constrain, "cache", lammps_kwargs), cache, lammps_kwargs)

    # Caller may have wished simply to get constants for run
    if stop:
        return

    # Everything has been determined now; call main routine
    return run_em(cell, pair, dt, N_equil, N_prod, N_block, N_dump, T, tau_damp,
        k_E, U_latt, O["QUADRATURE_POINTS"], N_pool, cd1, trans_symm, rot_symm,
        reference_mode, quad_err, constrain, cache, **lammps_kwargs)

def run_em(cell, pair, dt, N_equil, N_prod, N_block, N_dump, T, tau_damp, k_E_max, U_latt, N_gl,
    N_pool, cd1, trans_symm, rot_symm, reference_mode, quad_err, constrain, cache, **lammps_kwargs):
    """
    Driver routine for Einstein molecule calculations.  This routine performs no
    automatic constant determination, unlike :py:func:`helmholtz`.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    pair : lammps.Pair
        See :py:func:`helmholtz`.
    dt : float
        The timestep for molecular dynamics.
    N_equil : int
        The number of equilibration steps.  The system will be thermalized using
        a Langevin thermostat during this time, but no data will be recorded.
    N_prod : int
        The number of production steps.  Data will be collected during this time.
    N_block : int
        The interval for block averaging, which will be used to estimate the
        statistical error in the output.  This should evenly divide `N_prod`.
    N_dump : int
        The dump interval.  Can be used to avoid taking too many samples on runs
        which require very small timesteps, which can reduce the size of trajectory
        files during free energy perturbation.  This should evenly divide `N_block`.
    T : float
        See :py:func:`helmholtz`.
    tau_damp : float
        The damping time for the Langevin thermostat.
    k_E_max : float
        The maximum harmonic spring constant :math:`k_E` to use during the simulations.
    U_latt : float
        The equilibrium static lattice energy :math:`U_\\mathrm{S}(\\vec{r}_0^N)`
        (the internal energy at zero temperature).  This is used to avoid loss of precision
        in computing the Boltzmann factors during free energy perturbation.
    N_gl : int
        Number of sample points for Gauss-Legendre quadrature.
    N_pool : int
        Number of thermodynamic integration points to evaluate in parallel.
    cd1 : float
        Offset for variable :math:`\\lambda` when performing the integral transformation
        :math:`u=\\ln(c-\\lambda)` during thermodynamic integration.  This parameter is
        :math:`c-1` and should be positive but close to zero.  This is used instead of
        :math:`c` to prevent loss of numerical precision.
    trans_symm : int
        See :py:func:`helmholtz`.
    rot_symm : int
        See :py:func:`helmholtz`.
    reference_mode : ReferenceMode
        See :py:func:`helmholtz`.
    quad_err : bool
        See :py:func:`helmholtz`.
    constrain : tuple(int)
        See :py:func:`helmholtz`.
    cache : list
        See :py:func:`helmholtz`.
    lammps_kwargs : dict
        See :py:func:`helmholtz`.

    Returns
    -------
    float, float
        The absolute Helmholtz free energy per atom, :math:`A/N` of the system,
        along with an estimate of the error.
    """

    aref = calc_ig_a0(cell, T, False, cache, **lammps_kwargs) if reference_mode == ReferenceMode.EXCESS else 0
    a0 = calc_em_a0(cell, T, k_E_max, trans_symm, rot_symm,
        reference_mode == ReferenceMode.WAVELENGTH_UNITY, constrain, cache, **lammps_kwargs)
    delta_a1 = run_em_a1(cell, pair, dt, N_equil, N_prod, N_block, N_dump, T,
        tau_damp, k_E_max, U_latt, constrain, cache, **lammps_kwargs)
    delta_a2 = integrate_em_a2(cell, pair, dt, N_equil, N_prod, N_block, T,
        tau_damp, k_E_max, N_gl, N_pool, cd1, quad_err, constrain, cache, **lammps_kwargs)

    a = -aref + a0 + delta_a1[0] + delta_a2[0]
    da = numpy.sqrt((delta_a1[1] ** 2) + (delta_a2[1] ** 2))
    report("run_em", "beta*A_ref", aref / T, cache, lammps_kwargs)
    report("run_em", "beta*A_0", a0 / T, cache, lammps_kwargs)
    report("run_em", "beta*DeltaA_1", (delta_a1[0] / T, delta_a1[1] / T), cache, lammps_kwargs)
    report("run_em", "beta*DeltaA_2", (delta_a2[0] / T, delta_a2[1] / T), cache, lammps_kwargs)
    report("run_em", "beta*A", (a / T, da / T), cache, lammps_kwargs)
    return a, da

def calc_ig_a0(cell, T, lambda_norm, cache, **lammps_kwargs):
    """
    Determines the ideal gas free energy.
    
    .. math::
        \\frac{A_\\mathrm{IG}}{Nk_BT} = \\ln(\\rho\\Lambda^d)-1

    where:

    .. math::
        \\Lambda = \\frac{h}{\\sqrt{2\\pi mk_BT}}

    and :math:`d` is the number of dimensions, 2 or 3.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    T : float
        See :py:func:`helmholtz`.
    lambda_norm : bool
        If true, the free energy will be given as if the thermal de Broglie
        wavelength :math:`\\Lambda` is equal to 1.  Otherwise, it will be given
        as if the Planck constant :math:`h` is equal to 1.
    cache : list
        See :py:func:`helmholtz`.
    lammps_kwargs : dict
        See :py:func:`helmholtz`.

    Returns
    -------
    float
        The absolute Helmholtz free energy per atom, :math:`A_\\mathrm{IG}/N`.
    """

    # Note: Additional term in Stirling approximation for small N is not currently used
    log_lambda = 0 if lambda_norm else -0.5 * numpy.log(2 * numpy.pi * T)
    beta_a0 = numpy.log(cell.density) + (cell.dimensions * log_lambda) - 1
    report("calc_ig_a0", "beta*A_0_ig", beta_a0, cache, lammps_kwargs)
    return beta_a0 * T

def calc_em_a0(cell, T, k_E, trans_symm, rot_symm, lambda_norm, constrain, cache, **lammps_kwargs):
    """
    Determines the ideal Einstein molecule free energy
    :math:`A_\\mathrm{EM}=-k_BT\\ln Q_\\mathrm{EM}`.  The partition function is:

    .. math::
        Q_\\mathrm{EM}=\\frac{V}{\\sigma_t\\sigma_r\\Lambda^\\nu}{\\left(\\frac{2\\pi k_BT}{k_E}\\right)}^{\\nu_v/2}\\prod_{i=1}^{d_f-1}\\frac{2\\pi^{(i+1)/2}{\\left<\\ell_i\\right>}^i}{\\Gamma[(i+1)/2]}

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    T : float
        See :py:func:`helmholtz`.
    k_E : float
        See :py:func:`run_em`.
    trans_symm : int
        See :py:func:`helmholtz`.
    rot_symm : int
        See :py:func:`helmholtz`.
    lambda_norm : bool
        See :py:func:`calc_ig_a0`.
    constrain : tuple(int)
        See :py:func:`helmholtz`.
    cache : list
        See :py:func:`helmholtz`.
    lammps_kwargs : dict
        See :py:func:`helmholtz`.

    Returns
    -------
    float
        The absolute Helmholtz free energy per atom, :math:`A_\\mathrm{EM}/N`.
    """

    atom_count = cell.atom_count()
    dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, const_cell, const_radii = prepare_constrain(cell, constrain)
    
    # Calculate contributions to -log(Q)

    # Momentum term (thermal wavelength)
    nlq_momentum = 0 if lambda_norm else -dof * numpy.log(2 * numpy.pi * T) / 2
    # Translation term
    nlq_trans = -numpy.log(cell.enclosed)
    # Translational symmetry term (regarding periodic images)
    nlq_trans_symm = numpy.log(trans_symm)
    # Rotation terms
    nlq_rot = -sum((i + 1) * numpy.log(l) for i, l in enumerate(const_radii)) - calc_rot_term(free_dims)
    # Rotational symmetry term (not currently automatically determined)
    nlq_rot_symm = numpy.log(rot_symm)
    # Vibration term
    nlq_vib = -vib_dof * numpy.log(2 * numpy.pi * T / k_E) / 2

    # Calculate terms per atom
    beta_a0_momentum = nlq_momentum / atom_count
    beta_a0_trans = nlq_trans / atom_count
    beta_a0_trans_symm = nlq_trans_symm / atom_count
    beta_a0_rot = nlq_rot / atom_count
    beta_a0_rot_symm = nlq_rot_symm / atom_count
    beta_a0_vib = nlq_vib / atom_count
    beta_a0 = beta_a0_momentum + beta_a0_trans + beta_a0_trans_symm + \
        beta_a0_rot + beta_a0_rot_symm + beta_a0_vib

    # Report
    report("calc_em_a0", "beta*A_0_m", beta_a0_momentum, cache, lammps_kwargs)
    report("calc_em_a0", "beta*A_0_t", beta_a0_trans, cache, lammps_kwargs)
    report("calc_em_a0", "beta*A_0_ts", beta_a0_trans_symm, cache, lammps_kwargs)
    report("calc_em_a0", "beta*A_0_r", beta_a0_rot, cache, lammps_kwargs)
    report("calc_em_a0", "beta*A_0_rs", beta_a0_rot_symm, cache, lammps_kwargs)
    report("calc_em_a0", "beta*A_0_v", beta_a0_vib, cache, lammps_kwargs)
    report("calc_em_a0", "beta*A_0", beta_a0, cache, lammps_kwargs)

    return beta_a0 * T

def run_em_a1(cell, pair, dt, N_equil, N_prod, N_block, N_dump, T, tau_damp,
        k_E, U_latt, constrain, cache, **lammps_kwargs):
    """
    Performs a free energy perturbation simulation to calculate :math:`\\Delta A_1`
    for an Einstein molecule.  This is the free energy change between the ideal
    Einstein molecule and an Einstein molecule with both harmonic springs and the
    real interaction potential.

    .. math::
        \\frac{\\Delta A_1}{Nk_BT}=\\frac{U_\\mathrm{S}(\\vec{r}_0^N)}{Nk_BT}-\\ln{\\left<\\exp\\left[-\\frac{U_\\mathrm{S}(\\vec{r}^N)-U_\\mathrm{S}(\\vec{r}_0^N)}{Nk_BT}\\right]\\right>}_\\mathrm{EM}

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    pair : lammps.Pair
        See :py:func:`helmholtz`.
    dt : float
        See :py:func:`run_em`.
    N_equil : int
        See :py:func:`run_em`.
    N_prod : int
        See :py:func:`run_em`.
    N_block : int
        See :py:func:`run_em`.
    N_dump : int
        See :py:func:`run_em`.
    T : float
        See :py:func:`helmholtz`.
    tau_damp : float
        See :py:func:`run_em`.
    k_E : float
        See :py:func:`run_em`.
    U_latt : float
        See :py:func:`run_em`.
    constrain : tuple(int)
        See :py:func:`helmholtz`.
    cache : list
        See :py:func:`helmholtz`.
    lammps_kwargs : dict
        See :py:func:`helmholtz`.

    Returns
    -------
    float, float
        The  Helmholtz free energy difference per atom, :math:`\\Delta A_1/N`,
        along with an estimate of the error.
    """
    
    # Report
    report("run_em_a1", "k_E", k_E, cache, lammps_kwargs)

    # Get real intervals (they should be proper multiples)
    N_block_real = int(numpy.ceil(N_block / N_dump)) * N_dump
    N_prod_real = int(numpy.ceil(N_prod / N_block_real)) * N_block_real

    # Run 1: Oscillate the atoms on harmonic springs without the pair interactions
    run_1 = lammps.LAMMPS()
    dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, const_cell, const_radii = prepare_constrain(cell, constrain)

    run_1.cell(const_cell)

    # Prepare atom groups for atoms which should have their forces zeroed
    for constrain_index, atom_index in enumerate(constrain):
        run_1.group("freeze{}".format(constrain_index), "id", atom_index + 1)

    # Set up interactions
    run_1.fix("harmonic", "all", "spring/self", k_E)
    run_1.fix_modify("harmonic", "energy", "yes")

    # There are no pairwise interactions: turn off atom reordering or LAMMPS fails
    run_1.atom_modify("sort", 0, 0)

    # Set up NVT ensemble
    run_1.fix("thermostat", "all", "langevin", T, T, tau_damp, lammps.random_seed())
    run_1.fix("integrator", "all", "nve")

    # Constrain atoms as necessary
    for constrain_index in range(len(constrain)):
        run_1.fix("freeze{}".format(constrain_index), "freeze{}".format(constrain_index),
            "setforce", *("NULL" if index < constrain_index else 0 for index in range(3)))
    if const_cell.dimensions == 2:
        run_1.fix("twodim", "all", "enforce2d")

    # Set up initial velocities (Maxwell-Boltzmann)
    run_1.velocity("all", "create", T, lammps.random_seed(), "dist", "gaussian")
    for constrain_index in range(len(constrain)):
        # Only zero out velocities of atoms which have their forces zeroed
        run_1.velocity("freeze{}".format(constrain_index), "set",
            *("NULL" if index < constrain_index else 0 for index in range(3)))

    # Perform equilibration steps
    run_1.timestep(dt)
    run_1.run(N_equil)

    # Perform production steps
    run_1.reset_timestep(0)
    run_1.dump("traj", "all", "atom", N_dump, "traj.atom")
    run_1.dump_modify("traj", "format", "line", "%d %d %.8g %.8g %.8g")
    run_1.run(N_prod_real)

    # Prepare a cache between the two run_lammps calls to hold the "traj.atom"
    # file which could grow very large in some cases with large systems or a
    # large value of MAX_DUMP.  In this way, the file will be created once and
    # read once from the filesystem with no stops in memory.
    with lammps.PersistentStorage() as traj_cache:
        # Call LAMMPS
        answer_1 = lammps.run_lammps(run_1, [],
            move_out=[("traj.atom", traj_cache("traj.atom"))],
            **lammps_kwargs)

        # Run 2: Calculate energies with the real Hamiltonian using the ideal trajectory
        run_2 = lammps.LAMMPS()
        run_2.cell(const_cell)
        pair(run_2)

        # Set up information for thermodynamic averaging
        run_2.variable("boltzmann", "equal", "exp(-(epair-{})/{})".format(U_latt, T))
        run_2.fix("boltzmann_mean", "all", "ave/time", N_dump, N_block_real // N_dump, N_block_real, "v_boltzmann")
        run_2.thermo(N_block_real)
        run_2.thermo_style("custom", "step", "epair", "f_boltzmann_mean")
        run_2.thermo_modify("format", "float", "%.16g")

        # Run using the old trajectory
        run_2.rerun("traj.atom", "dump", *(["x", "y"] + (["z"] if const_cell.dimensions == 3 else [])), "box", "yes")
        
        # Call LAMMPS
        answer_2 = lammps.run_lammps(run_2, [],
            move_in=[(traj_cache("traj.atom"), "traj.atom")],
            **lammps_kwargs)

    # Get Boltzmann factor data and perform calculations
    boltzmann = numpy.stack(lammps.log_seek(answer_2[0], "step e_pair f_boltzmann_mean",
        True, N_prod_real // N_block_real))[:, 2]
    boltzmann_avg = numpy.mean(boltzmann)
    boltzmann_err = scipy.stats.sem(boltzmann)
    delta_a1_avg = U_latt - (T * numpy.log(boltzmann_avg))
    delta_a1_err = T * boltzmann_err / boltzmann_avg
    
    # Report
    report("run_em_a1", "beta*U_latt", U_latt / T, cache, lammps_kwargs)
    report("run_em_a1", "<exp(-beta*DeltaU)>", (boltzmann_avg, boltzmann_err), cache, lammps_kwargs)
    report("run_em_a1", "beta*DeltaA_1", (delta_a1_avg / T, delta_a1_err / T), cache, lammps_kwargs)
    return delta_a1_avg, delta_a1_err

def run_em_a2(cell, pair, dt, N_equil, N_prod, N_block, T,
        tau_damp, k_E, constrain, cache, parallel, **lammps_kwargs):
    """
    Performs a run to calculate a term in the integrand for :math:`\\Delta A_2`
    for an Einstein molecule.  See :py:func:`integrate_em_a2`.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    pair : lammps.Pair
        See :py:func:`helmholtz`.
    dt : float
        See :py:func:`run_em`.
    N_equil : int
        See :py:func:`run_em`.
    N_prod : int
        See :py:func:`run_em`.
    N_block : int
        See :py:func:`run_em`.
    T : float
        See :py:func:`helmholtz`.
    tau_damp : float
        See :py:func:`run_em`.
    k_E : float
        See :py:func:`run_em`.
    constrain : tuple(int)
        See :py:func:`helmholtz`.
    cache : list
        See :py:func:`helmholtz`.
    parallel : bool
        If true, the modified cache list will be returned.  This is required
        to return result information when the routine is invoked in parallel.
    lammps_kwargs : dict
        See :py:func:`helmholtz`.

    Returns
    -------
    float, float[, list]
        Mean square displacement of the atoms about their lattice sites,
        averaged over the course of the simulation, along with an estimate of
        the error in this quantity.  If `parallel` is `True`, the modified
        `cache` will also be passed back.
    """

    # Report
    report("run_em_a2", "k_E", k_E, cache, lammps_kwargs)

    # Get real intervals (they should be proper multiples)
    N_prod_real = int(numpy.ceil(N_prod / N_block)) * N_block

    # Run: Use real potentials and harmonic springs together
    run = lammps.LAMMPS()
    dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof, const_cell, const_radii = prepare_constrain(cell, constrain)
    run.cell(const_cell)

    # Prepare atom groups for atoms which should have their forces zeroed
    for constrain_index, atom_index in enumerate(constrain):
        run.group("freeze{}".format(constrain_index), "id", atom_index + 1)

    # Set up interactions
    pair(run)
    if k_E: # LAMMPS won't set harmonic springs with zero energy
        run.fix("harmonic", "all", "spring/self", k_E)
        run.fix_modify("harmonic", "energy", "yes")

    # Set up NVT ensemble
    run.fix("thermostat", "all", "langevin", T, T, tau_damp, lammps.random_seed())
    run.fix("integrator", "all", "nve")
    
    # Constrain atoms as necessary
    for constrain_index in range(len(constrain)):
        run.fix("freeze{}".format(constrain_index), "freeze{}".format(constrain_index),
            "setforce", *("NULL" if index < constrain_index else 0 for index in range(3)))
    if const_cell.dimensions == 2:
        run.fix("twodim", "all", "enforce2d")

    # Set up initial velocities (Maxwell-Boltzmann)
    run.velocity("all", "create", T, lammps.random_seed(), "dist", "gaussian")
    for constrain_index in range(len(constrain)):
        # Only zero out velocities of atoms which have their forces zeroed
        run.velocity("freeze{}".format(constrain_index), "set",
            *("NULL" if index < constrain_index else 0 for index in range(3)))

    # Set up mean square displacement computation (this will set the MSD
    # reference positions to the lattice positions, even though averages
    # will not be accumulated during equilibration)
    run.compute("msd", "all", "msd")

    # Perform equilibration steps
    run.timestep(dt)
    run.run(N_equil)

    # Turn on averaging of MSD (this must be delayed until now since the
    # timestep needs to be reset, but c_msd has been getting calculated based
    # on the lattice positions since the beginning of the equilibration stage)
    run.reset_timestep(0)
    run.fix("msd_mean", "all", "ave/time", 1, N_block, N_block, "c_msd[4]")
    run.thermo(N_block)
    run.thermo_style("custom", "step", "temp", "epair", "pe", "f_msd_mean")

    # Do output in case debugging might have been requested
    if lammps_kwargs.get("debug_repl"):
        run.dump("traj", "all", "atom", round(float(numpy.sqrt(N_prod_real))), "traj.atom")
        run.dump_modify("traj", "format", "line", "%d %d %.8g %.8g %.8g")

    # Perform production steps
    run.run(N_prod_real)

    # Call LAMMPS
    answer = lammps.run_lammps(run, [], **lammps_kwargs)

    # Get time average of mean square displacement and perform calculations
    msd = numpy.stack(lammps.log_seek(answer[0], "step temp e_pair poteng f_msd_mean",
        True, N_prod_real // N_block))[:, 4]
    msd_avg = numpy.mean(msd)
    msd_err = scipy.stats.sem(msd)

    # Report
    report("run_em_a2", "<Sum|r-r_0|^2>/N", (msd_avg, msd_err), cache, lammps_kwargs)
    if parallel:
        return msd_avg, msd_err, cache
    else:
        return msd_avg, msd_err

def integrate_em_a2(cell, pair, dt, N_equil, N_prod, N_block, T, tau_damp,
        k_E_max, N_gl, N_pool, cd1, quad_err, constrain, cache, **lammps_kwargs):
    """
    Performs thermodynamic integration to calculate :math:`\\Delta A_2` for an
    Einstein molecule.  This is the free energy change between an Einstein molecule
    with both harmonic springs and the real interaction potential, and the real
    solid (with translational and rotational constraints).

    .. math::
        \\Delta A_2=\int_0^1{\\left<U_\\mathrm{I}(\\lambda;\\vec{r}^N)\\right>}_\\lambda\\,d\\lambda

    where the potential applied during the thermodynamic integration is:

    .. math::
        U_\\mathrm{I}(\\lambda;\\vec{r}^N)=\\lambda U_\\mathrm{S}(\\vec{r}^N)+(1-\\lambda)(U_\\mathrm{S}(\\vec{r}^N)+U_\\mathrm{EM}(\\vec{r}^N))

    and the reference harmonic potential is:

    .. math::
        U_\\mathrm{EM}(\\vec{r}^N)=\\frac{k_E}{2}\\sum_{i=1}^N\\sum_{j=1}^d{\\left[r_{ij}-r_{0,ij}(\\vec{r}^N)\\right]}^2

    Note that the "reference positions" are defined based on all atomic coordinates: particular reference
    atoms define the coordinate system which sets the reference lattice points.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    pair : lammps.Pair
        See :py:func:`helmholtz`.
    dt : float
        See :py:func:`run_em`.
    N_equil : int
        See :py:func:`run_em`.
    N_prod : int
        See :py:func:`run_em`.
    N_block : int
        See :py:func:`run_em`.
    T : float
        See :py:func:`helmholtz`.
    tau_damp : float
        See :py:func:`run_em`.
    k_E_max : float
        See :py:func:`run_em`.
    N_gl : int
        See :py:func:`run_em`.
    N_pool : int
        See :py:func:`run_em`.
    cd1 : float
        See :py:func:`run_em`.
    quad_err : bool
        See :py:func:`run_em`.
    constrain : tuple(int)
        See :py:func:`helmholtz`.
    cache : list
        See :py:func:`helmholtz`.
    lammps_kwargs : dict
        See :py:func:`helmholtz`.

    Returns
    -------
    float, float
        The  Helmholtz free energy difference per atom, :math:`\\Delta A_2/N`,
        along with an estimate of the error.
    """

    # Nominal integral is from 1 to 0 of (k_E/2)<Sum|r-r_0|^2>_lambda d_lambda.
    # Substitute u = ln(c-lambda) -> (lambda-c) du = dlambda.  Integral to
    # evaluate is from ln(c) to ln(c-1) of
    # (k_E/2)exp(u)<Sum|r-r_0|^2>_(c-exp(u)).
    
    # Take note that:
    # u = ln(c-lambda) -> lambda = c - exp(u); k_E = k_E_max * (1-lambda)
    # dlambda = (lambda-c) du = -exp(u) du (negative sign is already in integral
    # limits) <dU/d_lambda>_lambda uses maximum k_E times MSD measured at
    # current k_E in integrand
    
    report("integrate_em_a2", "k_E_max", k_E_max, cache, lammps_kwargs)

    # Determine evaluation points and weights for Gauss-Legendre quadrature
    x_unscaled, w_unscaled = numpy.polynomial.legendre.leggauss(N_gl)
    a, b = numpy.log1p(cd1), numpy.log(cd1)
    scale_factor = (b - a) / 2
    u_list, w_list = a + (scale_factor * (x_unscaled + 1)), scale_factor * w_unscaled
    exp_u_cd1 = numpy.exp(u_list) - cd1
    lambda_list = 1 - exp_u_cd1
    k_E_list = k_E_max * exp_u_cd1

    # Report inputs for integration
    for index, (u, w, lambda_, k_E) in enumerate(zip(u_list, w_list, lambda_list, k_E_list)):
        report("integrate_em_a2", "u_{}".format(index + 1), u, cache, lammps_kwargs)
        report("integrate_em_a2", "w_{}".format(index + 1), w, cache, lammps_kwargs)
        report("integrate_em_a2", "lambda_{}".format(index + 1), lambda_, cache, lammps_kwargs)
        report("integrate_em_a2", "k_E_{}".format(index + 1), k_E, cache, lammps_kwargs)

    # Perform integration (either in serial or in parallel)
    if N_pool == 1:
        results = [run_em_a2(cell, pair, dt, N_equil, N_prod, N_block, T, tau_damp,
            k_E, constrain, cache, False, **lammps_kwargs) for k_E in k_E_list]
    else:
        with multiprocessing.Pool(N_pool) as pool:
            # Run in parallel
            async_results = [pool.apply_async(run_em_a2, (cell, pair, dt, N_equil, N_prod, N_block,
                T, tau_damp, k_E, constrain, [], True), lammps_kwargs) for k_E in k_E_list]
            pool.close()
            pool.join()

            # Post-process to pull out the results
            sync_results = [result.get() for result in async_results]
            results = [sync_result[:2] for sync_result in sync_results]
            for sync_result in sync_results:
                cache.extend(sync_result[2])

    # Process results
    factor_list = k_E_max * numpy.exp(u_list) / 2
    integrand_avg = factor_list * numpy.array([result[0] for result in results])
    integrand_err = factor_list * numpy.array([result[1] for result in results])
    integral_avg = numpy.sum(w_list * integrand_avg)
    integral_err = numpy.sqrt(-numpy.sum(w_list * (integrand_err ** 2)))

    # Error will also include contribution from finite number of Gaussian
    # quadrature points: make a conservative estimate of total error as follows:
    #  1) Add quadrature error to error from integrand instead of taking norm
    #  2) Estimate error based on quadrature scheme from half of the Gaussian
    #     quadrature points (look at difference between integral values)
    
    # For an odd number of points, don't include the endpoints; for an even
    # number of points, cut two points off of the right end (these have the
    # lowest spring constant and will contribute the most to the singularity if
    # one is present)

    err_x_unscaled = x_unscaled[1:-1:2]
    exponents = numpy.arange(err_x_unscaled.shape[0])
    err_w_list = numpy.linalg.solve(err_x_unscaled ** exponents.reshape((exponents.shape[0], 1)),
        (1 - ((-1) ** (exponents + 1))) / (exponents + 1)) * scale_factor
    err_integral = numpy.sum(err_w_list * integrand_avg[1:-1:2])
    quadrature_err = numpy.abs(err_integral - integral_avg)

    # Report results
    for index, (avg, err) in enumerate(zip(integrand_avg, integrand_err)):
        report("integrate_em_a2", "f(u_{})".format(index + 1), (avg, err), cache, lammps_kwargs)
    # Report this value with the error from both the integrand points alone and
    # the quadrature points alone
    report("integrate_em_a2", "beta*DeltaA_2_samp_err", (integral_avg / T, integral_err / T), cache, lammps_kwargs)
    report("integrate_em_a2", "beta*DeltaA_2_quad_err", (integral_avg / T, quadrature_err / T), cache, lammps_kwargs)
    if quad_err:
        # Only add the quadrature error on if the quad_err flag is set
        integral_err += quadrature_err
    report("integrate_em_a2", "beta*DeltaA_2", (integral_avg / T, integral_err / T), cache, lammps_kwargs)
    return integral_avg, integral_err

def prepare_constrain(cell, constrain):
    """
    Calculates various parameters necessary for constraining cells.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    constrain : tuple(int)
        See :py:func:`helmholtz`.

    Returns
    -------
    int, int, int, int, int, int, int, crystal.Cell, numpy.ndarray
        Number of dimensions :math:`d`, number of periodic dimensions :math:`d_p`,
        number of non-periodic dimensions :math:`d_n=d-d_p`, number of degrees of freedom (DOF) :math:`\\nu=dN`,
        number of translational DOF :math:`\\nu_t=d`, number of rotational DOF :math:`\\nu_r=d_n(d_n-1)/2`,
        number of vibrational DOF :math:`\\nu_v=\\nu-\\nu_t-\\nu_r`, transformed cell ready to be
        constrained, and characteristic distances :math:`\\left<\\ell_i\\right>`.
    """

    # Get the number of periodic and non-periodic dimensions
    dims = cell.dimensions
    period_dims = numpy.sum(cell.periodic)
    free_dims = dims - period_dims
    
    # Get number of degrees of freedom of various special types
    dof = dims * cell.atom_count()
    # There will always be as many translational degrees of freedom as dimensions
    trans_dof = dims
    # In d dimensions, there will be d(d - 1)/2 rotational degrees of freedom for
    # an unconstrained crystal, but adding a single periodic boundary effectively
    # reduces the number of dimensions available for rotation by 1
    rot_dof = free_dims * (free_dims - 1) // 2
    # All remaining degrees of freedom are associated with crystal vibrational motion
    vib_dof = dof - trans_dof - rot_dof

    # Check that the correct number of constraints are applied
    # There will always need to be one constrained atom due to translation
    if len(constrain) != max(1, free_dims):
        raise ValueError("unexpected number of constrained atoms for given geometry")
    # All parameters ready to be dispatched
    params = (dims, period_dims, free_dims, dof, trans_dof, rot_dof, vib_dof)

    # Make vector matrix triangular: otherwise this will happen in LAMMPS and may mess up constraints
    tri_cell = crystal.CellTools.normalize(cell)
    # Get map of constraint atom indices to original atom indices and vice versa
    atom_indices = [(type_index, atom_index) for type_index in range(tri_cell.atom_types)
        for atom_index in range(tri_cell.atom_count(type_index))]
    flat_indices, start_index = [], 0
    for type_index in range(tri_cell.atom_types):
        flat_indices.append([])
        for atom_index in range(tri_cell.atom_count(type_index)):
            flat_indices[-1].append(start_index)
            start_index += 1
    # Make cell periodic (avoid lost atoms in LAMMPS) and move desired atom to zero position
    tri_cell = with_periodic(crystal.CellTools.shift(tri_cell, *atom_indices[constrain[0]]), True)
    # Get positions of atoms involved in rotational constraints
    all_positions = numpy.concatenate(tri_cell.atom_lists)
    constrain_positions = all_positions[constrain[1:], :]

    # Constraint application involves some special casing since general case is
    # not useful, not supported anyway in LAMMPS, and somewhat complicated
    if (dims == 2 or dims == 3) and free_dims < 2:
        # Translational constraint only: nothing special needs to happen
        # since any rotational degrees of freedom should be constrained by boundaries
        # This includes semi-infinite 3D slabs: same as periodic case w.r.t. constraints!
        return (*params, tri_cell, numpy.array([]))

    elif dims == 2 and free_dims == 2:
        # 2D free crystallite case: single atom constrained to x-axis
        # Need to rotate all atoms about origin such that cp[0,1] = 0
        M_factor = numpy.sqrt(constrain_positions[0, 0] ** 2 + constrain_positions[0, 1] ** 2)
        M = numpy.array([
            [+constrain_positions[0, 0], +constrain_positions[0, 1]],
            [-constrain_positions[0, 1], +constrain_positions[0, 0]]
        ]) / M_factor
       
        # It is possible for atoms to move out of the box when this happens; the system is periodic
        # and will be wrapped before going into LAMMPS so this is not a problem.  It is assumed that
        # users will use a large enough box to contain the system even if it is rotated!  This is not
        # manually checked, and it is recommended to inspect the LAMMPS input manually for new systems.
        all_positions = (M @ all_positions.T).T
        # Recalculate constrain_positions once all_positions is updated
        constrain_positions = all_positions[constrain[1:], :]
        return (*params, crystal.Cell(tri_cell.vectors, [[all_positions[flat_indices[type_index][atom_index]]
            for atom_index in range(tri_cell.atom_count(type_index))] for type_index in range(tri_cell.atom_types)],
            tri_cell.names, tri_cell.periodic),
            # Distance of cp[0] from origin = |cp[0, 0]| since constrain[0] is already at origin
            numpy.array([numpy.abs(constrain_positions[0, 0])]))

    elif dims == 3 and free_dims == 3:
        # 3D free crystallite case: one atom constrained to x-axis, another to xy-plane
        # Need to rotate all atoms about origin such that cp[0,1] = cp[0,2] = cp[1,2] = 0

        # First rotational degree of freedom
        M_factor = numpy.sqrt(constrain_positions[0, 0] ** 2 + constrain_positions[0, 1] ** 2)
        M = numpy.array([
            [+constrain_positions[0, 0] / M_factor, +constrain_positions[0, 1] / M_factor, 0],
            [-constrain_positions[0, 1] / M_factor, +constrain_positions[0, 0] / M_factor, 0],
            [0, 0, 1]
        ])
        all_positions = (M @ all_positions.T).T
        constrain_positions = all_positions[constrain[1:], :]

        # Second rotational degree of freedom
        M_factor = numpy.sqrt(constrain_positions[0, 0] ** 2 + constrain_positions[0, 2] ** 2)
        M = numpy.array([
            [+constrain_positions[0, 0] / M_factor, 0, +constrain_positions[0, 2] / M_factor],
            [0, 1, 0],
            [-constrain_positions[0, 2] / M_factor, 0, +constrain_positions[0, 0] / M_factor]
        ])
        all_positions = (M @ all_positions.T).T
        constrain_positions = all_positions[constrain[1:], :]
        
        # Third rotational degree of freedom
        M_factor = numpy.sqrt(constrain_positions[1, 1] ** 2 + constrain_positions[1, 2] ** 2)
        M = numpy.array([
            [1, 0, 0],
            [0, +constrain_positions[1, 1] / M_factor, +constrain_positions[1, 2] / M_factor],
            [0, -constrain_positions[1, 2] / M_factor, +constrain_positions[1, 1] / M_factor]
        ])
        all_positions = (M @ all_positions.T).T
        constrain_positions = all_positions[constrain[1:], :]

        return (*params, crystal.Cell(tri_cell.vectors, [[all_positions[flat_indices[type_index][atom_index]]
            for atom_index in range(tri_cell.atom_count(type_index))] for type_index in range(tri_cell.atom_types)],
            tri_cell.names, tri_cell.periodic),
            numpy.array([
                # Distance of xy-plane atom to x-axis: gets multiplied as 2*pi*r
                numpy.abs(constrain_positions[1, 1]),
                # Distance of x-axis atom to origin: gets multiplied as 4*pi*r^2
                numpy.abs(constrain_positions[0, 0])
            ]))

    else:
        # Semi-infinite column case (and any other cases) not currently supported
        raise NotImplementedError("Number of dimensions or number of free dimensions not understood")

def with_periodic(cell, periodic):
    """
    Creates a copy of a cell with the desired periodicity.  This can easily be
    achieved using :py:func:`crystal.Cell` but this routine is provided for convenience.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    periodic : bool or numpy.ndarray
        Boolean flag or flags indicating periodicity in each dimension.  If a single flag is
        given, the periodicity will be set to the specified value in all dimensions.
    """

    return crystal.Cell(cell.vectors, cell.atom_lists, cell.names,
        [periodic] * cell.dimensions if numpy.isscalar(periodic) else periodic)

def calc_trans_rot(cell, T, trans_symm, rot_symm, lambda_norm):
    """
    Calculates real translational and rotational free energies for the non-periodic
    dimensions of a system.  The translational partition function is given as:
    
    .. math::
        Q_t=\\frac{VN^{d/2}}{\\sigma_t\\Lambda^d}

    while the rotational partition function is given for :math:`d_n=2` as:

    .. math::
        Q_r=\\frac{2\\pi\\sqrt{I/m}}{\\sigma_r\\Lambda}

    and for :math:`d_n=3` as:

    .. math::
        Q_r=\\frac{8\\pi^2\\sqrt{I_aI_bI_c/m^3}}{\\sigma_r\\Lambda^3}

    Here :math:`I` is the moment of inertia in two dimensions, while :math:`I_a`, :math:`I_b`,
    and :math:`I_c` are the principal moments of inertia in three dimensions.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.
    T : float
        See :py:func:`helmholtz`.
    trans_symm : int
        See :py:func:`helmholtz`.
    rot_symm : int
        See :py:func:`helmholtz`.
    lambda_norm : bool
        See :py:func:`calc_ig_a0`.

    Returns
    -------
    float
        The free energies per atom :math:`A_t/N` and :math:`A_r/N`.
    """

    dims = cell.dimensions
    free_dims = dims - numpy.sum(cell.periodic)
    rot_dof = free_dims * (free_dims - 1) // 2
    N = cell.atom_count()

    log_lambda = 0 if lambda_norm else -0.5 * numpy.log(2 * numpy.pi * T)
    nlq_t = numpy.log(trans_symm) + (dims * log_lambda) - numpy.log(cell.enclosed) - \
        (0.5 * dims * numpy.log(N))
    principal_moments, rotation_constant = calc_principal_moments(cell)
    nlq_r = numpy.log(rot_symm) + (rot_dof * log_lambda) - rotation_constant - \
        (0.5 * numpy.sum(numpy.log(principal_moments)))
    
    return nlq_t * T / N, nlq_r * T / N

def calc_principal_moments(cell):
    """
    Calculates principal moments of inertia for the non-periodic dimensions of a system.
    Additionally, determines the appropriate constant term for the logarithm of the
    rigid body rotational partition function.

    Parameters
    ----------
    cell : crystal.Cell
        See :py:func:`helmholtz`.

    Returns
    -------
    numpy.ndarray, float
        The principal moments of inertia, as well as a constant term calculated by
        :py:func:`calc_rot_term`.
    """

    # Normalize all positions so that the center of mass is at the origin
    atom_list = numpy.concatenate(cell.atom_lists)[:, ~cell.periodic]
    atom_list -= numpy.mean(atom_list, axis=0)

    dimensions = atom_list.shape[-1]
    trace_sum = numpy.einsum("ij,ij->", atom_list, atom_list)
    if dimensions < 2:
        # No rotational degrees of freedom to consider
        moments = numpy.array([])
    elif dimensions == 2:
        # In 2D, there is 2(2 - 1)/2 = 1 rotational degree of freedom
        # Take the sum over all normalized coordinates x_i^2 + y_i^2
        moments = numpy.array([trace_sum])
    elif dimensions == 3:
        # In 3D, there are 3(3 - 1)/2 = 3 rotational degrees of freedom
        # Take the eigenvalues of the 3D inertia tensor
        products = numpy.einsum("ij,ik->jk", atom_list, atom_list)
        tensor = numpy.eye(*products.shape) * trace_sum - products
        # The inertia tensor has I_jk = I_kj: it is symmetric and thus Hermitian
        moments = scipy.linalg.eigvalsh(tensor)
    else:
        raise NotImplementedError("Number of free dimensions not understood")

    return moments, calc_rot_term(dimensions)

def calc_rot_term(dimensions):
    """
    Calculates a constant term associated with rotation in a given number of dimensions.

    Parameters
    ----------
    dimensions : int
        The number of dimensions in which rotation can occur.

    Returns
    -------
    float
        A factor for the logarithm of the partition function: in particular, the sum of the
        logarithms of the surface areas of N-spheres up to the specified dimensionality.
        In two dimensions, this is :math:`\\ln2+\\ln\\pi`; in three, it is
        :math:`3\\ln2+2\\ln\\pi`.
    """

    factors = (numpy.arange(1, dimensions) + 1) / 2
    return numpy.sum(numpy.log(2) + factors * numpy.log(numpy.pi) - scipy.special.loggamma(factors))

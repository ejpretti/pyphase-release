# pyphase.structure: Evan Pretti
"""
Contains routines to make specific lattices and to generate non-periodic
clusters of specific shapes.
"""

import functools
import math
import numpy

from . import crystal

class Primitive:
    """
    Generates primitive cells.  The inter-atom spacing will be unity.  Use
    :py:meth:`.crystal.CellTools.scale_by` to adjust this.
    """

    @staticmethod
    def open_honeycomb():
        """
        2D open honeycomb lattice.

        .. plot::
            
            from pyphase import *
            visualization._cells(structure.Primitive.open_honeycomb()) 

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[numpy.sqrt(3), 0], [-numpy.sqrt(3) / 2, 3 / 2]]), [
            numpy.array([[0, 0], [0, 1]])], ["A"])


    @staticmethod
    def open_honeycomb_ab():
        """
        Binary 2D open honeycomb lattice.

        .. plot::
            
            from pyphase import *
            visualization._cells(structure.Primitive.open_honeycomb_ab()) 

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[numpy.sqrt(3), 0], [-numpy.sqrt(3) / 2, 3 / 2]]), [
            numpy.array([[0, 0]]),
            numpy.array([[0, 1]])],
            ["A", "B"])
    
    @staticmethod
    def square():
        """
        2D square lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.square())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[1, 0], [0, 1]]), [
            numpy.array([[0, 0]])], ["A"])

    @staticmethod
    def square_ab():
        """
        Binary 2D square lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.square_ab())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[2, 0], [1, 1]]), [
            numpy.array([[0, 0]]),
            numpy.array([[1, 0]])],
            ["A", "B"])

    @staticmethod
    def rhombic(alpha=50):
        """
        2D rhombic lattice.

        .. plot::
            
            from pyphase import *
            visualization._cells(structure.Primitive.rhombic_ab())

        Parameters
        ----------
        alpha : float
            See :py:meth:`rhombic_ab` for more information.

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        beta = (2 * alpha) - 60
        x_off = numpy.cos(beta * numpy.pi / 180)
        y_off = numpy.sin(beta * numpy.pi / 180)

        return crystal.Cell(
            numpy.array([[1 + (2 * x_off), 0], [(1 / 2) + x_off, (numpy.sqrt(3) / 2) + y_off]]), [
            numpy.array([[0, 0], [1, 0], [1 / 2, numpy.sqrt(3) / 2], [1 + x_off, y_off]])], ["A"])

    @staticmethod
    def rhombic_ab(alpha=50):
        """
        Binary 2D rhombic lattice.  The "characteristic angle" :math:`\\alpha` of the rhombic
        lattice may be varied between :math:`45^\\circ` (environment around all atoms
        is symmetric) and :math:`60^\\circ` degrees (degenerate case of alternating
        hexagonal).  The default is :math:`50^\\circ` degrees, in which type B neighbors of type
        A atoms are equally spaced as far as possible from each other.

        The examples below show, from left, to right, :math:`\\alpha=45^\\circ,50^\\circ,55^\\circ,60^\\circ`.

        .. plot::

            from pyphase import *
            visualization._cells(*(structure.Primitive.rhombic_ab(alpha=alpha)
                for alpha in (45, 50, 55, 60)))

        Parameters
        ----------
        alpha : float
            Characteristic angle :math:`\\alpha` (inclination of 4-atom rhombic centers) in
            degrees.  :math:`\\alpha=(\\beta/2)+30^\\circ`, where :math:`\\beta` is the inclination of
            an isolated B atom with respect to the A-A dimer axis.

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        # Angle alpha is inclination of 4-atom rhombic centers, while angle
        # beta is inclination of isolated B atom with respect to A-A axis.
        beta = (2 * alpha) - 60

        # Coordinates of inclination vector of B atom:
        x_off = numpy.cos(beta * numpy.pi / 180)
        y_off = numpy.sin(beta * numpy.pi / 180)

        return crystal.Cell(
            numpy.array([[1 + (2 * x_off), 0], [(1 / 2) + x_off, (numpy.sqrt(3) / 2) + y_off]]), [
            numpy.array([[0, 0], [1, 0]]),
            numpy.array([[1 / 2, numpy.sqrt(3) / 2], [1 + x_off, y_off]])],
            ["A", "B"])

    @staticmethod
    def hexagonal():
        """
        2D hexagonal lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.hexagonal())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """
        
        return crystal.Cell(
            numpy.array([[1, 0], [1 / 2, numpy.sqrt(3) / 2]]), [numpy.array([[0, 0]])], ["A"])


    @staticmethod
    def alternating():
        """
        Binary 2D alternating hexagonal lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.alternating())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[1, 0], [1, numpy.sqrt(3)]]), [
            numpy.array([[0, 0]]),
            numpy.array([[1 / 2, numpy.sqrt(3) / 2]])],
            ["A", "B"])

    @staticmethod
    def honeycomb():
        """
        Binary 2D honeycomb hexagonal lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.honeycomb())


        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[numpy.sqrt(3), 0], [-numpy.sqrt(3) / 2, 3 / 2]]), [
            numpy.array([[0, 0]]),
            numpy.array([[0, 1], [numpy.sqrt(3) / 2, 1 / 2]])],
            ["A", "B"])

    @staticmethod
    def kagome():
        """
        Binary 2D kagome hexagonal lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.kagome())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[2, 0], [1, numpy.sqrt(3)]]), [
            numpy.array([[0, 0]]),
            numpy.array([[1, 0], [1 / 2, numpy.sqrt(3) / 2], [3 / 2, numpy.sqrt(3) / 2]])],
            ["A", "B"])

    @staticmethod
    def square_kagome():
        """
        Binary 2D square kagome hexagonal lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Primitive.square_kagome())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[2, 0], [0, numpy.sqrt(3)]]), [
            numpy.array([[0, 0]]),
            numpy.array([[1, 0], [1 / 2, numpy.sqrt(3) / 2], [3 / 2, numpy.sqrt(3) / 2]])],
            ["A", "B"])

    @staticmethod
    def simple_cubic():
        """
        3D simple cubic lattice.
        
        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(numpy.eye(3), [numpy.array([[0, 0, 0]])], ["A"])

    @staticmethod
    def CsCl():
        """
        Binary 3D :math:`\\mathrm{CsCl}` body-centered cubic lattice.

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]) * (2 / numpy.sqrt(3)), [
            numpy.array([[0, 0, 0]]),
            numpy.array([[1, 1, 1]]) / numpy.sqrt(3)],
            ["A", "B"])

    @staticmethod
    def CuAu():
        """
        Binary 3D :math:`\\mathrm{CuAu}` face-centered cubic lattice.

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[1, 0, 0], [0, 1, 0], [0, 0, numpy.sqrt(2)]]), [
            numpy.array([[0, 0, 0]]),
            numpy.array([[1, 1, numpy.sqrt(2)]]) / 2],
            ["A", "B"])

    @staticmethod
    def CsCl_CuAu_transformation(x):
        """
        Binary 3D lattice in an intermediate state between :math:`\\mathrm{CsCl}` and
        :math:`\\mathrm{CuAu}` on a martensitic transformation pathway.

        Parameters
        ----------
        x : float
            The state between the two structures.  :math:`x=0` gives :math:`\\mathrm{CsCl}`,
            :math:`x=1` gives :math:`\\mathrm{CuAu}`, and intermediate values result in a
            linear interpolation of the angle along the arcs swept out by the particles during
            their transformations.

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        phi_cscl = numpy.arctan(1 / numpy.sqrt(2))
        phi_cuau = numpy.arctan(1)
        phi = (1 - x) * phi_cscl + x * phi_cuau
        a = numpy.sqrt(2) * numpy.cos(phi)
        c = 2 * numpy.sin(phi)

        return crystal.Cell(
            numpy.array([[a, 0, 0], [0, a, 0], [0, 0, c]]), [
            numpy.array([[0, 0, 0]]),
            numpy.array([[a, a, c]]) / 2],
            ["A", "B"])

    @staticmethod
    def HCPs():
        """
        Binary 3D straight hexagonal close packed lattice.

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[1, 0, 0], [0, numpy.sqrt(3), 0], [0, 0, 2 * numpy.sqrt(6) / 3]]), [
            numpy.array([[0, 0, 0], [0, numpy.sqrt(3) / 3, numpy.sqrt(6) / 3]]),
            numpy.array([[1 / 2, numpy.sqrt(3) / 2, 0], [1 / 2, 5 * numpy.sqrt(3) / 6, numpy.sqrt(6)/3]])],
            ["A", "B"])

class Unit:
    """
    Generates orthorhombic unit cells for some particular cases.
    """

    @staticmethod
    def alternating():
        """
        Binary 2D alternating hexagonal lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Unit.alternating())

        Returns
        -------
        crystal.Cell
            Primitive cell.
        """

        return crystal.Cell(
            numpy.array([[1, 0], [0, numpy.sqrt(3)]]), [
            numpy.array([[0, 0]]),
            numpy.array([[1 / 2, numpy.sqrt(3) / 2]])],
            ["A", "B"])

    @staticmethod
    def square_ab():
        """
        Binary 2D square lattice.

        .. plot::

            from pyphase import *
            visualization._cells(structure.Unit.square_ab())

        Returns
        -------
        crystal.Cell
            Unit cell.
        """

        return crystal.Cell(
            numpy.array([[2, 0], [0, 2]]), [
            numpy.array([[0, 0], [1, 1]]),
            numpy.array([[1, 0], [0, 1]])],
            ["A", "B"])

    @staticmethod
    def fcc():
        """
        3D face-centered cubic lattice.

        Returns
        -------
        crystal.Cell
            Unit cell.
        """

        return crystal.Cell(
            numpy.eye(3) * numpy.sqrt(2), [
            numpy.array([[0, 0, 0], [0, 1 / 2, 1 / 2],
                [1 / 2, 0, 1 / 2], [1 / 2, 1 / 2, 0]]) * numpy.sqrt(2)],
            ["A"])

    @staticmethod
    def NaCl():
        """
        Binary 3D NaCl lattice.

        Returns
        -------
        crystal.Cell
            Unit cell.
        """

        return crystal.Cell(
            numpy.eye(3) * 2, [
            numpy.array([[0, 0, 0], [1, 1, 0], [0, 1, 1], [1, 0, 1]]),
            numpy.array([[0, 1, 0], [1, 0, 0], [0, 0, 1], [1, 1, 1]])
            ], ["A", "B"])

class Cluster:
    """
    Generates clusters of specific shapes from periodic cells.  This feature should be used
    if shapes more complicated than what can be achieved with :py:func:`.crystal.CellTools.tile`
    are desired.
    """

    @staticmethod
    def min_stoich(atom_counts):
        """
        Determines the irreducible stoichiometric coefficients for a cell.
        For instance, for an :math:`\\mathrm{A}_8\\mathrm{B}_{12}` structure, this is
        :math:`\\mathrm{A}_2\\mathrm{B}_3`, whereas for an :math:`\\mathrm{A}_8\\mathrm{B}_{14}`
        lattice, this is :math:`\\mathrm{A}_4\\mathrm{B}_7`.

        Parameters
        ----------
        atom_counts : list(int)
            The stoichiometric coefficients to reduce.

        Returns
        -------
        list(int)
            Reduced coefficients.
        """

        gcd = functools.reduce(math.gcd, atom_counts)
        return [coefficient // gcd for coefficient in atom_counts]

    @staticmethod
    def make(cell, metric, density, *, radius=None, count=None):
        """
        Tiles a cell such that a cluster of the specified radius may be created,
        prunes atoms to fit while holding the cluster stoichiometry constant,
        and then places the cluster in the center of a box to set the desired density.

        Parameters
        ----------
        cell : crystal.Cell
            The template cell to tile.
        metric : callable
            A distance metric which indicates the effective distance from the
            coordinate system center given a coordinate.
        density : float
            The number density.  This affects the size of the box for the
            returned cell.  If this is set too high, atoms may exist outside
            of the box; although the output of this routine will not be affected,
            you may experience issues using the resulting configuration.
        radius : float
            The radius of the cluster (the exact interpretation of this parameter
            will depend on how the supplied metric handles it).
        count : int
            The number of atoms to place in the cluster.  Note that this may
            be rounded down if it is not commensurate with the stoichiometry of
            the cell.

        Returns
        -------
        crystal.Cell
            The cluster configuration created.
        """

        # Check specifications
        if (radius is None and count is None) or (radius is not None and count is not None):
            raise ValueError("Specify either a radius or count value to generate a cluster")

        # There is probably a more efficient way to achieve this, but this works
        if count is not None:
            # Set the count to a valid multiple that can be reduced
            stoichiometry = Cluster.min_stoich(cell.atom_counts)
            count_mult = sum(stoichiometry)
            count = (count // count_mult) * count_mult

            # Start at a low radius and work up
            trial_radius = 2 * (cell.enclosed ** (1 / cell.dimensions))
            while True:
                trial_cell = Cluster.make(cell, metric, density, radius=trial_radius)
                if sum(trial_cell.atom_counts) >= count:
                    break
                trial_radius *= (2 ** (1 / cell.dimensions))

            # We now have at least as many atoms as we need, and they will
            # be sorted from the core of the cluster to the surface: remove some
            # atoms as necessary
            coords = trial_cell.atom_lists
            red_count = count // count_mult
            coords = [coord_array[:red_count * stoich_coeff, :].copy()
                for coord_array, stoich_coeff in zip(coords, stoichiometry)]

            # Cell was centered before atoms were removed; now recenter properly
            # and set new density based on selected number of atoms
            old_center = trial_cell.vectors[0, 0] / 2
            new_length = (count / density) ** (1 / trial_cell.dimensions)
            coords = [coord_array - old_center + (new_length / 2) for coord_array in coords]
            return crystal.Cell(numpy.diag([new_length] * trial_cell.dimensions), coords, trial_cell.names,
                [False] * cell.dimensions)

        # This is the normal case in which the radius is specified
        else:
            # Determine the minimum distance across the cell
            minimum_distance = min([abs(numpy.dot(normal, cell.vectors[index]) / numpy.linalg.norm(normal)) \
                for index, normal in enumerate(cell.normals)])

            # Tile as necessary and shift to the center
            tiles = int(numpy.ceil(2 * radius / minimum_distance))
            cell = crystal.CellTools.tile(cell, (tiles,) * cell.dimensions)
            coords = [coord_array - (numpy.sum(cell.vectors, axis=0) / 2) for coord_array in cell.atom_lists]

            # Determine the irreducible stoichiometric coefficients for the system
            stoichiometry = Cluster.min_stoich(cell.atom_counts)
            
            # Sort and filter the coordinates by their metric distance from the center
            coords = [numpy.array(sorted((coord for coord in coord_array
                if metric(coord) < radius), key=metric)) for coord_array in coords]

            # Remove any extra atoms as necessary to maintain stoichiometry
            stoich_counts = [coord_array.shape[0] // stoich_coeff
                for coord_array, stoich_coeff in zip(coords, stoichiometry)]
            min_count = min(stoich_counts)
            coords = [coord_array[:min_count * stoich_coeff, :].copy()
                for coord_array, stoich_coeff in zip(coords, stoichiometry)]

            # Determine desired size to set number density and shift coordinates
            length = (sum(coord_array.shape[0] for coord_array in coords) / density) ** (1 / cell.dimensions)
            return crystal.Cell(numpy.diag([length] * cell.dimensions),
                [coord_array + (length / 2) for coord_array in coords], cell.names, [False] * cell.dimensions)

    @staticmethod
    def minkowski(norm_type=2):
        """
        Creates a norm from :py:func:`numpy.linalg.norm` (defaults to the Euclidean norm, but the
        type can be :math:`-\\infty, \\ldots, -2, -1, 0, 1, 2, \\ldots, \\infty`
        to get a variety of norms).

        Parameters
        ----------
        norm_type : float
            The Minkowski norm exponent.
        
        Returns
        -------
        norm
            A callable norm object.
        """

        return lambda coord, norm_type=norm_type: numpy.linalg.norm(coord, ord=norm_type)

    @staticmethod
    def spherical():
        """
        Uses the Euclidean distance to get a circular or spherical cluster.

        .. plot::

            from pyphase import *
            visualization._clusters(structure.Cluster.make(structure.Primitive.square_ab(), structure.Cluster.spherical(), 0.1, count=256))

        Returns
        -------
        norm
            A callable norm object.
        """

        return Cluster.minkowski()

    @staticmethod
    def rectangular():
        """
        Uses the norm :math:`\\max_i|x_i|`, to get a square or rectangular cluster.

        .. plot::

            from pyphase import *
            visualization._clusters(structure.Cluster.make(structure.Primitive.square_ab(), structure.Cluster.rectangular(), 0.1, count=256))


        Returns
        -------
        norm
            A callable norm object.
        """

        return Cluster.minkowski(numpy.inf)

    @staticmethod
    def diamond():
        """
        Uses the norm :math:`\\sum_i|x_i|`, to get a diamond-shaped cluster.

        .. plot::

            from pyphase import *
            visualization._clusters(structure.Cluster.make(structure.Primitive.square_ab(), structure.Cluster.diamond(), 0.1, count=256))


        Returns
        -------
        norm
            A callable norm object.
        """

        return Cluster.minkowski(1)

    @staticmethod
    def polygon(sides, rotate=False):
        """
        Defines a custom norm to generate polygons with any number of sides (this
        only works in the first two dimensions; for three-dimensional systems, the
        routine should produce polygonal prisms.  Note that there is no guarantee in
        general that the generated polygons will not have imperfections in their
        faces depending on the arrangement of the atoms in the input cell and the
        cluster specifications.  If shapes with particular exposed faces are desired,
        it may be necessary to perform some post-processing or construct the desired
        shapes manually.  For purposes where imperfections in the faces are tolerable,
        this routine should be sufficient.  Here are some examples without rotation:

        .. plot::

            from pyphase import *
            visualization._clusters(*(structure.Cluster.make(structure.Primitive.square_ab(), structure.Cluster.polygon(N), 0.1, count=256) for N in range(3, 7)))
        
        When the rotation flag is set, an :math:`N`-sided shape will be rotated by
        :math:`180^\\circ/N`.  Depending on :math:`N` and the input cell, this may
        simply flip the shape across one axis, or it may completely change the faces
        which are exposed:

        .. plot::

            from pyphase import *
            visualization._clusters(*(structure.Cluster.make(structure.Primitive.square_ab(), structure.Cluster.polygon(N, rotate=True), 0.1, count=256) for N in range(3, 7)))

        Parameters
        ----------
        sides : int
            Number of sides of the polygon; should be 3 or more.
        rotate : bool
            If true, rotates the polygon by half of the angle subtended by a
            single face.
        
        Returns
        -------
        norm
            A callable norm object.
        """

        alpha = 2 * numpy.pi / sides
        beta = alpha / 2 if rotate else 0
        def norm(coord, alpha=alpha, beta=beta):
            r = numpy.linalg.norm(coord[:2])
            theta = (numpy.arctan2(coord[1], coord[0]) - beta) % alpha
            return r * numpy.cos(theta - (alpha / 2)) / numpy.cos(alpha / 2)
        return norm

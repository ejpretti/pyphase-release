# pyphase.relax: Evan Pretti
"""
Uses LAMMPS to measure energies and enthalpies, and perform local minimizations.
"""

import enum
import io
import numpy

from . import crystal
from . import lammps

class RelaxType(enum.Enum):
    """
    Type of relaxation to perform.
    """
    
    #: Move only the atoms; do not modify the box at all.
    FROZEN = enum.auto()
    #: Scale the box isotropically.
    ISOTROPIC = enum.auto()
    #: Scale the box anisotropically but do not modify the tilt factors.
    ANISOTROPIC = enum.auto()
    #: Modify all parameters during the optimization.
    TRICLINIC = enum.auto()

def energy(cell, pair, relax_cell=False, **relax_kwargs):
    """
    Calculates the energy of a configuration.

    Parameters
    ----------
    cell : crystal.Cell
        The cell for which to calculate the energy.
    pair : lammps.Pair
        The Hamiltonian for the system (pair potentials).
    relax_cell : bool
        Whether or not to run relaxation before measuring energy.
    relax_kwargs : dict
        Keyword arguments to provide to :py:func:`relax`.

    Returns
    -------
    float
        The calculated energy.
    """

    return enthalpy(cell, pair, pressure=0, relax_cell=relax_cell, **relax_kwargs)

def enthalpy(cell, pair, pressure=0, relax_cell=False, **relax_kwargs):
    """
    Calculates the enthalpy of a configuration.

    Parameters
    ----------
    cell : crystal.Cell
        The cell for which to calculate the enthalpy.
    pair : lammps.Pair
        The Hamiltonian for the system (pair potentials).
    pressure : float
        The pressure of interest.
    relax_cell : bool
        Whether or not to run relaxation before measuring enthalpy.
    relax_kwargs : kwargs
        Keyword arguments to provide to :py:func:`relax`.

    Returns
    -------
    float
        The calculated enthalpy.
    """

    if relax_cell:
        cell, energy = relax(cell, pair, pressure=pressure, **relax_kwargs)
    else:
        cell, energy = relax(cell, pair, steps=0, **relax_kwargs)

    return energy + (pressure * cell.specific_enclosed) # U + PV

def relax(cell, pair, *, box_mode=RelaxType.FROZEN, pressure=None, tolerance=1e-12, steps=1000000, **lammps_kwargs):
    """
    Uses the LAMMPS minimizer to relax a cell.

    Parameters
    ----------
    cell : crystal.Cell
        The cell to optimize.
    pair : lammps.Pair
        The Hamiltonian for the system (pair potentials).
    box_mode : RelaxType
        Type of relaxation to perform.
    pressure : float
        The relaxation pressure.  This must be included if `box_mode` is anything
        but :py:data:`RelaxType.FROZEN` and there exist box dimensions to relax.
        Keep in mind that if, for instance, there are no periodic boundaries in
        the system, it will be impossible to apply pressure during the relaxation.
    tolerance : float
        The minimizer tolerance.  The smaller this value is, the more iterations
        LAMMPS will run before the minimizer converges.
    steps : int
        The maximum number of steps that LAMMPS should run.  Setting this to
        0 should cause LAMMPS to simply evaluate the energy without any change
        to the cell.
    lammps_kwargs : dict
        Keyword arguments to provide to :py:func:`.lammps.run_lammps`.

    Returns
    -------
    Cell, float
        The cell and the potential energy of the optimized configuration.
    """

    # Prepare LAMMPS input file
    run = lammps.LAMMPS()
    run.cell(cell)
    pair(run)

    # Set up minimization parameters
    if box_mode == RelaxType.FROZEN:
        pass

    else:
        # Always executed for all fix box/relax
        common_commands = ("boxmin", "all", "box/relax", "fixedpoint", 0, 0, 0)
        direction_str = "xyz"

        if box_mode == RelaxType.ISOTROPIC or box_mode == RelaxType.ANISOTROPIC:
            if numpy.all(cell.periodic):
                # Default case
                special_commands = ("iso" if box_mode == RelaxType.ISOTROPIC else "aniso", pressure)

            else:
                # Figure out directions in which box should be touched (leave non-periodic dimensions alone)
                directions = [direction for direction, periodic in zip(direction_str, cell.periodic) if periodic]

                if directions:
                    # Any dimensions to touch: specify value of pressure in that direction
                    special_commands = [command for direction in directions for command in (direction, pressure)]
                    if box_mode == RelaxType.ISOTROPIC:
                        if len(directions) > 1:
                            special_commands.extend(("couple", "".join(directions)))
                    else:
                        special_commands.extend(("couple", "none"))

                else:
                    # No dimensions to touch
                    special_commands = None

        elif box_mode == RelaxType.TRICLINIC:
            if numpy.all(cell.periodic):
                # Default case
                special_commands = ("tri", pressure)

            else:
                special_commands = []

                # Include diagonal components: only if periodic in that direction
                for index, name in enumerate(direction_str[:cell.dimensions]):
                    if cell.periodic[index]:
                        special_commands.extend((name, pressure))

                # Include off-diagonal components: only if periodic in those directions
                if numpy.all(cell.periodic[[0, 1]]):
                    special_commands.extend(("xy", 0))
                if cell.dimensions == 3:
                    if numpy.all(cell.periodic[[1, 2]]):
                        special_commands.extend(("yz", 0))
                    if numpy.all(cell.periodic[[2, 0]]):
                        special_commands.extend(("xz", 0))

                if special_commands:
                    # No coupling ever for triclinic case
                    special_commands.extend(("couple", "none"))
                else:
                    special_commands = None

        else:
            raise ValueError("Unrecognized box handling mode")
        
        # If special_commands is set to None explicitly, don't apply any fix (box is frozen in place)
        if special_commands is not None:
            run.fix(*common_commands, *special_commands)

    # Set up reporting parameters
    run.thermo_style("custom", "step", "etotal", "press", "vol", "enthalpy")
    run.thermo_modify("format", "float", "%.16g")

    # Run the minimization
    run.neigh_modify("delay", 0, "every", 1, "check", "yes")
    run.minimize(tolerance, tolerance, steps, steps)

    # Output the relaxed structure
    run.write_data("relaxed")

    # Call LAMMPS
    answer = lammps.run_lammps(run, ["relaxed"], **lammps_kwargs)

    # Retrieve the relaxed energy
    energy = next(lammps.log_seek(answer[0], "step toteng press volume enthalpy", bool(steps)))[1]

    # Retrieve the relaxed structure
    relaxed_data = io.StringIO(answer[1]["relaxed"].decode())
    relaxed_cell = crystal.CellCodecs.read_lammps(relaxed_data)

    # If necessary, reduce the dimensions of the structure from 3 to 2
    relaxed_cell = crystal.Cell(relaxed_cell.vectors[:cell.dimensions, :cell.dimensions],
        [atom_list[:, :cell.dimensions] for atom_list in relaxed_cell.atom_lists], cell.names,
        cell.periodic[:cell.dimensions])

    # Note: do not reduce relaxed cell until after restoration of periodicity specifications
    return crystal.CellTools.reduce(relaxed_cell, shift=False), energy

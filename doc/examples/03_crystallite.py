#!/usr/bin/env python3
# 03_crystallite.py: Finite crystallite example.

import inspect
import os
import sys

# Locate and import PyPhase.

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.realpath(inspect.getfile(inspect.currentframe()))))))

import pyphase
import pyphase_tools._test_utilities

# Make an FCC cube with the correct spacing and all (100) surfaces exposed
# There are 365 = 256 + 32+32+32 + 4+4+4 + 1 atoms (bulk + extra face + extra edge + extra corner)
cell = pyphase.crystal.CellTools.tile(pyphase.structure.Unit.fcc(), (4, 4, 4), partial_radii=(1,))
cell = pyphase.crystal.CellTools.scale_by(pyphase.crystal.CellTools.scale_by(cell, 2 ** (1 / 6)), 2, move_atoms=False)
cell = pyphase.einstein.with_periodic(cell, False)

# Make a Lennard-Jones potential and set the temperature
pair = pyphase.lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
T = 0.1

# Perform the calculation and summarize the results
cache = []
# Note the selection of the rotational symmetry number and constraint indices
# Constraint atoms have x/y = 4*2^(2/3) and are along axes
A, dA = pyphase.einstein.helmholtz(cell, pair, T, pyphase_tools._test_utilities.core_count(), 1, 24,
    pyphase.einstein.ReferenceMode.PLANCK_UNITY, False, (0, 100, 20), cache)
# Summarize the results
pyphase.einstein.echo_cache(cache)
print("A/N = {} +/- {}".format(A, dA))
pyphase.visualization.integrand((cache,))

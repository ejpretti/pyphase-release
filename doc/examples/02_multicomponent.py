#!/usr/bin/env python3
# 02_multicomponent.py: Free energy of multicomponent Lennard-Jones system.

import inspect
import os
import sys

# Locate and import PyPhase.

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.realpath(inspect.getfile(inspect.currentframe()))))))

import pyphase
import pyphase_tools._test_utilities

# Make a 250-atom CuAu cuboid with density 1
cell = pyphase.crystal.CellTools.scale_to(pyphase.crystal.CellTools.tile(pyphase.structure.Primitive.CuAu(), (5, 5, 5)), 1)
# Make a Lennard-Jones potential for 2 particle types with different values of epsilon
pair = pyphase.lammps.LAMMPSPair("lj/cut", 2, {
    (1, 1): (0.75, 1),
    (2, 2): (1.00, 1),
    (1, 2): (1.25, 1)
}, style_args=(3,))
# Use reduced temperature 0.2
T = 0.2

# Perform the calculation and summarize the results
cache = []
# Note the selection of the translational symmetry number
A, dA = pyphase.einstein.helmholtz(cell, pair, T, pyphase_tools._test_utilities.core_count(), cell.atom_count() // 2, 1,
    pyphase.einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache)
pyphase.einstein.echo_cache(cache)
print("A/N = {} +/- {}".format(A, dA))
pyphase.visualization.integrand((cache,))

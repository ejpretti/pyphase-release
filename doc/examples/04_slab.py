#!/usr/bin/env python3
# 04_slab.py: Semi-infinite slab example.

import inspect
import os
import sys

# Locate and import PyPhase.

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.realpath(inspect.getfile(inspect.currentframe()))))))

import pyphase
import pyphase_tools._test_utilities

import numpy

# Make an FCC slab with the correct spacing and all (100) surfaces exposed
# There are 288 = 256 + 32 atoms (bulk + extra face)
cell = pyphase.crystal.CellTools.tile(pyphase.structure.Unit.fcc(), (1, 1, 4), partial_radii=(1,))
cell = pyphase.crystal.CellTools.scale(cell, numpy.diag(numpy.diag(cell.vectors) * numpy.array([1, 1, 2])), move_atoms=False)
cell = pyphase.crystal.CellTools.condense(pyphase.crystal.CellTools.wrap(cell))
cell = pyphase.crystal.CellTools.scale_by(pyphase.crystal.CellTools.tile(cell, (4, 4, 1)), 2 ** (1 / 6))
cell = pyphase.einstein.with_periodic(cell, (True, True, False))

# Make a Lennard-Jones potential and set the temperature
pair = pyphase.lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
T = 0.1

# Perform the calculation and summarize the results
cache = []
# Note the selection of the translational symmetry number
# Also note a manual override to improve integrand sampling and use lower spring constants
A, dA = pyphase.einstein.helmholtz(cell, pair, T, pyphase_tools._test_utilities.core_count(), 16, 1,
    pyphase.einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache, overrides=dict(C_FACTOR=1e-3))
# Summarize the results
pyphase.einstein.echo_cache(cache)
print("A/N = {} +/- {}".format(A, dA))
pyphase.visualization.integrand((cache,))

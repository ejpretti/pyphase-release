#!/usr/bin/env python3
# 01_lj.py: Free energy of Lennard-Jones solid in FCC configuration.

import inspect
import os
import sys

# For example script: should locate and import PyPhase.  Normally, PyPhase
# would be in the PYTHONPATH and this step is not necessary.  This simply
# ensures that this example can always be run from the repository.

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.realpath(inspect.getfile(inspect.currentframe()))))))

import pyphase
import pyphase_tools._test_utilities

# Make a 256-atom FCC cube with density 1
cell = pyphase.crystal.CellTools.scale_to(pyphase.crystal.CellTools.tile(pyphase.structure.Unit.fcc(), (4, 4, 4)), 1)
# Make a Lennard-Jones potential for 1 particle type with epsilon 1, sigma 1, cutoff 3
pair = pyphase.lammps.LAMMPSPair("lj/cut", 1, {(1, 1): (1, 1)}, style_args=(3,))
# Use reduced temperature 0.2
T = 0.2

# Perform the calculation (takes a few minutes depending on processor speed and number of cores)
# This is fully automatic: change the cell, pair, T to anything and PyPhase will pick appropriate parameters
cache = []
A, dA = pyphase.einstein.helmholtz(cell, pair, T, pyphase_tools._test_utilities.core_count(), cell.atom_count(), 1,
    pyphase.einstein.ReferenceMode.PLANCK_UNITY, False, (0,), cache)
# Summarize the results
pyphase.einstein.echo_cache(cache)
print("A/N = {} +/- {}".format(A, dA))
pyphase.visualization.integrand((cache,))

:py:mod:`pyphase_tools.fermi_jagla`: Fermi-Jagla potential manipulation
=======================================================================

.. automodule:: pyphase_tools.fermi_jagla
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

:py:mod:`pyphase_test.test_relax`: tests for :py:mod:`pyphase.relax`
====================================================================

.. automodule:: pyphase_test.test_relax
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

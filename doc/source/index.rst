PyPhase documentation
=====================

This is the documentation for the PyPhase package.  Detailed API documentation
is presented below.  Note that it is possible from this documentation to view
the source associated with each routine: for completeness, test routines are
also included although the tests themselves do not have associated explicit
documentation.  In addition to the comprehensive API listing, some guide
material is also provided to discuss some important details for performing free
energy calculations.  It is recommended to review this material before running
such calculations.

.. toctree::
    guide
    api

:py:mod:`pyphase_test.test_structure`: tests for :py:mod:`pyphase.structure`
============================================================================

.. automodule:: pyphase_test.test_structure
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

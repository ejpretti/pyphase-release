:py:mod:`pyphase_test.test_einstein`: tests for :py:mod:`pyphase.einstein`
==========================================================================

.. automodule:: pyphase_test.test_einstein
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

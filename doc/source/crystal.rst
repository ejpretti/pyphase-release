:py:mod:`pyphase.crystal`: crystal manipulation
===============================================

.. automodule:: pyphase.crystal
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

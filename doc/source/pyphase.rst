:py:mod:`pyphase`: PyPhase core module
======================================

.. automodule:: pyphase
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

Importing this module should include all of its submodules, which are as follows:

.. toctree::
    crystal
    structure
    lammps
    relax
    einstein
    visualization

User guides
===========

The following pages contain useful information on using various modules to
perform specific types of calculations.

.. toctree::
    guide_einstein

:py:mod:`pyphase_test.test_crystal`: tests for :py:mod:`pyphase.crystal`
========================================================================

.. automodule:: pyphase_test.test_crystal
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

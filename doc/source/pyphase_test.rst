:py:mod:`pyphase_test`: test suite
==================================

.. automodule:: pyphase_test
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

This module has the following submodules:

.. toctree::
    test_crystal
    test_structure
    test_lammps
    test_relax
    test_einstein

The test suite is included in the documentation only so that the source code for
the tests can easily be browsed.  Detailed documentation on the tests will not be
present in these documentation pages, but the functionality of the tests can be
inspected.

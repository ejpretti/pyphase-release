:py:mod:`pyphase.einstein`: Frenkel-Ladd integration
====================================================

.. automodule:: pyphase.einstein
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

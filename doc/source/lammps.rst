:py:mod:`pyphase.lammps`: interface with LAMMPS
===============================================

.. automodule:: pyphase.lammps
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

:py:mod:`pyphase.structure`: periodic and non-periodic crystal generation
=========================================================================

.. automodule:: pyphase.structure
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

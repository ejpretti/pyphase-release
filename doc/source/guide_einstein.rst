User guide: Free energies of solid phases
=========================================

This page presents some important guidelines for performing free energy
calculations on solid phases.  For comprehensive API documentation, see
:py:mod:`pyphase.einstein`.  This solid phase free energy module supports
calculations for two- and three-dimensional systems, systems with any number of
components, fully periodic crystals, fully non-periodic "crystallites", and
semi-infinite slabs.  The module can also attempt to automatically determine
optimal parameters for convergence and accuracy of free energy perturbation and
thermodynamic integration calculation steps.

The main routine to use in most cases is :py:func:`pyphase.einstein.helmholtz`, which
determines the Helmholtz free energy per particle for a given system, along
with an estimate of the error in the calculated value.  Some examples are given below,
and the documentation is mostly self-explanatory, so this guide highlights some additional
caveats which might not otherwise be obvious.

Specifying input
----------------

For fully periodic systems, specifying the input is straightforward.  There are no rotational
degrees of freedom and ``rot_symm=1``.  ``trans_symm`` should be set to the translational
symmetry of the system.  For single-component periodic systems, this is usually the number
of particles in the system.  However, the value of this parameter is not critical as it
simply contributes a constant offset of :math:`\ln\sigma_t/N` to :math:`A/Nk_BT`; in
extrapolating to :math:`N\rightarrow\infty`, the contribution of this term is zero.  A single
particle must be selected to constrain translational motion, and usually the choice
of the first particle in the input crystal (``constrain=(0,)``) is suitable.

For non-periodic systems, ``trans_symm=1`` and ``rot_symm`` should be set to the rotational
symmetry of the system.  For semi-periodic systems, the symmetry parameters should be set
appropriately.  For non-periodic systems, the choice of particles to ``constrain`` is more
critical.  Selecting particles which are close to each other may allow for more rotational
drifting than selecting those which are far from each other.  Less drift will lead to a
more accurate integration.  In theory, selecting different particles to constrain should not
change the final free energy, although it will affect the various free energy terms found
along the way.

A few additional parameters may affect the results.  ``reference_mode`` will change
the absolute value of the free energy: note that options other than
:py:data:`pyphase.einstein.ReferenceMode.PLANCK_UNITY` (which effectively uses
natural units in which :math:`\Lambda=h/\sqrt{2\pi mk_BT}` is calculated as though
:math:`h=m=k_B=1`) will introduce an additional dependence upon temperature and the density
of the ``cell`` to the free energy.  ``quad_err`` does not change the value of the
result, but will increase the estimated error to also included an expected error between
the result of Gaussian quadrature and the true area under the integrand for thermodynamic
integration.

Checking output
---------------

As with any calculation, it is important to check intermediate and final
results to ensure that the system being simulated is behaving properly, instead
of blindly trusting the numerical output.  For a new system, it is recommended
to use ``debug_repl=True`` when first attempting a calculation.  This option stops
after the end of each simulation run in an interactive IPython session.  The
contents of the temporary directories created for each run can be inspected,
and trajectories can be visualized using any desired package which can read LAMMPS
text data files (e.g. OVITO_).

.. _OVITO: https://www.ovito.org/

The :py:func:`pyphase.einstein.helmholtz` routine takes a parameter ``cache``,
which should usually be an empty list initialized outside of the routine call parameter list.
This can be pickled and written out to a file, where the ``einsteincache`` tool
in the ``bin`` directory can be used to read it back.  This tool can generate a
summary of solid free energy calculation run information, and also produce plots useful for
evaluating the quality of the thermodynamic integration.  In some cases, poor
selection of constraints or other parameters may lead to a large increase in mean
squared displacement near one end of the integrand; this is usually immediately
evident upon examining the generated plots.

If many runs under similar conditions are desired, it can be useful to set
``stop=True``, which will perform the automatic constant determination steps but
will not proceed to calculate the free energy.  In this case, parameters to
:py:func:`pyphase.einstein.run_em` are generated, and can be used to more
efficiently perform many calculations while skipping over the constant determination
steps.  However, this functionality may also be useful if additional manual fine-tuning
of the parameters is desired or necessary for good results.

If it is desired to manually override parameters used in the *automatic determination*
step instead of the actual free energy calculation step, the ``overrides`` parameter to
``helmholtz`` can be employed.  Overridable parameters and their defaults are given
and described at :py:class:`pyphase.einstein.OptimizationDefaults`.  Useful parameters to
adjust are:

* ``C_FACTOR``: Making this value smaller and smaller will integrate using lower and lower
  spring constant values, which can be useful to resolve a sharply varying integrand.
  However, for metastable systems in which simulating with a spring constant of zero will
  cause transformation or disintegration of the system, care should be taken not to set this
  value too low.

* ``ESTIMATE_CONSTANT``: To determine the necessary amount of time needed for sampling a system,
  trial simulations with no harmonic springs will be run.  As described above, metastable systems
  may cause a problem and prevent the automatic constant determination from converging.  A finite
  spring constant for this step can be set in such a case.

* ``PROD_LONG_PERIODS``: Can be increased to get a more accurate final answer.  Simulations
  will be run for greater numbers of timesteps as this value is raised.

* ``QUADRATURE_POINTS``: May need to be increased for troublesome integrands.

Examples
--------

All of these examples can be run from the ``doc/examples`` directory.  They are reproduced here
for convenience.

Lennard-Jones crystal
^^^^^^^^^^^^^^^^^^^^^

Simple periodic FCC LJ system.

.. literalinclude:: ../examples/01_lj.py

Multicomponent system
^^^^^^^^^^^^^^^^^^^^^

Demonstrates use of multiple particle types in cells and potentials.

.. literalinclude:: ../examples/02_multicomponent.py

Non-periodic crystallite
^^^^^^^^^^^^^^^^^^^^^^^^

Illustrates how to set up a crystal system for this kind of calculation.

.. literalinclude:: ../examples/03_crystallite.py

Semi-infinite slab
^^^^^^^^^^^^^^^^^^

Illustrates how to set up a system for this kind of calculation, and the use of a parameter override.

.. literalinclude:: ../examples/04_slab.py

:py:mod:`pyphase_tools`: ancillary utilities
============================================

.. automodule:: pyphase_tools
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

This module has the following submodules:

.. toctree::
    fermi_jagla
    test_utilities

Note that they will not be automatically imported when :py:mod:`pyphase_tools`
is imported.  Instead, individual modules must be imported to use their functionality.

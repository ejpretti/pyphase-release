#!/usr/bin/env python3
# ipyphase: Evan Pretti
# Presents an interactive IPython shell with pyphase loaded.

def load_pyphase():
    # Remove this function from the global namespace
    global load_pyphase
    del load_pyphase
    
    # Import necessary modules in local namespace
    import inspect
    import os
    import sys

    # Get current file real absolute path
    current_file = os.path.realpath(inspect.getfile(inspect.currentframe()))
    # Backtrack up to pyphase root directory
    pyphase_root = os.path.dirname(os.path.dirname(current_file))
    # This functionality is in pyphase_tools._test_utilities but we would
    # have to do this to find that module anyway
    
    # Do the import
    sys.path.insert(0, pyphase_root)
    import pyphase
    return pyphase

def shell(local):
    # Remove this function from the global namespace
    global shell
    del shell

    try:
        # Try IPython by default
        import IPython
        return lambda local=local: IPython.embed(colors="Linux", user_ns=local)
    except ImportError:
        # Fall back to the Python shell
        import code
        return lambda local=local: code.interact(local=local)

if __name__ == "__main__":
    import numpy
    pyphase = load_pyphase()
    shell(locals())()
